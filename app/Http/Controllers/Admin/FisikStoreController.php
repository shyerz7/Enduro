<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FisikStore;
use App\Models\Fisik;
use App\Models\Store;
use App\Models\Report;
use Breadcrumbs;
use Session;

class FisikStoreController extends Controller
{
    public function stores() {

        $pageTitle = 'List ';


        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs)
        {
            $breadcrumbs->push('List Store', route('admin.fisikstores.store'));
        });

        $stores = Store::with('kota')->whereNull('is_main')->get();

        return view('admin.fisikstores.store',compact('stores','pageTitle','breadcrumb'));
    }

    public function index($code_store)
    {
    	$store = Store::findOrFail($code_store);

    	$pageTitle = 'List '.$store->nama;

    	$breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($store)
        {
            $breadcrumbs->push('List Store', route('admin.fisikstores.store'));
            $breadcrumbs->push($store->name, route('admin.fisikstores.index', $store->code));
        });

        $fisikstores = FisikStore::where('report_id',Report::getCurrentSession()->id)
        						  ->where('code_store',$code_store)
        						  ->with(['fisik','store'])
        						  ->get();

        return view('admin.fisikstores.index',compact('store','pageTitle','breadcrumb','fisikstores'));						  
    }

    public function create($code_store)
    {
    	$store = Store::findOrFail($code_store);

    	$pageTitle = 'Create Fisik '.$store->nama;

    	$fisiks = Fisik::pluck('name','id');

    	return view('admin.fisikstores.create',compact('store','pageTitle','breadcrumb','fisiks'));		
    }

    public function store(Request $request,$code_store)
    {
    	$data['report_id']      = Report::getCurrentSession()->id;
    	$data['code_store']     = $code_store;
    	$data['fisik_id']       = $request->input('fisik_id');

    	$fisikstore = FisikStore::create($data);

    	return redirect()->route('admin.fisikstores.index',$code_store)->with('alert-success', 'Data Berhasil Disimpan.');
    }

    public function destroy($id)
    {
    	FisikStore::destroy($id);
    }
}
