<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Fisik;
use App\Http\Requests\FisikRequest;
use Breadcrumbs;


class FisiksController extends Controller
{
    public function index()
    {
        $pageTitle = 'Fisik Panel';

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs)
        {
            $breadcrumbs->push('List Fisik', route('fisiks.index'));
        });

        $fisiks = Fisik::orderBy('id','asc')->get();

        return view('admin.fisik.index',compact('fisiks','pageTitle','breadcrumb'));
    }

    public function create()
    {
    	$pageTitle = 'Fisik Create';

    	return view('admin.fisik.create',compact('pageTitle'));
    }

    public function store(FisikRequest $request)
    {

    	$data['name']  = $request->input('name');

    	$fisik = Fisik::create($data);

    	return redirect()->route('fisiks.index')->with('alert-success', 'Data Berhasil Disimpan.');

    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($code)
    {
    	$fisik = Fisik::findOrFail($code);

        return view('admin.fisik.edit',compact('fisik'))
            ->withPageTitle('position Edit');
    }

    public function update($code, FisikRequest $request)
    {
    	$fisik = Fisik::findOrFail($code);
    	$fisik->name  = $request->input('name');
    	$fisik->save();

    	return redirect()->route('fisiks.index')->with('alert-success', 'Data Berhasil Diupdate.');

    }

    public function destroy($code)
    {
    	Fisik::destroy($code);
    }
}
