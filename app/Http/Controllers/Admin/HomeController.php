<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report;

class HomeController extends Controller
{
    public function index() {
    	
    	$pageTitle = 'Homepage Panel';

    	$report = Report::all()->last();

    	$reports = Report::orderBy('id','asc')->get();

    	\Session::put('report', $report);

    	\Session::put('reports', $reports);

    	return view('admin.homepage.index',compact('reports','pageTitle'));
    }
}
