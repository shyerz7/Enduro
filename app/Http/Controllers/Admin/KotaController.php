<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kota;
use App\Models\Region;
use App\User;
use App\Role;
use App\Http\Requests\KotaRequest;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Excel;
use Validator;
use Session;

class KotaController extends Controller
{

    public function index()
    {
        $pageTitle = 'Kota Panel';

        $kotas = Kota::get();

        return view('admin.kota.index',compact('kotas','pageTitle'));
    }

    public function create()
    {
    	$pageTitle = 'Kota Create';

    	return view('admin.kota.create',compact('pageTitle'));
    }

    public function store(KotaRequest $request)
    {

    	$data['code']			= substr( md5(rand()), 0, 4);
    	$data['name']			= $request->input('name');

    	$kota = Kota::create($data);

    	return redirect()->route('kotas.index')->with('alert-success', 'Data Berhasil Disimpan.');

    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($code)
    {
    	$kota = Kota::findOrFail($code);

        return view('admin.kota.edit',compact('kota'))
            ->withPageTitle('Kota Edit');
    }

    public function update($code, KotaRequest $request)
    {
    	$kota = Kota::findOrFail($code);
    	$kota->name  = $request->input('name');
    	$kota->save();
    	return redirect()->route('kotas.index')->with('alert-success', 'Data Berhasil Diupdate.');

    }

    public function destroy($code)
    {
    	Kota::destroy($code);
    }

    /**
     * [generateExcelTemplate description]
     * @return [type] [description]
     */
    public function generateExcelTemplate()
    {
        ob_end_clean();  
        
        Excel::create('Template Upload Kota',function($excel){

            $excel->setTitle('Template Upload Report Staff')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data kota projek MS');

            $excel->sheet('Data Kota',function($sheet){
                $column = [];
                array_push($column,'region');
                array_push($column,'code');
                array_push($column,'name');
                array_push($column,'address');
                array_push($column,'latitude');
                array_push($column,'longitude');
                array_push($column,'email');
                array_push($column,'password');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function importExcel(Request $request)
    {
        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'id_kota'   =>  'required',
            'code'      =>  'required',
            'name'      =>  'required',
            'address'   =>  'required',
            'email'     =>  'required',
            'password'  =>  'required',
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $kotas_id = [];

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {


            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            // $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            // if ($validator->fails()) continue;

            $region  = Region::where('name',$row['region'])->first();
            $cekkota = Kota::where('code',$row['code'])->first();

            // if((is_null($cekkota)) && (!is_null($region)))
            if(is_null($cekkota))
            {
                /**
                 * create kota
                 */
                // $data['id_region']      =   $region->id;
                $data['code']           =   $row['code'];
                $data['name']           =   $row['name'];
                $data['latitude']       =   $row['latitude'];
                $data['longitude']      =   $row['longitude'];
                $data['address']        =   $row['address'];

                $kota = Kota::create($data);

                array_push($kotas_id,$kota->id);


                // buatkan account email
                $user = User::create([
                    'name'      => $row['name'],
                    'email'     => $row['email'],
                    'password'  => bcrypt($row['password']),
                ]);

                $kotaRole = Role::where('name','admin_kota')->first();
                $user->attachRole($kotaRole);

                $kota->user_id  =   $user->id;
                $kota->save();
                
            }

        }// end foreach

        // Ambil semua staff yang baru dibuat
        $kotas = Kota::whereIn('code',$kotas_id)->get();

        // redirect ke form jika tidak ada buku yang berhasil diimport
        if ($kotas->count() == 0) {

            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Tidak ada staff yang berhasil diimport."
            ]);

            return redirect()->back();
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport " . $kotas->count() . " kota."
        ]);

        // Tampilkan index buku
        return redirect()->back();
    }
}
