<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ParameterCategoryFisik;
use App\Models\Fisik;
use App\Http\Requests\ParameterCategoryFisikRequest;
use Breadcrumbs;
use Session;
use App\Models\Report;

class ParameterCategoryFisikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_fisik)
    {

        $report_id = Report::getCurrentSession()->id;

    	$fisik = Fisik::findOrFail($id_fisik);

    	$breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($fisik)
        {
            $breadcrumbs->push('List Fisik', route('fisiks.index'));
            $breadcrumbs->push($fisik->name, route('admin.fisik.parameter_category.fisik', $fisik->id));
        });

        $categories = ParameterCategoryFisik::where('report_id',$report_id)
                                            ->where('fisik_id','=',$id_fisik)->orderBy('id','asc')->get();

        return view('admin.fisik.parameter_category.create',compact('id_fisik','fisik','categories','breadcrumb'))
            ->withPageTitle('Parameter Category');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id_fisik,ParameterCategoryFisikRequest $request)
    {
		$report_id	=	Session::get('report')->id;

        $category = $request->input('category');

        ParameterCategoryFisik::tambah($report_id,$id_fisik,$category);

        return redirect()->route('admin.fisik.parameter_category.fisik',$id_fisik)->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_fisik,$id)
    {
        $fisik = Fisik::findOrFail($id_fisik);

        $category = ParameterCategoryFisik::findOrFail($id);

        return view('admin.fisik.parameter_category.edit',compact('fisik','category'))
            ->withPageTitle('Parameter Category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id_fisik,$id,Request $request)
    {
        $category = ParameterCategoryFisik::findOrFail($id);

        $category->category  = $request->input('category');
        $category->save();

        return redirect()->route('admin.fisik.parameter_category.fisik',$id_fisik)->with('alert-success', 'Data Berhasil Diupdate.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterCategoryFisik::destroy($id);

    }
}
