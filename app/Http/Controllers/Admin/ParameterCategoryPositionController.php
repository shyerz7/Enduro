<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ParameterCategoryPosition;
use App\Models\Position;
use App\Http\Requests\ParameterCategoryPositionRequest;
use Breadcrumbs;
use Session;

class ParameterCategoryPositionController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_posisi)
    {
    	$position = Position::findOrFail($id_posisi);

    	$breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($position)
        {
            $breadcrumbs->push('List Posisi', route('positions.index'));
            $breadcrumbs->push($position->name, route('admin.posisi.parameter_category.position', $position->id));
        });


        $categories = ParameterCategoryPosition::where('position_id','=',$id_posisi)->orderBy('id','asc')->get();

        return view('admin.position.parameter_category.create',compact('id_posisi','position','categories'))
            ->withPageTitle('Parameter Category');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id_posisi,ParameterCategoryPositionRequest $request)
    {
		// $report_id	=	Session::get('report')->id;

        $category = $request->input('category');

        ParameterCategoryPosition::tambah($id_posisi,$category);

        return redirect()->route('admin.posisi.parameter_category.position',$id_posisi)->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_posisi,$id)
    {
        $position = Position::findOrFail($id_posisi);

        $category = ParameterCategoryPosition::findOrFail($id);

        return view('admin.position.parameter_category.edit',compact('position','category'))
            ->withPageTitle('Parameter Category');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id_posisi,$id,Request $request)
    {
        $category = ParameterCategoryPosition::findOrFail($id);

        $category->category  = $request->input('category');
        $category->save();

        return redirect()->route('admin.posisi.parameter_category.position',$id_posisi)->with('alert-success', 'Data Berhasil Diupdate.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterCategoryPosition::destroy($id);

    }
}
