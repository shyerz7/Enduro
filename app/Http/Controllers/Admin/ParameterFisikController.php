<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ParameterCategoryFisik;
use App\Models\ParameterFisik;
use App\Models\Fisik;
use Breadcrumbs;
use Session;
use App\Http\Requests\ParameterFisikRequest;
use App\Models\Report;

class ParameterFisikController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_fisik, $id_kategori)
    {
        $report_id = Report::getCurrentSession()->id;

        $fisik = Fisik::findOrFail($id_fisik);

        $category = ParameterCategoryFisik::findOrFail($id_kategori);

        $parameters = ParameterFisik::where('report_id',$report_id)
                                    ->where('fisik_parameter_category_id', '=', $id_kategori)
                                    ->orderBy('id')
                                    ->get();

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($fisik,$category)
        {
            $breadcrumbs->push('List Posisi', route('positions.index'));
            $breadcrumbs->push($fisik->name, route('admin.fisik.parameter_category.fisik', $fisik->id));
            $breadcrumbs->push($category->category, route('admin.fisik.parameter', [$fisik->id,$category->id]));
        });

        return view('admin.fisik.parameter.create', compact('fisik', 'category', 'parameters','breadcrumb'))
            	->withPageTitle('Parameter');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id_fisik,$id_kategori, ParameterFisikRequest $request)
    {
        $text = $request->input('teks');
        $parameter_ids = [];
        $report_id = Session::get('report')->id;

        foreach ($text as $i => $value) {
            // $value = variabel untuk menyimpan inputan teks

            // pecah inputan
            $temp = explode(";",$value);
            $kode_atribut = $temp[0];
            $teks = $temp[1];
            $positif_stat = $temp[2];
            $negatif_stat = $temp[3];

            if (trim($value)) {
                $id_parameter = ParameterFisik::tambah($report_id,$id_kategori,$teks,$kode_atribut,$positif_stat,$negatif_stat);

                $parameter_ids[]['id'] = $id_parameter;
            }
        }

        return redirect()->route('admin.fisik.parameter',[$id_fisik,$id_kategori])->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_fisik,$id_kategori,$id)
    {

        $fisik = Fisik::findOrFail($id_fisik);

        $category = ParameterCategoryFisik::findOrFail($id_fisik);

        $parameter = ParameterFisik::findOrFail($id);

        return view('admin.fisik.parameter.edit',compact('fisik','category', 'parameter'))
                ->withPageTitle('Parameter')
                ->withPageDescription('Parameter  Dashboard Panel');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {

        

        $parameter = ParameterFisik::with('category')->where('id',$id)->first();

        $parameter->text = $request->input('teks');
        $parameter->kode_atribut = $request->input('kode_atribut');
        $parameter->positif_statement = $request->input('positif_statement');
        $parameter->negatif_statement = $request->input('negatif_statement');
        $parameter->save();

        return redirect()->route('admin.fisik.parameter',[$parameter->category->fisik_id,$parameter->fisik_parameter_category_id])->with('alert-success', 'Data Berhasil Disimpan.');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterFisik::destroy($id);

    }
}
