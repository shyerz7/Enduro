<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ParameterCategoryPosition;
use App\Models\ParameterPosition;
use App\Models\ParameterPositionGender;
use App\Models\Position;
use Breadcrumbs;
use Session;
use App\Http\Requests\ParameterPositionRequest;
use Excel;
use Validator;

class ParameterPositionController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_posisi, $id_kategori)
    {
        $position = Position::findOrFail($id_posisi);

        $category = ParameterCategoryPosition::findOrFail($id_kategori);

        $parameters = ParameterPosition::with(['parameter_gender' => function ($query) {
            $query->select('*', \DB::raw('IF(gender = 1, "Pria", "Wanita") as gender'));
        }])->where('position_parameter_category_id', '=', $id_kategori)->orderBy('id')->get();

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($position,$category)
        {
            $breadcrumbs->push('List Posisi', route('positions.index'));
            $breadcrumbs->push($position->name, route('admin.posisi.parameter_category.position', $position->id));
            $breadcrumbs->push($category->category, route('admin.posisi.parameter', [$position->id,$category->id]));
        });

        return view('admin.position.parameter.create', compact('position', 'category', 'parameters'))
            	->withPageTitle('Parameter');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id_posisi,$id_kategori, ParameterPositionRequest $request)
    {
        $text = $request->input('teks');
        $parameter_ids = [];
        $parameter_gender = [];
        $report_id = Session::get('report')->id;

        foreach ($text as $i => $value) {
            // $value = variabel untuk menyimpan inputan teks

            // pecah inputan
            $temp = explode(";",$value);
            $kode_atribut = $temp[0];
            $teks = $temp[1];
            $positif_stat = $temp[2];
            $negatif_stat = $temp[3];

            if (trim($value)) {
                $id_parameter = ParameterPosition::tambah($report_id,$id_kategori,$teks,$kode_atribut,$positif_stat,$negatif_stat);

                $parameter_ids[]['id'] = $id_parameter;
            }
        }

        foreach ($parameter_ids as $index => $id_parameter) {
            foreach ($request->input('gender') as $gender) {
                $array = [
                    'id_parameter' => $id_parameter['id'],
                    'gender' => $gender
                ];

                array_push($parameter_gender, $array);
            }
        }

        ParameterPositionGender::tambah($parameter_gender);


        return redirect()->route('admin.posisi.parameter',[$id_posisi,$id_kategori])->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_posisi,$id_kategori,$id)
    {

        $ada_pria   = false;
        $ada_wanita = false;

        $position = Position::findOrFail($id_posisi);

        $category = ParameterCategoryPosition::findOrFail($id_kategori);

        $parameter = ParameterPosition::with('parameter_gender')->findOrFail($id);

        foreach ($parameter['parameter_gender']->toArray() as $value) {
            if($value['gender'] == 1) {
                $ada_pria = true;
            }
            if($value['gender'] == 2) {
                $ada_wanita = true;
            }
        }

        $selected_pria   = $ada_pria;
        $selected_wanita = $ada_wanita;

        return view('admin.position.parameter.edit',compact('position','category', 'parameter', 'selected_pria', 'selected_wanita'))
                ->withPageTitle('Parameter')
                ->withPageDescription('Parameter  Dashboard Panel');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $parameter = ParameterPosition::with('category')->findOrFail($id);

        $parameter->text = $request->input('teks');
        $parameter->kode_atribut = $request->input('kode_atribut');
        $parameter->positif_statement = $request->input('positif_statement');
        $parameter->negatif_statement = $request->input('negatif_statement');
        $parameter->save();

        return redirect()->route('admin.posisi.parameter',[$parameter->category->position_id,$parameter->position_parameter_category_id])->with('alert-success', 'Data Berhasil Disimpan.');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterPosition::destroy($id);

    }

    /**
     * [generateExcelTemplate description]
     * @return [type] [description]
     */
    public function generateExcelTemplate()
    {
        ob_end_clean();  
        
        Excel::create('Template Upload Parameter Posisi',function($excel){

            //set the properties
            $excel->setTitle('Template Upload Parameter Posisi')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data repsonden projek MS');

            $excel->sheet('Data Parameter POsisi',function($sheet){
                $column = [];
                array_push($column,'id_parameter');
                array_push($column,'gender');
                array_push($column,'kode_atribut');
                array_push($column,'atribut');
                array_push($column,'atribut_positif');
                array_push($column,'atribut_negatif');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function importExcel(Request $request)
    {
        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'id_parameter'          =>  'required',
            'gender'                =>  'required',
            'kode_atribut'          =>  'required',
            'atribut'               =>  'required',
            'atribut_positif'       =>  'required',
            'atribut_negatif'       =>  'required',
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $datas_id = [];

        $report_id = \Session::get('report')->id;

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            $parameterdata =  ParameterPosition::firstOrCreate([
                                                    'report_id'                         =>  $report_id,
                                                    'position_parameter_category_id'    =>  $row['id_parameter'],
                                                    'kode_atribut'                      =>  $row['kode_atribut'],
                                                ]); 
            $parameterdata->text                    =   $row['atribut'];                                      
            $parameterdata->positif_statement       =   $row['atribut_positif'];                                      
            $parameterdata->negatif_statement       =   $row['atribut_negatif'];     
            $parameterdata->save();   

            array_push($datas_id,$parameterdata->id);

            /**
             * atur gender
             */
            $parameter_gender = [];
            $gender           = $row['gender'];

            if($gender == 'PRIA')
            {
                
                $data['id_parameter'] = $parameterdata->id; 
                $data['gender']       = 1; 

                ParameterPositionGender::create($data);

            }elseif($gender == 'WANITA')
            {
                $data['id_parameter'] = $parameterdata->id; 
                $data['gender']       = 2; 

                ParameterPositionGender::create($data);

            }else{
                $data['id_parameter'] = $parameterdata->id; 
                $data['gender']       = 1; 

                ParameterPositionGender::create($data);

                $data['id_parameter'] = $parameterdata->id; 
                $data['gender']       = 2; 

                ParameterPositionGender::create($data);
            }


            
        }// end foreach

        // Ambil semua staff yang baru dibuat
        $datas = ParameterPosition::whereIn('id',$datas_id)->get();

        // redirect ke form jika tidak ada buku yang berhasil diimport
        if ($datas->count() == 0) {

            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Tidak ada parameter position yang berhasil diimport."
            ]);

            return redirect()->back();
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport " . $datas->count() . " parameter position."
        ]);

        // Tampilkan index buku
        return redirect()->back();
    }

}
