<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Position;
use App\Http\Requests\PositionRequest;
use Breadcrumbs;

class PositionController extends Controller
{
    public function index()
    {
        $pageTitle = 'Position Panel';

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs)
        {
            $breadcrumbs->push('List Posisi', route('positions.index'));
        });

        $positions = Position::orderBy('id','asc')->get();

        return view('admin.position.index',compact('positions','pageTitle','breadcrumb'));
    }

    public function create()
    {
    	$pageTitle = 'Position Create';

    	return view('admin.position.create',compact('pageTitle'));
    }

    public function store(PositionRequest $request)
    {

    	$data['name']			= $request->input('name');

    	$kota = Position::create($data);

    	return redirect()->route('positions.index')->with('alert-success', 'Data Berhasil Disimpan.');

    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($code)
    {
    	$position = Position::findOrFail($code);

        return view('admin.position.edit',compact('position'))
            ->withPageTitle('position Edit');
    }

    public function update($code, PositionRequest $request)
    {
    	$position = Position::findOrFail($code);
    	$position->name  = $request->input('name');
    	$position->save();

    	return redirect()->route('positions.index')->with('alert-success', 'Data Berhasil Diupdate.');

    }

    public function destroy($code)
    {
    	Position::destroy($code);
    }
}
