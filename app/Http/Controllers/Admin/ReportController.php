<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Report;
use App\Http\Requests\ReportRequest;
use Illuminate\Support\Facades\File;
use Validator;

class ReportController extends Controller
{

    /**
     * Display a listing of the resource.
     * GET /admin/reports
     *
     * @return Response
     */
    public function index()
    {
    	$pageTitle = 'Report Panel';

        $reports = Report::get();

        return view('admin.report.index',compact('reports','pageTitle'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /admin/reports/create
     *
     * @return Response
     */
    public function create()
    {
         return view('admin.report.create')
            ->withPageTitle('Report Create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /admin/reports
     *
     * @return Response
     */
    // public function store(Request $request)
    public function store(ReportRequest $request)
    {

        $data['name']			        = $request->input('name');
        $data['periode_start']	        = $request->input('periode_start');
        $data['periode_end']            = $request->input('periode_end');
        $data['maintenance_mode']       = $request->input('maintenance_mode');
        $data['autosummary_positif']    = $request->input('autosummary_positif');
        $data['autosummary_negatif']	= $request->input('autosummary_negatif');

        $report = Report::create($data);

        if($request->hasFile('kuisioner'))
        {
        	// Mengambil file yang diupload
        	$kuisioner_uploaded = $request->file('kuisioner');

        	 // mengambil extension file
            $extension = $kuisioner_uploaded->getClientOriginalExtension();

             // membuat nama file random berikut extension
            $filename = md5(time()) . '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'kuisioner';
            $kuisioner_uploaded->move($destinationPath, $filename);

            // update data report
            $report->kuisioner = $filename;
            $report->save();
        }

        return redirect()->route('reports.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * [edit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function edit($id)
    {
    	$report = Report::findOrFail($id);

        return view('admin.report.edit',compact('report'))
            ->withPageTitle('Report Edit');
    }

    /**
     * [update description]
     * @param  [type]        $id      [description]
     * @param  ReportRequest $request [description]
     * @return [type]                 [description]
     */
    public function update($id,ReportRequest $request)
    {

        $report = Report::findOrFail($id);
    	$report->name                   = $request->input('name');
    	$report->periode_start          = $request->input('periode_start');
        $report->periode_end            = $request->input('periode_end');
        $report->maintenance_mode       = $request->input('maintenance_mode');
        $report->autosummary_positif    = $request->input('autosummary_positif');
    	$report->autosummary_negatif    = $request->input('autosummary_negatif');
    	$report->save();

    	if($request->hasFile('kuisioner'))
        {
        	// Mengambil file yang diupload
        	$filename = null;
        	$kuisioner_uploaded = $request->file('kuisioner');

        	 // mengambil extension file
            $extension = $kuisioner_uploaded->getClientOriginalExtension();

             // membuat nama file random berikut extension
            $filename = md5(time()) . '.' . $extension;

            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'kuisioner';
            $kuisioner_uploaded->move($destinationPath, $filename);

            if ($report->kuisioner) {
                $old_kuisioner = $report->kuisioner;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'kuisioner'. DIRECTORY_SEPARATOR .$old_kuisioner;

                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }

             // hapus cover lama, jika ada

            // update data report
            $report->kuisioner = $filename;
            $report->save();

            // if($report->id == \Session::get('report')->id)
            // {
            //     \Session::forget('report');
            //     \Session::put('report', $report);
            // }

        }

        return redirect()->route('reports.index')->with('alert-success', 'Data Berhasil Diupdate.');
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {

        $report = Report::findOrFail($id);

        $kuisioner = $report->kuisioner;

        if ($kuisioner) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'kuisioner'. DIRECTORY_SEPARATOR .$kuisioner;

            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }

        Report::destroy($id);



	}

    /**
     * [downloadKuisioner description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
	public function downloadKuisioner($id)
	{
		$report = Report::findOrFail($id);

		$kuisioner = $report->kuisioner;
		$filepath = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'kuisioner'. DIRECTORY_SEPARATOR .$kuisioner;
		if(\File::exists($filepath))
		{
			return \Response::download($filepath);
		}

	}

    public function changeSession($id_report)
    {

        /**
         * cek jika route di admin/home
         */
        // if(\Request::segment(2) == 'home')
        // {
        //     return redirect()->back();
        // }

        $report = Report::findOrFail($id_report);

        \Session::forget('report');

        \Session::put('report', $report);
        // dd(\Session::get('report')->toArray());

        return redirect()->back();
    }
}
