<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FisikStore;
use App\Models\Fisik;
use App\Models\Store;
use App\Models\Report;
use App\Models\ReportFisikStore;
use App\Models\ParameterFisikReport;
use App\Models\ReportPerformanceFisikStore;
use Breadcrumbs;
use Session;

class ReportFisikStoreController extends Controller
{
    public function stores() {

        $pageTitle = 'List ';


        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs)
        {
            $breadcrumbs->push('List Store', route('admin.reportfisikstores.stores'));
        });

        $stores = Store::whereNull('is_main')->with('kota')->get();

        return view('admin.report_fisikstore.store',compact('stores','pageTitle','breadcrumb'));
    }

    public function index($code_store)
    {
    	$store = Store::findOrFail($code_store);

    	$pageTitle = 'Store '.$store->name;

    	$breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($store)
        {
            $breadcrumbs->push('List Store', route('admin.reportfisikstores.stores'));
            $breadcrumbs->push($store->name, route('admin.reportfisikstores.index', $store->code));
        });

    	$fisikstores = FisikStore::where('report_id',Report::getCurrentSession()->id)
        						  ->where('code_store',$code_store)
        						  ->with(['fisik','store','score'])
        						  ->get();

        return view('admin.report_fisikstore.index',compact('store','pageTitle','breadcrumb','fisikstores'));						  
    }

    public function edit($id)
    {
    	$report_id = Report::getCurrentSession()->id;

    	$pageTitle = 'Isikan Form Index ';

    	$fisikstore = FisikStore::where('id',$id)
        						  ->with([
                                        'fisik',
                                        'store',
                                        'score',
                                        'fisik.parameter_category.parameter_data'
                                    ])
        						  ->first();

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($fisikstore)
        {
            $breadcrumbs->push('List Store', route('admin.reportfisikstores.stores'));
            $breadcrumbs->push($fisikstore->store->name, route('admin.reportfisikstores.index', $fisikstore->store->code));
        });						  

        return view('admin.report_fisikstore.form',compact('report_id','pageTitle','breadcrumb','fisikstore'));	


    }

    public function update(Request $request,$id)
    {
    	$report_id = Session::get('report')->id;

    	$fisikstore = FisikStore::findOrFail($id);

    	$reportfisikstore = ReportFisikStore::firstOrCreate(['report_id'=>$report_id,'fisik_store_id'=>$fisikstore->id]);					      

        $reportfisikstore->score = $request->input('score');
        $reportfisikstore->save();	

        /**
         * jika ada inputan atribut
         */
        if($request->has('komentar'))
        {
            //loop komentar
            $komentar = $request->input('komentar'); 
            foreach($komentar as $i => $komentar)
            {
                //$i = id parameter
                //ambil input komentar
                $parameter = $request->input('parameter');

                //cek data report parameter di db
                $cek = ParameterFisikReport::where('report_id', '=', $report_id)
                                              ->where('fisik_store_id', '=', $fisikstore->id)
                                              ->where('fisik_parameter_data_id', '=', $i)
                                              ->first();

                //sudah ada
                if(count($cek))
                {
                    $cek->status  = $parameter[ $i ];
                    $cek->comment = $komentar;
                    $cek->save();
                }
                //belum ada
                else
                {
                    ParameterFisikReport::create([
                                                        'report_id'                  => $report_id,
                                                        'fisik_store_id'             => $fisikstore->id,
                                                        'fisik_parameter_data_id'    => $i,
                                                        'status'                     => $parameter[ $i ],
                                                        'comment'                    => $komentar
                                                    ]);
                }
            }
        }

        /**
         * input report performance
         */
        if($request->has('performance'))
        {
            $performances = $request->input('performance'); 
            foreach($performances as $i => $performance)
            {
                $data = $request->input('performance');

                $data_performance = ReportPerformanceFisikStore::firstOrCreate(['report_id'=>$report_id,'fisik_store_id'=>$fisikstore->id,'category_id'=>$i]);

                $data_performance->score = $data[$i];
                $data_performance->save();

            }

        }


        return redirect()->route('admin.reportfisikstores.index',$fisikstore->code_store)->with('alert-success', 'Data Berhasil Diupdate.');								      		

    }
}
