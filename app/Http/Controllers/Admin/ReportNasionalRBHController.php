<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ReportNasionalRBH;
use App\Models\Kota;
use Session;

class ReportNasionalRBHController extends Controller
{
    public function edit()
    {
    	// data report
        $report = Session::get('reportfrontend');

    	return view('admin.reportnasionalrbh.edit',compact('report'));
    }

    public function store(Request $request)
    {
    	// nilai nasional
    	$report = Session::get('reportfrontend');

    	$nasionalscore = ReportNasionalRBH::where('report_id',$report->id)
                                                                       ->whereNotNull('is_nasional')
                                                                       ->first();


        if(!is_null($nasionalscore)){
        	$nasionalscore->score = $request->input('nasional');
        	$nasionalscore->save();
        }else{
        	$data['report_id']		=	$report->id;
        	$data['is_nasional']	=	true;
        	$data['score']	        =	$request->input('nasional');
        	$reportnasional = ReportNasionalRBH::create($data);
        }      


        $kotas = Kota::get();

        foreach ($kotas as $kota) {

        	$rbhscore = ReportNasionalRBH::where('report_id',$report->id)
                                                                       ->where('kota_code',$kota->code)
                                                                       ->first();
            if(!is_null($rbhscore)){
	        	$rbhscore->score = $request->input($kota->code);
	        	$rbhscore->save();
	        }else{
	        	$datarbh['report_id']		=	$report->id;
	        	$datarbh['kota_code']	    =	$kota->code;
	        	$datarbh['score']	        =	$request->input($kota->code);
	        	ReportNasionalRBH::create($datarbh);
	        }                                                                 
        }

        return redirect()->route('admin.reportnasionalrbh.edit');

    }
}
