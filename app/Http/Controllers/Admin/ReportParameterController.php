<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ReportParameter;
use App\Models\Kota;

class ReportParameterController extends Controller
{
    public function index()
    {
    	$reportnasionalget = ReportParameter::where('is_nasional',true)->first();

    	$reportnasionalpeople = null;
    	$reportnasionaltangible = null;
    	if(!is_null($reportnasionalget)) {
    		$reportnasionalpeople = json_decode($reportnasionalget->report_people,true);
    		$reportnasionaltangible = json_decode($reportnasionalget->report_tangible,true);
    	}	

    	$kotas = Kota::get();

    	return view('admin.reportparameter.index',compact('reportnasionalget','reportnasionalpeople','reportnasionaltangible','kotas'));
    }

    public function storeNasional(Request $request)
    {	
    	/**
    	 * credentials
    	 */
    	
    	$data = ReportParameter::firstOrCreate(['is_nasional'=>true]);

    	$array_report_people = [];
    	array_push($array_report_people,array("cs"=>$request->input('cs')));
    	array_push($array_report_people,array("teller"=>$request->input('teller')));
    	array_push($array_report_people,array("satpam"=>$request->input('satpam')));
    	$data->report_people	=	json_encode($array_report_people);

    	$array_report_tangible = [];
    	array_push($array_report_tangible,array("gedung"=>$request->input('gedung')));
    	array_push($array_report_tangible,array("toilet"=>$request->input('toilet')));
    	array_push($array_report_tangible,array("atm"=>$request->input('atm')));
    	$data->report_tangible	=	json_encode($array_report_tangible);

    	$data->report_mc	=	$request->input('mc');
    	$data->save();

    	return redirect()->route('admin.reportparameter.index')->with('alert-success','Data Report Parameter Nasional');


    }

    public function getRbh($id_rbh) {
    	$rbh = Kota::findOrFail($id_rbh);

    	$reportrbhget = ReportParameter::where('kota_code',$rbh->code)->first();

    	$reportrbhgetpeople = null;
    	$reportrbhgettangible = null;
    	if(!is_null($reportrbhget)) {
    		$reportrbhgetpeople = json_decode($reportrbhget->report_people,true);
    		$reportrbhgettangible = json_decode($reportrbhget->report_tangible,true);
    	}	

    	return view('admin.reportparameter.rbhreport',compact('rbh','reportrbhget','reportrbhgetpeople','reportrbhgettangible'));
    }

    public function storeRbh(Request $request,$code) {

    	/**
    	 * credentials
    	 */
    	
    	$rbh = Kota::findOrFail($code);
    	
    	$data = ReportParameter::firstOrCreate(['kota_code'=>$rbh->code]);

    	$array_report_people = [];
    	array_push($array_report_people,array("cs"=>$request->input('cs')));
    	array_push($array_report_people,array("teller"=>$request->input('teller')));
    	array_push($array_report_people,array("satpam"=>$request->input('satpam')));
    	$data->report_people	=	json_encode($array_report_people);

    	$array_report_tangible = [];
    	array_push($array_report_tangible,array("gedung"=>$request->input('gedung')));
    	array_push($array_report_tangible,array("toilet"=>$request->input('toilet')));
    	array_push($array_report_tangible,array("atm"=>$request->input('atm')));
    	$data->report_tangible	=	json_encode($array_report_tangible);

    	$data->report_mc	=	$request->input('mc');
    	$data->save();

    	return redirect()->route('admin.reportparameter.index')->with('alert-success','Data Report Parameter Nasional');
    }
}
