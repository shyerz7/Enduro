<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\Staff;
use App\Models\ReportStaff;
use App\Models\Position;
use App\Models\File;
use App\Models\ParameterReportPosition;
use Breadcrumbs;
use App\Http\Requests\ReportStaffRequest;
use Session;
use Excel;
use Validator;
use App\Models\ReportPerformanceStaff;
use App\Models\PerformanceCategory;

class ReportStaffController extends Controller
{
    public function stores() {

       $pageTitle = 'List ';


        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs)
        {
            $breadcrumbs->push('List Store', route('admin.reportstaffs.stores'));
        });

        $stores = Store::whereNull('is_main')->get();
        $positions = Position::get();

        $positionsList = Position::pluck('name','id');

        $performances = PerformanceCategory::where('report_id',\Session::get('report')->id)->get();

        return view('admin.report_staff.store',compact('stores','pageTitle','breadcrumb','positions','positionsList','performances'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {
        $pageTitle = 'Staff Panel';
        $report_id = Session::get('report')->id;
        $store = Store::findOrFail($code);

        $staffs = Staff::with(['position','reportstaff'])->where('code_store',$store->code)
                                                         ->where('report_id',$report_id)
                                                         ->get();

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($store)
        {
            $breadcrumbs->push('List Store', route('admin.reportstaffs.stores'));
            $breadcrumbs->push($store->name, route('admin.reportstaffs.index', $store->code));
        });


        return view('admin.report_staff.index',compact('store','pageTitle','staffs','breadcrumb'));
    }

    public function edit($id)
    {
        $report_id = Session::get('report')->id;

        $staff = Staff::with([
                                'store',
                                'reportstaff',
                                'position',
                                'position.parameter_category.parameter_data.parameter_gender',
                            ])->findOrFail($id);


        $pageTitle = 'Edit Staff '.$staff->nama;

        $files = File::where('report_id',$report_id)
                       ->where('staff_id',$staff->id)
                       ->get();

        return view('admin.report_staff.form',compact('staff','pageTitle','files'));

    }

    public function update(ReportStaffRequest $request,$code)
    {
        $report_id = Session::get('report')->id;

        $staff = Staff::findOrFail($code);

        $store = Store::findOrFail($staff->code_store);

        /**
         * save to report_staff table
         * @var [type]
         */
        $reportstaff = ReportStaff::firstOrCreate(['report_id'=>$report_id,'staff_id'=>$staff->id]);
        $reportstaff->score   =   $request->input('score');
        $reportstaff->summary =   $request->input('summary');
        $reportstaff->save();

        /**
         * jika ada inputan atribut
         */
        if($request->has('komentar'))
        {
            //loop komentar
            $komentar = $request->input('komentar');
            foreach($komentar as $i => $komentar)
            {
                //$i = id parameter
                //ambil input komentar
                $parameter = $request->input('parameter');

                //cek data report parameter di db
                $cek = ParameterReportPosition::where('report_id', '=', $report_id)
                                              ->where('staff_id', '=', $staff->id)
                                              ->where('position_parameter_data_id', '=', $i)
                                              ->first();

                //sudah ada
                if(count($cek))
                {
                    $cek->status  = $parameter[ $i ];
                    $cek->comment = $komentar;
                    $cek->save();
                }
                //belum ada
                else
                {
                    ParameterReportPosition::create([
                                                        'report_id'                  => $report_id,
                                                        'staff_id'                   => $staff->id,
                                                        'position_parameter_data_id' => $i,
                                                        'status'                     => $parameter[ $i ],
                                                        'comment'                    => $komentar
                                                    ]);
                }
            }
        }

        /**
         * input report performance
         */
        // if($request->has('performance'))
        // {
        //     $performances = $request->input('performance');
        //     foreach($performances as $i => $performance)
        //     {
        //         $data = $request->input('performance');

        //         $data_performance = ReportPerformanceStaff::firstOrCreate(['report_id'=>$report_id,'staff_id'=>$staff->id,'category_id'=>$i]);

        //         $data_performance->score = $data[$i];
        //         $data_performance->save();

        //     }

        // }

        /**
         * jika da attachment
         */
        if($request->hasFile('file'))
        {

            $file = File::where('report_id',$report_id)
                       ->where('staff_id',$staff->id)
                       ->first();

            if(is_null($file)){
                $file = new File();
                $file->report_id = $report_id;
                $file->staff_id  = $staff->id;
            }

            // Mengambil file yang diupload
            $file_uploaded = $request->file('file');

            $extension = $file_uploaded->getClientOriginalExtension();

            $destinationMP3     = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'file'. DIRECTORY_SEPARATOR .'mp3';
            $destinationVideo   = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'file'. DIRECTORY_SEPARATOR .'video';

            $filename = $staff->nama.'-'.substr( md5(rand()), 0, 7) . '.' . $extension;

            if($extension == 'mp4')
            {
                $file_uploaded->move($destinationVideo,$filename);
                $file->filetype = 'mp4';

            }elseif($extension == 'mp3')
            {
                $file_uploaded->move($destinationMP3,$filename);
                $file->filetype = 'mp3';
            }

            $file->filename = $filename;
            $file->save();
        }

        return redirect()->route('admin.reportstaffs.index',$store->code)->with('alert-success', 'Data Berhasil Diupdate.');
    }

    /**
     * [generateExcelTemplatePosition description]
     * @param  [type] $id_posisi [description]
     * @return [type]            [description]
     */
    public function generateExcelTemplatePosition($id_posisi)
    {

        ob_end_clean();


        Excel::create('Template Upload Report Staff',function($excel) use($id_posisi) {


            $position = Position::with('parameter_category.parameter_data')->findOrFail($id_posisi);

            //set the properties
            $excel->setTitle('Template Upload Report Store')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data repsonden projek MS');

            $excel->sheet('Data Report Staff',function($sheet) use ($position){
                $column = [];

                array_push($column,'kode_staff');
                array_push($column,'gender');
                array_push($column,'score');

                if(!is_null($position['parameter_category']))
                {
                    foreach ($position['parameter_category'] as $key => $category) {
                        array_push($column,$category->category);

                        $parameter = $category['parameter_data'];

                        if(count($parameter))
                        {
                            foreach($parameter as $param)
                            {
                                array_push($column, $param->kode_atribut);
                                array_push($column, $param->kode_atribut.'_c');
                            }

                        }

                    }
                }

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    /**
     * [generateExcelTemplatePosition description]
     * @param  [type] $id_posisi [description]
     * @return [type]            [description]
     */
    public function generateExcelTemplateScore()
    {

        ob_end_clean();


        Excel::create('Template Upload Report Staff Score',function($excel)  {


            //set the properties
            $excel->setTitle('Template Upload Report Staff Score')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data repsonden projek MS');

            $excel->sheet('Data Report Staff Score',function($sheet){
                $column = [];

                array_push($column,'kode_staff');
                array_push($column,'score');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    /**
     * [importExcelReport description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function importExcelReport(Request $request)
    {


        $report_id = \Session::get('report')->id;



        $position = Position::with('parameter_category.parameter_data')->findOrFail($request->input('position_id'));

        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kode_staff'    =>  'required',
            'gender'        =>  'required'
            // 'score'         =>  'required'
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $staff_id = [];

          //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {
            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $staff              = Staff::where('report_id',$report_id)
                                        ->where('kode_staff',$row['kode_staff'])
                                        ->first();

            if(count($staff))
            {
                /**
             * save to report_staff table
             * @var [type]
             */
            $reportstaff = ReportStaff::firstOrCreate(['report_id'=>$report_id,'staff_id'=>$staff->id]);
            // $reportstaff->score   =   $row['score'];
            // $reportstaff->summary =   $request->input('summary');
            // $reportstaff->save();

            $reportstaff->summary  = '<p style="text-align:center"><strong>SUMMARY</strong></p>';

            /**
             * update ceklis
             */
            if(!is_null($position['parameter_category']))
            {
                $array_negatif_statement = [];

                // $temp_negatif_stement = '';
                // $temp_positif_stement = '';

                foreach ($position['parameter_category'] as $key => $category) {

                    $parameter = $category['parameter_data'];

                    if(count($parameter))
                    {


                        $temp_negatif_stement = '<ul>';
                        $temp_positif_stement = '<ul>';
                        $ada_negatif_stement = false;
                        $ada_positif_stement = false;

                        foreach($parameter as $param)
                        {

                            $ada_gender   = false;
                            $parameter_gender = 0;

                            foreach ($param['parameter_gender']->toArray() as $value) {
                                if($value['gender'] == $row['gender']) {
                                    $ada_gender = true;
                                    break;
                                }
                            }

                            if($ada_gender)
                            {
                                $value = ParameterReportPosition::where('staff_id', '=', $staff->id)
                                                            ->where('position_parameter_data_id', '=', $param->id)
                                                            ->where('report_id', '=', $report_id)
                                                            ->first();

                                //sudah ada
                                if(count($value))
                                {
                                    // $value->status  = (int)$row[$param->kode_atribut];
                                    $value->status  = (int)$row[$param->kode_atribut];
                                    // $value->comment = $row[$param->kode_atribut.'_c'];
                                    $value->save();
                                }
                                //belum ada
                                else
                                {

                                    $value = ParameterReportPosition::create([
                                                                        'report_id'                  => $report_id,
                                                                        'staff_id'                   => $staff->id,
                                                                        'position_parameter_data_id' => $param->id,
                                                                        'status'                     => (int)$row[$param->kode_atribut],
                                                                        // 'comment'                    => $row[$param->kode_atribut.'_c'],
                                                                    ]);


                                }

                                if(\Session::get('report')->autosummary_positif == 1)
                                {
                                    //jika ya
                                    if((int)$row[$param->kode_atribut] == 1)
                                    {

                                        $temp_positif_stement .= '<li style="text-align:justify"><span style="color:#2ecc71">';
                                        $temp_positif_stement .= $param->positif_statement;
                                        $temp_positif_stement .= '</span></li>';
                                        $ada_positif_stement = true;
                                        // array_push($array_negatif_statement, $param->negatif_statement);

                                    }
                                }

                                if(\Session::get('report')->autosummary_negatif == 1)
                                {
                                    //jika tidsak
                                    if((int)$row[$param->kode_atribut] == 2)
                                    {

                                        $temp_negatif_stement .= '<li style="text-align:justify"><span style="color:#FF0000">';
                                        $temp_negatif_stement .= $param->negatif_statement;
                                        $temp_negatif_stement .= '</span></li>';
                                        $ada_negatif_stement = true;
                                        // array_push($array_negatif_statement, $param->negatif_statement);

                                    }
                                }


                            }

                        }

                        $temp_negatif_stement .= '</ul>';
                        $temp_positif_stement .= '</ul>';

                        if($ada_positif_stement) $reportstaff->summary .= $temp_positif_stement;
                        if($ada_negatif_stement) $reportstaff->summary .= $temp_negatif_stement;

                        $reportstaff->save();

                    }

                }

                /**
                 * input summary
                 */
                // $reportstaff->summary = json_encode($array_negatif_statement);

            }
            }

        }


        return redirect()->route('admin.reportstaffs.stores')->with('alert-success', 'Data Berhasil Diupdate.');

    }

    /**
     * [importExcelReport description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function importExcelReportScore(Request $request)
    {
        $report_id = \Session::get('report')->id;

        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kode_staff'    =>  'required',
            'score'         =>  'required'
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $staff_id = [];

          //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $staff              = Staff::where('report_id',$report_id)
                                        ->where('kode_staff',$row['kode_staff'])
                                        ->first();

            if(!is_null($staff)) {
                $reportstaff = ReportStaff::firstOrCreate(['report_id'=>$report_id,'staff_id'=>$staff->id]);
                $reportstaff->score   =   $row['score'];
                $reportstaff->save();
            }



        }


        return redirect()->route('admin.reportstaffs.stores')->with('alert-success', 'Data Berhasil Diupdate.');

    }

     public function importExcelReportScoreIndividu(Request $request)
    {
        $report_id = \Session::get('report')->id;

        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kode_staff'    =>  'required',
            'score'         =>  'required'
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $staff_id = [];

          //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $staff              = Staff::where('report_id',$report_id)
                                        ->where('kode_staff',$row['kode_staff'])
                                        ->first();


            if(!is_null($staff)) {
                $reportstaff = ReportStaff::firstOrCreate(['report_id'=>$report_id,'staff_id'=>$staff->id]);
                $reportstaff->score_individu   =   $row['score'];
                $reportstaff->save();

            }



        }


        return redirect()->route('admin.reportstaffs.stores')->with('alert-success', 'Data Berhasil Diupdate.');

    }

    /**
     * [destroyFile description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroyFile($id)
    {
        $file = File::findOrFail($id);

        if($file->filetype == 'mp3')
        {
            $destination   = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'file'. DIRECTORY_SEPARATOR .'mp3';
        }elseif($file->filetype == 'mp4'){
            $destination   = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'file'. DIRECTORY_SEPARATOR .'video';
        }

        $storedFile = $destination.DIRECTORY_SEPARATOR.$file->filename;

        try {
            \File::delete($storedFile);
        } catch (FileNotFoundException $e) {
            // File sudah dihapus/tidak ada
        }

        File::destroy($id);
    }

    /**
     * [generateExcelTemplateValue description]
     * @return [type] [description]
     */
    public function generateExcelTemplateValue()
    {
        ob_end_clean();


        Excel::create('Template Upload Multimedia Report Staff',function($excel){


            //set the properties
            $excel->setTitle('Template Upload Report Staff File')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data file repsonden projek MS');

            $excel->sheet('Data Report File Staff',function($sheet) {
                $column = [];

                array_push($column,'kode_staff');
                array_push($column,'filetype');
                array_push($column,'filename');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function importFileMultimediaViaExcel(Request $request)
    {
         // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kode_staff' =>  'required',
            'filetype'   =>  'required',
            'filename'   =>  'required',
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $files_id = [];

        $report_id = Session::get('report')->id;

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $staff = Staff::where('report_id',$report_id)
                            ->where('kode_staff',$row['kode_staff'])
                            ->first();

            $file = File::where('report_id',$report_id)
                       ->where('staff_id',$staff->id)
                       ->first();

            if(is_null($file)){
                $file = new File();
                $file->report_id = $report_id;
                $file->staff_id  = $staff->id;
            }

            if(!is_null($staff))
            {
                $file->filename = $row['filename'];
                $file->filetype = $row['filetype'];
                $file->save();

                array_push($files_id,$file->id);
            }

        }// end foreach

        // Ambil semua staff yang baru dibuat
        $files = File::whereIn('id',$files_id)->get();

        // redirect ke form jika tidak ada buku yang berhasil diimport
        if ($files->count() == 0) {

            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Tidak ada report staff yang berhasil diimport."
            ]);

            return redirect()->back();
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport " . $files->count() . " staff."
        ]);

        // Tampilkan index buku
        return redirect()->back();
    }

    /**
     * [generateExcelTemplateValue description]
     * @return [type] [description]
     */
    public function generateExcelTemplateScorePerformance()
    {
        ob_end_clean();


        Excel::create('Template Upload Multimedia Report Staff Performance',function($excel){


            //set the properties
            $excel->setTitle('Template Upload Report Staff File')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data file repsonden projek MS');

            $excel->sheet('Data Report File Staff',function($sheet) {
                $column = [];

                array_push($column,'kode_staff');
                array_push($column,'kode_performance');
                array_push($column,'score');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    /**
     * [importExcelReport description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function importExcelReportScorePerformance(Request $request)
    {
        $report_id = \Session::get('report')->id;

        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kode_staff'    =>  'required',
            'score'         =>  'required',
            'kode_performance'         =>  'required'
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $staff_id = [];

          //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajut,nya
            if ($validator->fails()) continue;

            // credentials data
            $staff              = Staff::where('report_id',$report_id)
                                        ->where('kode_staff',$row['kode_staff'])
                                        ->first();

            if(!is_null($staff)) {

                $cat =  PerformanceCategory::where('kode',$row['kode_performance'])
                                             ->where('report_id',$report_id)
                                             ->first();

                if (!is_null($cat)) {

                    $data = $request->input('performance');

                    $data_performance = ReportPerformanceStaff::firstOrCreate(['report_id'=>$report_id,'staff_id'=>$staff->id,'category_id'=>$cat->id]);

                    $data_performance->score = $row['score'];
                    $data_performance->save();

                }

            }



        }


        return redirect()->route('admin.reportstaffs.stores')->with('alert-success', 'Data Berhasil Diupdate.');

    }
}
