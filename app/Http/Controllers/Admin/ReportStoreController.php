<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\ReportStore;
use App\Http\Requests\ReportStoreRequest;
use Excel;
use Validator;
use Session;

class ReportStoreController extends Controller
{
    public function index()
    {
    	$stores = Store::with(['reportstore'])->whereNull('is_main')->get();

        return view('admin.report_store.index',compact('stores','pageTitle'));
    }

     /**
     * [generateExcelTemplate description]
     * @return [type] [description]
     */
    public function generateExcelTemplate()
    {
        ob_end_clean();  
        
        Excel::create('Template Upload Report Store',function($excel){


            //set the properties
            $excel->setTitle('Template Upload Report Store')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data repsonden projek MS');

            $excel->sheet('Data Report Store',function($sheet){
                $column = [];
                array_push($column,'kode_store');
                array_push($column,'score');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    /**
     * [importExcel description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function importExcel(Request $request)
    {
        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kode_store'  =>  'required',
            'score'  	  =>  'required',
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $reports_id = [];

        $report_id = Session::get('report')->id;

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $store              = Store::findOrFail($row['kode_store']);

            if($store)
            {
            	$reportstore = ReportStore::firstOrCreate(['report_id'=>$report_id,'code_store'=>$store->code]);
            	$reportstore->score = 	$row['score'];
            	$reportstore->save();

                array_push($reports_id,$reportstore->id);
            }

            
        }// end foreach

        // Ambil semua staff yang baru dibuat
        $reportstores = ReportStore::whereIn('id',$reports_id)->get();

        // redirect ke form jika tidak ada buku yang berhasil diimport
        if ($reportstores->count() == 0) {

            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Tidak ada report store yang berhasil diimport."
            ]);

            return redirect()->back();
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport " . $reportstores->count() . " staff."
        ]);

        // Tampilkan index buku
        return redirect()->back();
    }

    public function edit($code)
    {
    	$report_id = Session::get('report')->id;
    	$store = Store::with(['kota','reportstore'])->where('code',$code)->first();

    	return view('admin.report_store.edit',compact('report_id','store'));							
    }

    public function update(ReportStoreRequest $request,$code)
    {
    	$report_id = Session::get('report')->id;
    	$store = Store::findOrFail($code);
    	$reportstore = ReportStore::firstOrCreate(['report_id'=>$report_id,'code_store'=>$store->code]);
    	$reportstore->score = 	$request->input('score');
    	$reportstore->save();

    	return redirect()->route('reportstore.index')->with('alert-success', 'Data Berhasil Diupdate.');
    }
}
