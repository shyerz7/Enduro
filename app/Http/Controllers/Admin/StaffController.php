<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\Staff;
use App\Models\Position;
use Breadcrumbs;
use App\Http\Requests\StaffRequest;
use Session;
use Image;
use Excel;
use Validator;

class StaffController extends Controller
{

    public function stores() {

        $pageTitle = 'List ';


        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs)
        {
            $breadcrumbs->push('List Store', route('admin.staffs.stores'));
        });

        $stores = Store::whereNull('is_main')->get();
        $positions = Position::get();

        return view('admin.staff.store',compact('stores','pageTitle','breadcrumb','positions'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {
        $pageTitle = 'Staff Panel';
        $report_id = Session::get('report')->id;
        $store = Store::findOrFail($code);

        $staffs = Staff::with('position')->where('code_store',$store->code)
                                         ->where('report_id',$report_id)
                                         ->get();

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($store)
        {
            $breadcrumbs->push('List Store', route('admin.staffs.stores'));
            $breadcrumbs->push($store->name, route('admin.staffs.index', $store->code));
        });

        return view('admin.staff.index',compact('store','pageTitle','staffs','breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($code)
    {
        $pageTitle = 'Staff Create';

        $store = Store::findOrFail($code);

        $positions = Position::pluck('name','id');

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($store)
        {
            $breadcrumbs->push('List Store', route('admin.staffs.stores'));
            $breadcrumbs->push($store->name, route('admin.staffs.index', $store->code));
        });

        return view('admin.staff.create',compact('store','pageTitle','breadcrumb','positions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaffRequest $request,$code)
    {
        $data['report_id']      = Session::get('report')->id;
        $data['position_id']    = $request->input('position_id');
        $data['code_store']     = $code;
        $data['kode_staff']     = $request->input('kode_staff');
        $data['nama']           = $request->input('nama');
        $data['gender']         = $request->input('gender');

        $staff = Staff::create($data);

        $store = Store::findOrFail($code);

        if($request->hasFile('ava'))
        {
            // Mengambil file yang diupload
            $ava_uploaded = $request->file('ava');

             // mengambil extension file
            $extension = $ava_uploaded->getClientOriginalExtension();

            $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'origin';
            $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'thumb';
            $destinationTable  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'table';

            $filename = substr( md5(rand()), 0, 3) . '.' . $extension;

            /**
             * membuat ava origin
             */
            $ava_uploaded->move($destinationOrigin, $filename);

            /**
             * membuat ava thumbnail
             */
            $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
            $img->resize(150, 150, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationThumb.'/'.$filename);


            /**
             * membuat ava table
             */
            $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
            $img->resize(50,50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationTable.'/'.$filename);

            // update ava store original
            $staff->ava = $filename;
            $staff->save();
        }

        return redirect()->route('admin.staffs.index',$store->code)->with('alert-success', 'Data Berhasil Disimpan.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pageTitle = 'Staff Edit';

        $staff = Staff::findOrFail($id);

        $store = Store::findOrFail($staff->code_store);

        $positions = Position::pluck('name','id');

        $breadcrumb = Breadcrumbs::register('breadcrumb', function($breadcrumbs) use($store)
        {
            $breadcrumbs->push('List Store', route('admin.staffs.stores'));
            $breadcrumbs->push($store->name, route('admin.staffs.index', $store->code));
        });

        return view('admin.staff.edit',compact('staff','store','pageTitle','breadcrumb','positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StaffRequest $request,$id)
    {
        $staff = Staff::findOrFail($id);

        $staff->position_id =  $request->input('position_id');
        $staff->kode_staff  =  $request->input('kode_staff');
        $staff->nama        =  $request->input('nama');
        $staff->gender      =  $request->input('gender');

        $staff->save();

        $store = Store::findOrFail($staff->code_store);

        if($request->hasFile('ava'))
        {
            // Mengambil file yang diupload
            $ava_uploaded = $request->file('ava');

             // mengambil extension file
            $extension = $ava_uploaded->getClientOriginalExtension();

            $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'origin';
            $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'thumb';
            $destinationTable  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'table';

            $filename = substr( md5(rand()), 0, 3) . '.' . $extension;

            $ava_uploaded->move($destinationOrigin, $filename);

            /**
             * membuat ava thumbnail
             */
            $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationThumb.'/'.$filename);

            /**
             * membuat ava table
             */
            $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
            $img->resize(50,50, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationTable.'/'.$filename);

            if($staff->ava)
            {
                $old_ava = $store->ava;
                $old_origin_ava     = $destinationOrigin. DIRECTORY_SEPARATOR .$old_ava;
                $old_thumb_ava      = $destinationThumb. DIRECTORY_SEPARATOR .$old_ava;
                $old_thumb_table    = $destinationTable. DIRECTORY_SEPARATOR .$old_ava;

                //hapus dari folde origin
                try {
                    \File::delete($old_origin_ava);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }


                //hapus dari folder thumb
                try {
                    \File::delete($old_thumb_ava);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }

                //hapus dari folder table
                try {
                    \File::delete($old_thumb_table);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }



            }

            // update ava store original
            $staff->ava = $filename;
            $staff->save();

        }


        return redirect()->route('admin.staffs.index',$store->code)->with('alert-success', 'Data Berhasil Diupdate.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff::findOrFail($id);

        if($staff->ava)
        {
            $old_ava = $staff->ava;

            $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'origin';
            $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'thumb';
            $destinationTable  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'table';

            $old_origin_ava = $destinationOrigin. DIRECTORY_SEPARATOR .$old_ava;
            $old_thumb_ava  = $destinationThumb. DIRECTORY_SEPARATOR .$old_ava;
            $old_table_ava  = $destinationTable. DIRECTORY_SEPARATOR .$old_ava;

            //hapus dari folde origin
            try {
                \File::delete($old_origin_ava);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }


            //hapus dari folder thumb
            try {
                \File::delete($old_thumb_ava);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }

            //hapus dari folder thumb
            try {
                \File::delete($old_table_ava);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }

        }

        Staff::destroy($id);
    }

    /**
     * [generateExcelTemplate description]
     * @return [type] [description]
     */
    public function generateExcelTemplate()
    {
        ob_end_clean();

        Excel::create('Template Upload Report Staff',function($excel){

            //set the properties
            $excel->setTitle('Template Upload Report Staff')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data repsonden projek MS');

            $excel->sheet('Data Staff',function($sheet){
                $column = [];
                array_push($column,'kode_posisi');
                array_push($column,'kode_store');
                array_push($column,'kode_staff');
                array_push($column,'nama');
                array_push($column,'gender');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function importExcel(Request $request)
    {
        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kode_posisi' =>  'required',
            'kode_store'  =>  'required',
            'kode_staff'  =>  'required',
            'nama'        =>  'required',
            'gender'      =>  'required',
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $staffs_id = [];

        $report_id = Session::get('report')->id;

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            if ($validator->fails()) continue;

            // credentials data
            $position           = Position::findOrFail($row['kode_posisi']);
            $store              = Store::findOrFail($row['kode_store']);

            if($position && $store)
            {

                //
                $staff = Staff::firstOrCreate([
                                                'report_id'     => $report_id,
                                                'position_id'   => $position->id,
                                                'code_store'    => $store->code,
                                                'kode_staff'    => $row['kode_staff'],
                                            ]);

                $staff->nama    = $row['nama'];
                $staff->gender  = $row['gender'];
                $staff->save();

                // create staff baru
                // $staff = Staff::create([
                //     'report_id'     => $report_id,
                //     'position_id'   => $position->id,
                //     'code_store'    => $store->code,
                //     'kode_staff'    => $row['kode_staff'],
                //     'nama'          => $row['nama'],
                //     'gender'        => $row['gender'],
                // ]);

                array_push($staffs_id,$staff->id);
            }


        }// end foreach

        // Ambil semua staff yang baru dibuat
        $staffs = Staff::whereIn('id',$staffs_id)->get();

        // redirect ke form jika tidak ada buku yang berhasil diimport
        if ($staffs->count() == 0) {

            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Tidak ada staff yang berhasil diimport."
            ]);

            return redirect()->back();
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport " . $staffs->count() . " staff."
        ]);

        // Tampilkan index buku
        return redirect()->back();
    }


    public function importAva(Request $request)
    {
        if($request->hasFile('ava'))
        {
            $report_id = \Session::get('report')->id;

            // Request the file input named 'attachments'
            $files = $request->file('ava');

            //If the array is not empty
            if ($files[0] != '') {

                foreach($files as $file) {

                    // Get the orginal filname or create the filename of your choice
                    $data       = $file->getClientOriginalName();
                    $filenameOri   = pathinfo($data,PATHINFO_FILENAME);

                    /**
                     * cek responden
                     */
                    $staff  =   Staff::where('report_id',$report_id)
                                       ->where('kode_staff',$filenameOri)
                                       ->first();

                    if($staff)
                    {
                        // Mengambil file yang diupload
                        $ava_uploaded = $file;

                         // mengambil extension file
                        $extension = $ava_uploaded->getClientOriginalExtension();

                        $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'origin';
                        $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'thumb';
                        $destinationTable  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_staff'. DIRECTORY_SEPARATOR .'table';

                        $filename = substr( md5(rand()), 0, 3) . '.' . $extension;

                        /**
                         * membuat ava origin
                         */
                        $ava_uploaded->move($destinationOrigin, $filename);

                        /**
                         * membuat ava thumbnail
                         */
                        $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
                        $img->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumb.'/'.$filename);


                        /**
                         * membuat ava table
                         */
                        $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
                        $img->resize(50,50, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationTable.'/'.$filename);

                        // update ava store original
                        $staff->ava = $filename;
                        $staff->save();
                    }

                }
            }
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport ava staff"
        ]);

        // Tampilkan index buku
        return redirect()->back();

    }
}
