<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kota;
use App\Models\Store;
use App\User;
use App\Role;
use App\Http\Requests\StoreRequest;
use Image;
use File;
use Excel;
use Validator;
use Session;



class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Store Panel';

        $stores = Store::whereNUll('is_main')->with(['user','kota'])->get();

        return view('admin.store.index',compact('stores','pageTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Store Create';

        $kotas = Kota::pluck('name','code');

        return view('admin.store.create',compact('pageTitle','kotas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data['kota_code']      = $request->input('kota_code');
        $data['code']           = substr( md5(rand()), 0, 4);
        $data['name']           = $request->input('name');
        $data['latitude']       = $request->input('latitude');
        $data['longitude']      = $request->input('longitude');
        $data['address']        = $request->input('address');

        $store = Store::create($data);

        if($request->hasFile('ava'))
        {
            // Mengambil file yang diupload
            $ava_uploaded = $request->file('ava');

             // mengambil extension file
            $extension = $ava_uploaded->getClientOriginalExtension();

            $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'origin';
            $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'thumb';

            $filename = substr( md5(rand()), 0, 3) . '.' . $extension;

            $ava_uploaded->move($destinationOrigin, $filename);

            /**
             * membuat ava thumbnail
             */
            $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
            $img->resize(200, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationThumb.'/'.$filename);

            // update ava store original
            $store->ava = $filename;
            $store->save();
        }

        // create user
        $user = User::create([
            'name'      => $request->input('name'),
            'email'     => $request->input('email'),
            'password'  => bcrypt($request->input('password')),
        ]);

        $storeRole = Role::where('name','store')->first();
        $user->attachRole($storeRole);

        $store->user_id = $user->id;
        $store->save();

        return redirect()->route('stores.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::with('user')->where('code',$id)->first();

        $pageTitle = 'Store Edit';

        $kotas = Kota::pluck('name','code');

        return view('admin.store.edit',compact('pageTitle','kotas','store'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $code)
    {
        $store = Store::findOrFail($code);

        $store->kota_code      = $request->input('kota_code');
        $store->name           = $request->input('name');
        $store->latitude       = $request->input('latitude');
        $store->longitude      = $request->input('longitude');
        $store->address        = $request->input('address');
        $store->so1        = $request->input('so1');
        $store->so2        = $request->input('so2');
        $store->so3        = $request->input('so3');

        $store->save();

        if($request->hasFile('ava'))
        {
            // Mengambil file yang diupload
            $ava_uploaded = $request->file('ava');

             // mengambil extension file
            $extension = $ava_uploaded->getClientOriginalExtension();

            $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'origin';
            $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'thumb';

            $filename = substr( md5(rand()), 0, 3) . '.' . $extension;

            $ava_uploaded->move($destinationOrigin, $filename);

            /**
             * membuat ava thumbnail
             */
            $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationThumb.'/'.$filename);

            if($store->ava)
            {
                $old_ava = $store->ava;
                $old_origin_ava = $destinationOrigin. DIRECTORY_SEPARATOR .$old_ava;
                $old_thumb_ava = $destinationThumb. DIRECTORY_SEPARATOR .$old_ava;

                //hapus dari folde origin
                try {
                    \File::delete($old_origin_ava);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }


                //hapus dari folder thumb
                try {
                    \File::delete($old_thumb_ava);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }

            }

            // update ava store original
            $store->ava = $filename;
            $store->save();

        }

        /**
         * atur user
         */
        // $user = User::findOrFail($store->user_id);
        // $user->email = $request->input('email');


        // if($request->input('password')){
        //     $user->password = bcrypt($request->input('password'));
        // }

        // $user->save();

        return redirect()->route('stores.index')->with('alert-success', 'Data Berhasil Disimpan.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::findOrFail($id);

        if($store->ava)
        {
            $old_ava = $store->ava;

            $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'origin';
            $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'thumb';

            $old_origin_ava = $destinationOrigin. DIRECTORY_SEPARATOR .$old_ava;
            $old_thumb_ava = $destinationThumb. DIRECTORY_SEPARATOR .$old_ava;

            //hapus dari folde origin
            try {
                \File::delete($old_origin_ava);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }


            //hapus dari folder thumb
            try {
                \File::delete($old_thumb_ava);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }

        }

        Store::destroy($id);
        User::destroy($store->user_id);

    }

    /**
     * [generateExcelTemplate description]
     * @return [type] [description]
     */
    public function generateExcelTemplate()
    {
        ob_end_clean();

        Excel::create('Template Upload Store',function($excel){

            $excel->setTitle('Template Upload Report Store')
                ->setCreator('Teten')
                ->setCompany('RAD')
                ->setDescription('Template untuk upload data kota projek MS');

            $excel->sheet('Data Kota',function($sheet){
                $column = [];
                array_push($column,'kota_id');
                array_push($column,'code');
                array_push($column,'type');
                array_push($column,'name');
                array_push($column,'latitude');
                array_push($column,'longitude');
                array_push($column,'address');
                array_push($column,'email');
                array_push($column,'password');

                $row = 1;
                $sheet->row($row,$column);
            });
        })->export('xlsx');
    }

    public function importExcel(Request $request)
    {

        // validasi untuk memastikan file yang diupload adalah excel
        $this->validate($request, [ 'excel' => 'required|mimes:xls,xlsx' ]);

        // ambil file yang baru diupload
        $excel = $request->file('excel');

        // baca sheet pertama
        $excels = Excel::selectSheetsByIndex(0)->load($excel, function($reader) {
            // options, jika ada
        })->get();

        //rule untuk validasi setiap row pada file excel
        $rowRules = [
            'kota_id'   =>  'required',
            'code'      =>  'required',
            'name'      =>  'required',
            'email'     =>  'required',
            'password'  =>  'required',
            'type'      =>  'required',
        ];

        // Catat semua id buku baru
        // ID ini kita butuhkan untuk menghitung total buku yang berhasil diimport
        $stores_id = [];

        //looping setiap baris, mulai dari baris ke 2 (karena baris ke 1 adalah nama kolom)
        foreach($excels as $row) {

            //Membuat vlidasi untuk row di excel
            // dsini kita ubah baris yang sedang diproses menjadi array
            // $validator = Validator::make($row->toArray(),$rowRules);

            // skip baris ini jika tidak valid, langsung ke baris selajutnya
            // if ($validator->fails()) continue;

            $cekkota   = Kota::where('code',$row['kota_id'])->first();
            $store     = Store::where('code',$row['code'])->first();

            if(!is_null($cekkota))
            {

                /**
                 * cek store exist on db
                 */
                if(!is_null($store))
                {
                    // $store->kota_code       =   $cekkota->code;
                    // $store->code            =   $cekkota->code;
                    $store->name            =   $row['name'];
                    $store->latitude        =   $row['latitude'];
                    $store->longitude       =   $row['longitude'];
                    $store->address         =   $row['address'];
                    $store->type            =   $row['type'];

                    $store->save();

                    /**
                     * update user
                     */
                    $user = User::findOrFail($store->user_id);
                    $user->email    = $row['email'];
                    $user->password = bcrypt($row['password']);
                    $user->save();

                }else{

                    /**
                     * create kota
                     */
                    $data['kota_code']      =   $cekkota->code;
                    $data['code']           =   $row['code'];
                    $data['name']           =   $row['name'];
                    $data['latitude']       =   $row['latitude'];
                    $data['longitude']      =   $row['longitude'];
                    $data['address']        =   $row['address'];
                    $data['type']           =   $row['type'];

                    $store = Store::create($data);

                    // buatkan account email
                    $user = User::create([
                        'name'      => $row['name'],
                        'email'     => $row['email'],
                        'password'  => bcrypt($row['password']),
                    ]);

                    $storeRole = Role::where('name','store')->first();
                    $user->attachRole($storeRole);

                    $store->user_id  =   $user->id;
                    $store->save();
                }


                array_push($stores_id,$store->code);




            }

        }// end foreach

        // Ambil semua staff yang baru dibuat
        $stores = Store::whereIn('code',$stores_id)->get();

        // redirect ke form jika tidak ada buku yang berhasil diimport
        if ($stores->count() == 0) {

            Session::flash("flash_notification", [
                "level" => "danger",
                "message" => "Tidak ada staff yang berhasil diimport."
            ]);

            return redirect()->back();
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport " . $stores->count() . " Store."
        ]);

        // Tampilkan index buku
        return redirect()->back();
    }

    public function importAva(Request $request)
    {
        if($request->hasFile('ava'))
        {
            $report_id = \Session::get('report')->id;

            // Request the file input named 'attachments'
            $files = $request->file('ava');

            //If the array is not empty
            if ($files[0] != '') {

                foreach($files as $file) {

                    // Get the orginal filname or create the filename of your choice
                    $data          = $file->getClientOriginalName();
                    $filenameOri   = pathinfo($data,PATHINFO_FILENAME);

                    /**
                     * cek responden
                     */
                    $store  =   Store::findOrFail($filenameOri);

                    if($store)
                    {
                        // Mengambil file yang diupload
                        $ava_uploaded = $file;

                         // mengambil extension file
                        $extension = $ava_uploaded->getClientOriginalExtension();

                        $destinationOrigin = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'origin';
                        $destinationThumb  = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'ava_store'. DIRECTORY_SEPARATOR .'thumb';

                        $filename = substr( md5(rand()), 0, 3) . '.' . $extension;

                        /**
                         * membuat ava origin
                         */
                        $ava_uploaded->move($destinationOrigin, $filename);

                        /**
                         * membuat ava thumbnail
                         */
                        $img = Image::make($destinationOrigin. DIRECTORY_SEPARATOR . $filename);
                        $img->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationThumb.'/'.$filename);


                         // update ava store original
                        $store->ava = $filename;
                        $store->save();
                    }

                }
            }
        }

        // set feedback
        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil mengimport ava store"
        ]);

        // Tampilkan index buku
        return redirect()->back();

    }
}
