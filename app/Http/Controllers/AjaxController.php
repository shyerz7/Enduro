<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Store;
use App\Models\Kota;
use App\Models\Staff;
use App\Models\ReportStaff;

class AjaxController extends Controller
{
    public function gettopkc(Request $request)
    {
    	if($request->ajax())
    	{
    		$report_id = \Session::get('reportfrontend')->id;
	    	$kc_perfomances = Kota::topPerformance($report_id);
	    	return view('frontend.home.ajax.gettopkc',compact('kc_perfomances'));
    	}
    	abort(404);
    	
    }

    public function gettop10kcp(Request $request)
    {
    	if($request->ajax())
    	{
    		$report_id = \Session::get('reportfrontend')->id;
	    	$kcp_perfomances = Store::topPerformance($report_id);
	    	return view('frontend.home.ajax.gettop10kcp',compact('kcp_perfomances'));
    	}
    	abort(404);
    	
    }

    public function getlow10kcp(Request $request)
    {
        if($request->ajax())
        {
            $report_id = \Session::get('reportfrontend')->id;
            $kcp_low_perfomances = Store::lowPerformance($report_id);
            return view('frontend.home.ajax.getlow10kcp',compact('kcp_low_perfomances'));
        }
        abort(404);
        
    }

    public function gettop10cs(Request $request)
    {
    	if($request->ajax())
    	{
    		$report_id = \Session::get('reportfrontend')->id;
            $array_teller = [1,2];
	    	$top_staffs = Staff::topByPositionGabung($report_id, $array_teller, 5);
            return view('frontend.home.ajax.gettopstaff',compact('top_staffs'));
    	}
    	abort(404);
    	
    }

    public function gettop10Teller(Request $request)
    {
        if($request->ajax())
        {
            $report_id = \Session::get('reportfrontend')->id;
            $array_teller = [3,4];
            $top_staffs = Staff::topByPositionGabung($report_id, $array_teller, 5);
            return view('frontend.home.ajax.gettopstaff',compact('top_staffs'));
        }
        abort(404);
    }

    public function gettop10Satpam(Request $request)
    {
        if($request->ajax())
        {
            $report_id = \Session::get('reportfrontend')->id;
            $array_teller = [5];
            $top_staffs = Staff::topByPositionGabung($report_id, $array_teller, 5);
            return view('frontend.home.ajax.gettopstaff',compact('top_staffs'));
        }
        abort(404);
    }

    public function gettop10mc(Request $request)
    {
        if($request->ajax())
        {
            $report_id = \Session::get('reportfrontend')->id;
            $array_teller = [6,7];
            $top_staffs = Staff::topByPositionGabung($report_id, $array_teller, 5);
            return view('frontend.home.ajax.gettopstaff',compact('top_staffs'));
        }
        abort(404);
    }

}
