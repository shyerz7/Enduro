<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FisikStore;
use App\Models\ReportFisikStore;
use App\Models\ReportPerformanceFisikStore;
use Session;

class FisikStoreController extends Controller
{
    public function index($id) 
    {
    	// data report
        $report = Session::get('reportfrontend');

    	$fisikstore = FisikStore::with('fisik')->findOrFail($id);

    	/**
    	 * chart performance
    	 */
    	$report_performances = ReportPerformanceFisikStore::with('category')->where('report_id',$report->id)
                                                      ->where('fisik_store_id',$fisikstore->id)
                                                      ->get();
        $report_performance_data = [];
        foreach ($report_performances as $data ) {
            $data =  array("name"=>$data->category->category,"y"=>$data ? $data->score : '0' );
            array_push($report_performance_data,$data);
        }    

    	return view('frontend.fisikstore.index',compact('fisikstore','report_performance_data','report_performances'));

    }

    public function pdf($id)
    {
    	// data report
        $report = Session::get('reportfrontend');

        $fisikstore = FisikStore::with([
        								'fisik.parameter_category',
        								'store.kota'
        						  ])->findOrFail($id);

        $reportfisikstore = ReportFisikStore::where('report_id',$report->id)
        									  ->where('fisik_store_id',$fisikstore->id)
        									  ->first();

        $pdf = \App::make('dompdf.wrapper');									  

        $view =  view('frontend.fisikstore.partials.pdf',compact('report','fisikstore','reportfisikstore'));	

        $pdf->loadHTML($view);

        return $pdf->stream();								  

    }
}
