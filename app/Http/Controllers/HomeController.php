<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\ReportStore;
use App\Models\Store;
use App\Models\Staff;
use App\Models\Kota;
use App\Models\FisikStore;
use App\Models\ReportParameter;
use App\Models\ReportNasionalRBH;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('cekrolesistem');

        $report = Report::all()->last();

        \Session::put('reportfrontend', $report);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $report_id = \Session::get('reportfrontend')->id;

        $mainstore = Store::whereNotNull('is_main')->first();

        // $score = ReportStore::score($report_id);
        $score = ReportNasionalRBH::where('report_id',$report_id)
                                    ->whereNotNull('is_nasional')
                                    ->first();

        $rbh1_scores = ReportNasionalRBH::where('report_id',$report_id)
                                        ->where('kota_code','rbh1')
                                        ->first();

        $rbh2_scores = ReportNasionalRBH::where('report_id',$report_id)
                                        ->where('kota_code','rbh2')
                                        ->first();

        $rbh3_scores = ReportNasionalRBH::where('report_id',$report_id)
                                        ->where('kota_code','rbh3')
                                        ->first();

        $kc_top = Kota::top($report_id);

        $top_staff = Staff::top($report_id, 1);

        $top_store = Store::topStore($report_id, 1);

        /**
         * staff terbaik
         */

        $array_teller = [3,4]; 
        $top_teller = Staff::topByPositionGabung2($report_id,$array_teller,1);

        $array_teller = [1,2];
        $top_cs = Staff::topByPositionGabung2($report_id,$array_teller,1);

        $array_teller = [5];

        $top_satpam = Staff::topByPositionGabung($report_id,$array_teller,1);

        $array_mc = [6,7];

        $top_mc = Staff::topByPositionGabung($report_id,$array_mc,1);

        /**
         * fisik terbaik
         */
        $array_gedung = [1];
        $top_gedung = FisikStore::topByFisikGabung($report_id,$array_gedung,1);

        $array_toilet = [2];
        $top_toilet = FisikStore::topByFisikGabung($report_id,$array_toilet,1);

        $array_atm = [4];
        $top_atm = FisikStore::topByFisikGabung($report_id,$array_atm,1);

        /**
         * chart kc performance
         */
        $kc_perfomances = Kota::topPerformance($report_id);
        $kc_performance_data = [];
        foreach ($kc_perfomances as $kc_perfomance) {
            $data_kc_perform =  array("name"=>$kc_perfomance->namaKota,"y"=>number_format((float) $kc_perfomance->average,1) );
            array_push($kc_performance_data,$data_kc_perform);
        }

        /**
         * chart kcp performance
         */
        $kcp_perfomances = Store::topPerformance($report_id);
        $kcp_performance_data = [];
        foreach ($kcp_perfomances as $kcp_perfomance) {
            $data_kcp_perform =  array("name"=>$kcp_perfomance->namaStore,"y"=>number_format((float) $kcp_perfomance->average,1) );
            array_push($kcp_performance_data,$data_kcp_perform);
        }

        /**
         * chart rbh1
         */
        $rbh1_performances = Store::reportStoreByKkota($report_id,"rbh1");
        $rbh1_performance_data = [];
        foreach ($rbh1_performances as $rbh1_performance) {
            $data_rbh1_perform =  array("name"=>$rbh1_performance->nama,"y"=>number_format((float) $rbh1_performance->average,1) );
            array_push($rbh1_performance_data,$data_rbh1_perform);
        }

        $rbh2_performances = Store::reportStoreByKkota($report_id,"rbh2");
        $rbh2_performance_data = [];
        foreach ($rbh2_performances as $rbh2_performance) {
            $data_rbh2_perform =  array("name"=>$rbh2_performance->nama,"y"=>number_format((float) $rbh2_performance->average,1) );
            array_push($rbh2_performance_data,$data_rbh2_perform);
        }

        $rbh3_performances = Store::reportStoreByKkota($report_id,"rbh3");
        $rbh3_performance_data = [];
        foreach ($rbh3_performances as $rbh3_performance) {
            $data_rbh3_perform =  array("name"=>$rbh3_performance->nama,"y"=>number_format((float) $rbh3_performance->average,1) );
            array_push($rbh3_performance_data,$data_rbh3_perform);
        }

        /**
         * datatables
         */
        $stores = Store::join('report_stores','report_stores.code_store','=','stores.code')
                        ->join('kotas','kotas.code','=','stores.kota_code')
                        ->select('kotas.code as codeKC','kotas.name as nameKC','stores.code as codeKCP','stores.name as nameKCP','stores.type as type',\DB::raw('report_stores.score AS score'))
                        ->whereNull('is_main')
                        ->latest('score')
                        ->get();

        $staffs = Staff::join('report_staffs','report_staffs.staff_id','=','staff.id')
                        ->join('stores','stores.code','=','staff.code_store')
                        ->join('positions','positions.id','=','staff.position_id')
                        ->select('positions.name as posisi','staff.id as id','staff.nama as namaStaff','stores.code as codeKCP','stores.name as nameKCP','stores.type as type',\DB::raw('report_staffs.score AS score'))
                        ->latest('score')
                        ->get();

        /**
         * breadcrumb
         */
        $list_kc = Kota::get();

        /**
          * report parameter nasional
        */
        $reportnasionalget = ReportParameter::where('is_nasional',true)->first();

      	$reportnasionalpeople = null;
      	$reportnasionaltangible = null;
      	if(!is_null($reportnasionalget)) {
      		$reportnasionalpeople = json_decode($reportnasionalget->report_people,true);
      		$reportnasionaltangible = json_decode($reportnasionalget->report_tangible,true);
      	}

        /**
         * report total parameter tangible
         */
        $nilai_total_tangible = 0;
        if(!is_null($reportnasionaltangible)){
          $nilai_total_tangible = $nilai_total_tangible+$reportnasionaltangible[0]['gedung'];
          $nilai_total_tangible = $nilai_total_tangible+$reportnasionaltangible[1]['toilet'];
          $nilai_total_tangible = $nilai_total_tangible+$reportnasionaltangible[2]['atm'];
          $nilai_total_tangible = $nilai_total_tangible/3;
        } 

        /**
         * report total parameter people
         */
        $nilai_total_people = 0;
        if(!is_null($reportnasionalpeople)){
          $nilai_total_people = $nilai_total_people+$reportnasionalpeople[0]['cs'];
          $nilai_total_people = $nilai_total_people+$reportnasionalpeople[1]['teller'];
          $nilai_total_people = $nilai_total_people+$reportnasionalpeople[2]['satpam'];
          $nilai_total_people = $nilai_total_people/3;
        }
        
        

        return view('frontend.home.index',compact('mainstore','score','kc_top','top_staff','top_store','kc_performance_data','kcp_performance_data',
          'kcp_perfomances','stores','staffs','list_kc','top_teller','top_cs','top_satpam','top_mc','top_atm','top_toilet','top_gedung','rbh1_performance_data',
          'rbh2_performance_data','rbh3_performance_data',
          'reportnasionalpeople', 'reportnasionalget', 'reportnasionaltangible','nilai_total_tangible','nilai_total_people','rbh1_scores','rbh2_scores','rbh3_scores'));
    }
}
