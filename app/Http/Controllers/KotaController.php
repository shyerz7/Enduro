<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kota;
use App\Models\ReportParameter;
use App\Models\ReportStore;
use App\Models\Store;
use App\Models\ReportNasionalRBH;
use DB;
use Session;

class KotaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {

        $report_id = \Session::get('reportfrontend')->id;

        $kota = Kota::findOrFail($code);

        // $kc_scores = ReportStore::where('report_id',$report_id)
        //                             ->join('stores',function($join) use ($kota) {
        //                                 $join->on('stores.code','=','report_stores.code_store')
        //                                         ->where('stores.kota_code','=',$kota->code);
        //                                 })
        //                             ->select(DB::raw('AVG(report_stores.score) as avg_score'))
        //                             ->first();

        $kc_scores = ReportNasionalRBH::where('report_id',$report_id)
                                    ->where('kota_code',$kota->code)
                                    ->first();

        $storemax = Store::select('stores.name as nama',DB::raw('IF(report_stores.score is null,0,score) as score'))
                            ->leftJoin('report_stores',function($leftJoin) use ($report_id) {
                                    $leftJoin->on('stores.code','=','report_stores.code_store')
                                            ->where('report_stores.report_id','=',$report_id);
                                    })
                            ->where('stores.kota_code',$kota->code)
                            ->orderBy('score','desc')
                            ->first();


        $storemin = Store::select('stores.name as nama',DB::raw('IF(report_stores.score is null,0,score) as score'))
                            ->leftJoin('report_stores',function($leftJoin) use ($report_id) {
                                    $leftJoin->on('stores.code','=','report_stores.code_store')
                                            ->where('report_stores.report_id','=',$report_id);
                                    })
                            ->where('stores.kota_code',$kota->code)
                            ->orderBy('score','asc')
                            ->first();

        /**
         * chart performance
         */
        $report_stores = Store::leftJoin('report_stores',function($leftJoin) use ($report_id) {
                                        $leftJoin->on('stores.code','=','report_stores.code_store')
                                                ->where('report_stores.report_id','=',$report_id);
                                        })
                                    ->select('stores.name as nama',DB::raw('IF(report_stores.score is null,0,score) as score'))
                                    ->where('stores.kota_code',$kota->code)
                                    ->get();

        $report_performance_data = [];
        foreach($report_stores as $data) {
            $data =  array("name"=>$data->nama,"y"=>$data->score );
            array_push($report_performance_data,$data);
        }

        $stores = Store::where('kota_code',$kota->code)->get();


      	$reportrbhget = ReportParameter::where('kota_code',$code)->first();


      	$reportrbhgetpeople = null;
      	$reportrbhgettangible = null;
      	if(!is_null($reportrbhget)) {
      		$reportrbhgetpeople = json_decode($reportrbhget->report_people,true);
      		$reportrbhgettangible = json_decode($reportrbhget->report_tangible,true);
      	}

        /**
         * report total parameter tangible
         */
        $nilai_total_tangible = 0;
        if(!is_null($reportrbhgettangible)){
          $nilai_total_tangible = $nilai_total_tangible+$reportrbhgettangible[0]['gedung'];
          $nilai_total_tangible = $nilai_total_tangible+$reportrbhgettangible[1]['toilet'];
          $nilai_total_tangible = $nilai_total_tangible+$reportrbhgettangible[2]['atm'];
          $nilai_total_tangible = $nilai_total_tangible/3;
        } 

        /**
         * report total parameter people
         */
        $nilai_total_people = 0;
        if(!is_null($reportrbhgetpeople)){
          $nilai_total_people = $nilai_total_people+$reportrbhgetpeople[0]['cs'];
          $nilai_total_people = $nilai_total_people+$reportrbhgetpeople[1]['teller'];
          $nilai_total_people = $nilai_total_people+$reportrbhgetpeople[2]['satpam'];
          $nilai_total_people = $nilai_total_people/3;
        }

        return view('frontend.kota.index',compact('kota','kc_scores','storemax','storemin','report_performance_data','stores',
        'reportrbhgetpeople', 'reportrbhgettangible', 'reportrbhget','nilai_total_tangible','nilai_total_people'));
    }
}
