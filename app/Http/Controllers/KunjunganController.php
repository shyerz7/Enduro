<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kunjungan;
use App\Models\Kota;
use App\Models\Staff;
use App\Models\Report;
use App\Models\ReportStaff;
use App\Models\ReportFisikkunjungan;
use App\Models\Fisikkunjungan;
use DB;

class KunjunganController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {
        $so = Report::where('id', $code)->first();
        $report_id = \Session::get('reportfrontend')->id;
        $staffs = Staff::where('report_id',$code)
                         ->whereNotNull('ava')
                         ->where('position_id', '!=', 15)
                         ->join('positions', 'staff.position_id', '=', 'positions.id')
                         ->select('staff.*', 'positions.name as position_name')
                         ->paginate(15);

        return view('frontend.kunjungan.index',compact('report_id', 'so', 'staffs'));
    }
}
