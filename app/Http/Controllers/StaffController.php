<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Staff;
use App\Models\ReportStaff;
use App\Models\ReportPerformanceStaff;
use Session;
use App\Models\File;

class StaffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        // data report
        $report = Session::get('reportfrontend');
        $staff = Staff::with(['position','store'])->findOrFail($id);

        /**
         * score dan ranking
         */
        $staff_score = ReportStaff::setStaff($report,$staff);

        //belum ada nilai
        if(is_null($staff_score))
        {
            $staff_score        = new ReportStaff;
            $staff_score->score = 0;
        }
        $array_posisi = [];
        // ranking berdasarkan posisi
        if(($staff->position_id == 1) || ($staff->position_id == 2)){
            $array_posisi = [1,2];
        }elseif(($staff->position_id == 3) || ($staff->position_id == 4)){
            $array_posisi = [3,4];
        }elseif(($staff->position_id == 6) || ($staff->position_id == 7)){
            $array_posisi = [6,7];
        }elseif($staff->position_id == 5){
            $array_posisi = [5];
        }

        $ranks_in_position = Staff::rankInPosition($report, $staff, $array_posisi)->toArray();

        // filter ranking berdasarkan posisi
        $filter_ranks_in_position = array_where($ranks_in_position, function ($index, $value) use ($staff)
        {
            return $index['id'] == $staff->id;
        });

        // ranking berdasarkan posisi
        $current_rank_in_position = key($filter_ranks_in_position);

        // ada ranking berdasarkan posisi
        if( ! is_null($current_rank_in_position))
        {
            $current_rank_in_position += 1;
        }
        // belum ada ranking berdasarkan posisi
        else
        {
            $current_rank_in_position = 0;
        }

        // ranking berdasarkan cabang & posisi
        $ranks_in_kota_by_position = Staff::rankInKotaByPosition($report, $staff->store->kota, $array_posisi)->toArray();

        // filter ranking berdasarkan cabang & posisi
        $filter_ranks_in_kota_by_position = array_where($ranks_in_kota_by_position, function ($index, $value) use ($staff)
        {
            return $index['id'] == $staff->id;
        });

        // ranking berdasarkan cabang & posisi
        $current_rank_in_kota_by_position = key($filter_ranks_in_kota_by_position);

        // ada ranking berdasarkan cabang & posisi
        if( ! is_null($current_rank_in_kota_by_position))
        {
            $current_rank_in_kota_by_position += 1;
        }
        // belum ada ranking berdasarkan cabang & posisi
        else
        {
            $current_rank_in_kota_by_position = 0;
        }

        /**
         * download file multimedia dan pdf report
         */
        $file_report = File::where('staff_id',$staff->id)
                            // ->where('report_id',$report->id)
                            ->first();

        $score = (float) $staff_score->score;

        /**
         * chart performance staff
         */
        $report_performances = ReportPerformanceStaff::where('report_id',$report->id)
                                                      ->where('staff_id',$staff->id)
                                                      ->with('category')
                                                      ->get();

        $report_performance_data = [];
        foreach ($report_performances as $data ) {
            $data =  array("name"=>$data->category->kategori,"y"=>$data ? $data->score : '0' );
            array_push($report_performance_data,$data);
        }



        return view('frontend.staff.index',compact('staff','staff_score','current_rank_in_position','current_rank_in_kota_by_position','ranks_in_kota_by_position','file_report','score','report_performance_data'));
    }

    public function downloadMultimedia($id)
    {
        $file_report = File::findOrFail($id);

        if($file_report->filetype == 'mp4' || $file_report->filetype == 'MP4')
        {
            $destinationPath   = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'file'. DIRECTORY_SEPARATOR .'video';
        }else{
            $destinationPath     = public_path() . DIRECTORY_SEPARATOR . 'attachments'. DIRECTORY_SEPARATOR . 'file'. DIRECTORY_SEPARATOR .'mp3';
        }

        $path = $destinationPath. DIRECTORY_SEPARATOR .$file_report->filename;

        return \Response::download($path);

    }

    public function pdf($id)
    {
        // data report
        $report = Session::get('reportfrontend');

        $staff = Staff::with('position')->findOrFail($id);

        /**
         * score dan ranking
         */
        $staff_score = ReportStaff::setStaff($report,$staff);

        //belum ada nilai
        if(is_null($staff_score))
        {
            $staff_score        = new ReportItem;
            $staff_score->score = 0;
        }
        if(($staff->position_id == 1) || ($staff->position_id == 2)){
            $array_posisi = [1,2];
        }elseif(($staff->position_id == 3) || ($staff->position_id == 4)){
            $array_posisi = [3,4];
        }elseif(($staff->position_id == 6) || ($staff->position_id == 7)){
            $array_posisi = [6,7];
        }elseif($staff->position_id == 5){
            $array_posisi = [5];
        }
        // ranking berdasarkan posisi
        $ranks_in_position = Staff::rankInPosition($report, $staff, $array_posisi)->toArray();

        // filter ranking berdasarkan posisi
        $filter_ranks_in_position = array_where($ranks_in_position, function ($index, $value) use ($staff)
        {
            return $index['id'] == $staff->id;
        });

        // ranking berdasarkan posisi
        $current_rank_in_position = key($filter_ranks_in_position);

        // ada ranking berdasarkan posisi
        if( ! is_null($current_rank_in_position))
        {
            $current_rank_in_position += 1;
        }
        // belum ada ranking berdasarkan posisi
        else
        {
            $current_rank_in_position = 0;
        }

        // ranking berdasarkan cabang & posisi
        $ranks_in_kota_by_position = Staff::rankInKotaByPosition($report, $staff->store->kota, $staff['position'])->toArray();

        // filter ranking berdasarkan cabang & posisi
        $filter_ranks_in_kota_by_position = array_where($ranks_in_kota_by_position, function ($index, $value) use ($staff)
        {
            return $index['id'] == $staff->id;
        });

        // ranking berdasarkan cabang & posisi
        $current_rank_in_kota_by_position = key($filter_ranks_in_kota_by_position);

        // ada ranking berdasarkan cabang & posisi
        if( ! is_null($current_rank_in_kota_by_position))
        {
            $current_rank_in_kota_by_position += 1;
        }
        // belum ada ranking berdasarkan cabang & posisi
        else
        {
            $current_rank_in_kota_by_position = 0;
        }

        $pdf = \App::make('dompdf.wrapper');

        $view = view('frontend.staff.partials.pdf',compact('report','staff','store','staff_score','current_rank_in_kota_by_position','current_rank_in_position','ranks_in_kota_by_position'));

        $pdf->loadHTML($view);

        return $pdf->stream();

    }
}
