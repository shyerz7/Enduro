<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Store;
use App\Models\Kota;
use App\Models\Staff;
use App\Models\Report;
use App\Models\ReportStaff;
use App\Models\ReportFisikStore;
use App\Models\FisikStore;
use DB;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {
        $so = Store::where('code', $code)->first();
        $report_id = \Session::get('reportfrontend')->id;

        $store = Store::with('reportstore')->findOrFail($code);

        $kota  = Kota::findOrFail($store->kota_code);

        $staffmax = Staff::select('staff.nama as nama','positions.alias as alias',DB::raw('IF(report_staffs.score is null,0,score) as score'))
                            ->leftJoin('report_staffs',function($leftJoin) use ($report_id) {
                                    $leftJoin->on('staff.id','=','report_staffs.staff_id')
                                            ->where('report_staffs.report_id','=',$report_id);
                                    })
                            ->join('positions','staff.position_id','=','positions.id')
                            ->where('staff.code_store',$store->code)
                            ->orderBy('score','desc')
                            ->first();


        $staffmin = Staff::select('staff.nama as nama',DB::raw('IF(report_staffs.score is null,0,score) as score'))
                            ->leftJoin('report_staffs',function($leftJoin) use ($report_id) {
                                    $leftJoin->on('staff.id','=','report_staffs.staff_id')
                                            ->where('report_staffs.report_id','=',$report_id);
                                    })
                            ->where('staff.code_store',$store->code)
                            ->orderBy('score','asc')
                            ->first();

        /**
         * chart staff
         */
        $staffs = Staff::where('report_id',$report_id)
                         ->where('code_store',$store->code)
                         ->with('position')
                         ->whereIn('position_id',[1,2,3,4,5,6,7])
                         ->get();

        $staff_performance_data = [];
        foreach ($staffs as $staff) {
            $reportdata= ReportStaff::where('report_id',$report_id)
                                     ->where('staff_id',$staff->id)
                                     ->select('score')
                                     ->first();

            $nama = $staff->nama.'<br />'.$staff->position->alias;

            $data =  array("name"=>$nama,"y"=>$reportdata ? $reportdata->score : '0' );
            array_push($staff_performance_data,$data);

        }

        /**
         * fisik performance
         */
        $fisikstores = FisikStore::where('report_id',$report_id)
                                 ->where('code_store',$store->code)
                                 ->with(['fisik'])
                                 ->get();

        $fisik_performance_data = [];
        foreach ($fisikstores as $fisikstore) {
            $reportfisik= ReportFisikStore::where('report_id',$report_id)
                                     ->where('fisik_store_id',$fisikstore->id)
                                     ->select('score')
                                     ->first();

            $datafisik =  array("id"=>$fisikstore->id,"name"=>$fisikstore->fisik->name,"y"=>$reportfisik ? $reportfisik->score : '0' );
            array_push($fisik_performance_data,$datafisik);

        }

        /**
         * mc performance
         */
        $mcs = Staff::where('report_id',$report_id)
                         ->where('code_store',$store->code)
                         ->with('position')
                         ->whereIn('position_id',[6,7])
                         ->get();
        $mc_performance_data = [];
        foreach ($mcs as $mc) {
            $reportdatamc= ReportStaff::where('report_id',$report_id)
                                     ->where('staff_id',$staff->id)
                                     ->select('score')
                                     ->first();

            $nama = '<b>'.$mc->nama.'</b>-'.$mc->position->alias;

            $data =  array("name"=>$nama,"y"=>$reportdatamc ? $reportdatamc->score : '0' );
            array_push($mc_performance_data,$data);

        }

        /**
         * rank
         */

        $semua_store = Store::data();

        $ranks = Store::ranks($report_id);

        // filter ranking keseluruhan
        $filter_ranks = array_where($ranks, function ($index, $value) use ($store)
        {
            return $index['code'] == $store->code;
        });


        // ranking keseluruhan
        $current_rank = key($filter_ranks);

        // ada ranking keseluruhan
        if( ! is_null($current_rank))
        {
            $current_rank += 1;
        }
        // belum ada ranking keseluruhan
        else
        {
            $current_rank = 0;
        }


        $count_store_by_kota = Store::dataByKota($kota);

        $ranks_in_kota = Store::ranksInKota($report_id,$kota)->toArray();

        // filter ranking berdasarkan cabang
        $filter_ranks_in_kota = array_where($ranks_in_kota, function ($index, $value) use ($store)
        {
            return $index['code'] == $store->code;
        });

        // ranking berdasarkan cabang
        $current_rank_in_kota = key($filter_ranks_in_kota);

        // ada ranking berdasarkan cabang
        if( ! is_null($current_rank_in_kota))
        {
            $current_rank_in_kota += 1;
        }
        // belum ada ranking berdasarkan cabang
        else
        {
            $current_rank_in_kota = 0;
        }

        return view('frontend.store.index',compact('store','staffmax','staff_performance_data','fisik_performance_data','staffs','fisikstores','current_rank','semua_store','current_rank_in_kota','count_store_by_kota','staffmin','kota','mc_performance_data', 'so'));
    }
}
