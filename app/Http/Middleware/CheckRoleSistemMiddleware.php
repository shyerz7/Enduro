<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Kota;
use App\Models\Store;

class CheckRoleSistemMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userAuth = \Auth::user();

        if($userAuth->hasRole('admin_kota'))
        {
            $kota = Kota::where('user_id',$userAuth->id)->first();

            return redirect()->route('kota',$kota->code);
            
        }elseif($userAuth->hasRole('store'))
        {
            $store = Store::where('user_id',$userAuth->id)->first();

            return redirect()->route('store',$store->code);
            
        }

        return $next($request);
    }
}
