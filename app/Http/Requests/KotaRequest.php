<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KotaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required'
        ];
    }

    /**
     * custom validasi teks
     * @return [type] [description]
     */
    public function messages()
    {
        return [
            'name.required'             => 'Nama Tidak Boleh Kosong'
        ];
    }
}
