<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    protected $fillable = [
        'report_id',
        'staff_id',
        'filetype',
        'filename',
    ];

    public function staff()
    {
        return $this->belongsTo('App\Models\Staff','staff_id');
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Report','report_id');
    }
}
