<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fisik extends Model
{
    /**
    * @var array
    */
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parameter_category()
    {
        return $this->hasMany('App\Models\ParameterCategoryFisik', 'fisik_id', 'id');
    }
}
