<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FisikStore extends Model
{
    protected $fillable = [
        'report_id',
        'fisik_id',
        'code_store',
    ];

    public static function topByFisikGabung($report_id,$array_fisik,$take)
    {
      $query = "";  
      if($take == 1)
      {
        $query = self::select('fisik_stores.id as id','fisiks.name as namaFisik','stores.name as namaStore', \DB::raw('report_fisik_stores.score AS average'))
                    ->leftJoin('report_fisik_stores', 'report_fisik_stores.fisik_store_id', '=', 'fisik_stores.id')
                    ->join('stores', 'stores.code', '=', 'fisik_stores.code_store')
                    ->join('fisiks', 'fisiks.id', '=', 'fisik_stores.fisik_id')
                    ->where('report_fisik_stores.report_id', '=', $report_id)
                    ->whereIn('fisik_stores.fisik_id',$array_fisik)
                    ->take($take)
                    ->orderBy('average','desc')
                    ->first();
      }else{ 
        $query =  self::select('fisik_stores.id as id','fisiks.name as namaFisik','stores.name as namaStore', \DB::raw('report_fisik_stores.score AS average'))
                    ->leftJoin('report_fisik_stores', 'report_fisik_stores.fisik_store_id', '=', 'fisik_stores.id')
                    ->join('stores', 'stores.code', '=', 'fisik_stores.code_store')
                    ->join('fisiks', 'fisiks.id', '=', 'fisik_stores.fisik_id')
                    ->where('report_fisik_stores.report_id', '=', $report_id)
                    ->whereIn('fisik_stores.fisik_id',$array_fisik)
                    ->take($take)
                    ->orderBy('average','desc')
                    ->get(); 
      }    

      return $query;          
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Report','report_id');
    }

    public function fisik()
    {
        return $this->belongsTo('App\Models\Fisik','fisik_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store','code_store');
    }

    public function score()
    {
        return $this->hasOne('App\Models\ReportFisikStore');
    }
}
