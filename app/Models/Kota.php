<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kota extends Model
{
    protected $fillable = [
        'code',
        'name',
        'latitude',
        'longitude',
        'ava',
        'user_id',
        'address',
        'id_region',
    ];

    protected $primaryKey = 'code'; // or null

    public $incrementing = false;

    /**
     * [user description]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region','id_region');
    }

    /**
     * @param $report
     *
     * @return array|static[]
     */
    public static function top($report)
    {
        return self::select('kotas.name as namaKota',DB::raw('AVG(report_stores.score) as average'))
            ->join('stores', 'stores.kota_code', '=', 'kotas.code')
            ->join('report_stores', 'report_stores.code_store', '=', 'stores.code')
            ->where('report_stores.report_id', '=', $report)
            ->groupBy('kotas.code')
            ->latest('average')
            ->take(1)
            ->first();
    }

    /**
     * @param $report
     *
     * @return array|static[]
     */
    public static function topPerformance($report)
    {
        return self::select('kotas.name as namaKota',DB::raw('AVG(report_stores.score) as average'))
            ->join('stores', 'stores.kota_code', '=', 'kotas.code')
            ->join('report_stores', 'report_stores.code_store', '=', 'stores.code')
            ->where('report_stores.report_id', '=', $report)
            ->groupBy('kotas.code')
            ->latest('average')
            // ->take(1)
            ->get();
    }

}
