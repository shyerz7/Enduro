<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterCategoryFisik extends Model
{
    protected $table = 'fisiks_parameter_category';

    protected $fillable = [
        'report_id',
        'fisik_id',
        'category'
    ];

    /**
     * @param $id_posisi
     * @param $kategori
     */
    public static function tambah($report_id,$fisik_id,$kategori) {
        self::create([
            'report_id'   =>  $report_id,
            'fisik_id'    =>  $fisik_id,
            'category'    =>  $kategori
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parameter_data()
    {
        return $this->hasMany('App\Models\ParameterFisik', 'fisik_parameter_category_id', 'id');
    }
}
