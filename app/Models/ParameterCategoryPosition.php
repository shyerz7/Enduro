<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterCategoryPosition extends Model
{
    protected $table = 'positions_parameter_category';

    protected $fillable = [
        'report_id',
        'position_id',
        'category'
    ];

    /**
     * @param $id_posisi
     * @param $kategori
     */
    public static function tambah($id_posisi,$kategori) {
        self::create([
            'position_id' =>  $id_posisi,
            'category'    =>  $kategori
        ]);
    }

      /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parameter_data()
    {
        return $this->hasMany('App\Models\ParameterPosition', 'position_parameter_category_id', 'id');
    }
}
