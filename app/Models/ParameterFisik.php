<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterFisik extends Model
{
    protected $table = 'fisiks_parameter_data';

    protected $fillable = [
        'report_id',
        'fisik_parameter_category_id',
        'text',
        'kode_atribut',
        'sort',
        'positif_statement',
        'negatif_statement',
    ];

    /**
     * @param $id_kategori
     * @param $teks
     *
     * @return int
     */
    public static function tambah($report_id,$id_kategori, $teks,$kode_atribut,$positif_statement,$negatif_statement)
    {
        return self::insertGetId([
        		'report_id'							=> $report_id,
        		'fisik_parameter_category_id' 	    => $id_kategori, 
        		'text' 								=> $teks,
        		'kode_atribut' 						=> $kode_atribut,
        		'positif_statement' 				=> $positif_statement,
        		'negatif_statement' 				=> $negatif_statement,
        ]);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ParameterCategoryFisik','fisik_parameter_category_id');
    }

    
}
