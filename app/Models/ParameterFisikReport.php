<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterFisikReport extends Model
{
    protected $table = 'fisiks_parameter_reports';

    /**
     * @var array
     */
    protected $fillable = [
        'report_id',
        'fisik_store_id',
        'fisik_parameter_data_id',
        'status',
        'comment'
    ];
}
