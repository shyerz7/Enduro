<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterPosition extends Model
{
    protected $table = 'positions_parameter_data';

    protected $fillable = [
        'report_id',
        'position_parameter_category_id',
        'text',
        'kode_atribut',
        'positif_statement',
        'negatif_statement',
    ];

      /**
     * @param $id_kategori
     * @param $teks
     *
     * @return int
     */
    public static function tambah($report_id,$id_kategori, $teks,$kode_atribut,$positif_statement,$negatif_statement)
    {
        return self::insertGetId([
            'report_id'                         => $report_id,
            'position_parameter_category_id'    => $id_kategori, 
            'text'                              => $teks,
            'kode_atribut'                      => $kode_atribut,
            'positif_statement'                 => $positif_statement,
            'negatif_statement'                 => $negatif_statement
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parameter_gender()
    {
        return $this->hasMany('App\Models\ParameterPositionGender', 'id_parameter', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ParameterCategoryPosition','position_parameter_category_id');
    }
}
