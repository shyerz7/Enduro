<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterPositionGender extends Model
{
    protected $table = 'positions_parameter_gender';

    protected $fillable = [
        'id_parameter',
        'gender'
    ];

    /**
     * @param $parameter_gender
     */
    public static function tambah($parameter_gender)
    {
        self::insert($parameter_gender);
    }

    /**
     * @param $id
     *
     * @throws \Exception
     */
    public static function hapusByParameter($id)
    {
        self::where('id_parameter', '=', $id)->delete();
    }
}
