<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParameterReportPosition extends Model
{
    protected $table = 'positions_parameter_report';

    /**
     * @var array
     */
    protected $fillable = [
        'report_id',
        'staff_id',
        'position_parameter_data_id',
        'status',
        'comment'
    ];
    
}
