<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerformanceCategory extends Model
{
    protected $fillable = [
    	'report_id',
    	'kode',
    	'kategori',
    ];
}
