<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parameter_category()
    {
        return $this->hasMany('App\Models\ParameterCategoryPosition', 'position_id', 'id');
    }
}
