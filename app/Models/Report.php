<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class Report extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'periode_start',
        'periode_end',
        'kuisioner',
        'maintenance_mode',
        'autosummary_positif',
        'autosummary_negatif'
    ];

    /**
     * getter semua data report
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getDataReport()
    {
        return self::orderBy('id','asc')->get();
    }

    /**
     * set data report berdasarkan id
     *
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static
     */
    public static function set($id)
    {
        return self::findOrFail($id);
    }

    public static function getCurrentSession()
    {
        $currentSession = \Session::get('report');
        return self::findOrFail($currentSession->id);
    }

    public static function getMaintenanceMode()
    {
        $currentSession = \Session::get('report');
        $obj =  self::findOrFail($currentSession->id);
        return $obj->maintenance_mode;
    }

}
