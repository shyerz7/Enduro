<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportFisikStore extends Model
{
    protected $fillable = [
        'report_id',
        'fisik_store_id',
        'score',
    ];

    

}
