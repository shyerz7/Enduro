<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportNasionalRBH extends Model
{
	protected $table = 'report_nasional_rbh';

    protected $fillable = [
    	'report_id',
    	'is_nasional',
    	'kota_code',
    	'score'
    ];
}
