<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportParameter extends Model
{
    protected $fillable = [
    	'is_nasional',
    	'kota_code',
    	'report_people',
    	'report_mc',
    	'report_tangible',
    ];
}
