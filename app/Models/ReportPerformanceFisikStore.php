<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportPerformanceFisikStore extends Model
{
    protected $fillable = [
        'report_id',
        'fisik_store_id',
        'category_id',
        'score',
    ];

    public function category()
    {
    	return $this->belongsTo('App\Models\ParameterCategoryFisik','category_id');
    }
}
