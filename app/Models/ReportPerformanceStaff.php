<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportPerformanceStaff extends Model
{
    protected $fillable = [
        'report_id',
        'staff_id',
        'category_id',
        'score',
    ];

    public function category()
    {
    	return $this->belongsTo('App\Models\PerformanceCategory','category_id');
    }
}
