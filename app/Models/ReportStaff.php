<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportStaff extends Model
{
    protected $table = 'report_staffs';

    protected $fillable = [
        'report_id',
        'staff_id',
        'summary',
        'score',
        'score_individu',
    ];

    /**
     * @param $report
     * @param $staff
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public static function setStaff($report, $staff)
    {
        return self::where('staff_id', '=', $staff->id)
                    // ->where('report_id', '=', $report->id)
                    ->first();
    }

    public function staff()
    {
        return $this->belongsTo('App\Models\Staff','staff_id');
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Report','report_id');
    }

}
