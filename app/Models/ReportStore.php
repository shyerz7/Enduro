<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReportStore extends Model
{
    protected $table = 'report_stores';
    
    protected $fillable = [
        'report_id',
        'code_store',
        'score',
    ];

    public function store()
    {
        return $this->belongsTo('App\Models\Store','code_store');
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Report','report_id');
    }

    /**
     * @param $report
     *
     * @return mixed
     */
    public static function score($report)
    {
        return self::with('store')
            ->whereHas('store', function($query) {
                $query->whereNull('is_main');
            })
            ->whereReportId($report)->avg('score');
    }

     
}
