<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = [
        'report_id',
        'position_id',
        'code_store',
        'kode_staff',
        'nama',
        'ava',
        'gender',
    ];


    /**
     * @param $report
     * @param $staff
     *
     * @return array|static[]
     */
    public static function rankInPosition($report, $staff,$array_posisi)
    {
        return self::select('staff.id')
                   ->leftJoin('positions', 'positions.id', '=', 'staff.position_id')
                   ->leftJoin('stores', 'stores.code', '=', 'staff.code_store')
                   ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.code_store')
                   ->where('staff.report_id', '=', $report->id)
                   ->whereIn('staff.position_id', $array_posisi)
                   ->latest('report_staffs.score')
                   ->orderBy('staff.nama')
                   ->get();
    }

    /**
     * @param $report
     * @param $cabang
     *
     * @return array|static[]
     */
    public static function rankInKotaByPosition($report, $kota, $array_posisi)
    {
        return self::select('staff.id')
                   ->leftJoin('positions', 'positions.id', '=', 'staff.position_id')
                   ->leftJoin('stores', 'stores.code', '=', 'staff.code_store')
                   ->leftJoin('kotas', 'kotas.code', '=', 'stores.kota_code')
                   ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.id')
                   ->where('staff.report_id', '=', $report->id)
                   ->where('kotas.code', '=', $kota->code)
                   ->whereIn('staff.position_id', $array_posisi)
                   ->latest('report_staffs.score')
                   ->orderBy('staff.nama')
                   ->get();
    }

    /**
     * @param $report
     * @param $max
     *
     * @return array|static[]
     */
    public static function top($report, $max)
    {
        return self::select('staff.*', \DB::raw('report_staffs.score AS average'))
            ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.id')
            ->where('report_staffs.report_id', '=', $report)
            ->take($max)
            ->orderBy('average','desc')
            ->get();
    }

    public static function topByPosition($report, $max,$id_position)
    {
        return self::select('staff.nama as namaStaff','stores.name as namaStore', \DB::raw('report_staffs.score AS average'))
            ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.id')
            ->join('stores', 'stores.code', '=', 'staff.code_store')
            ->where('report_staffs.report_id', '=', $report)
            ->where('staff.position_id', '=', $id_position)
            ->take($max)
            ->orderBy('average','desc')
            ->get();
    }

    public static function topByPositionGabung($report_id,$array_posisi,$take)
    {
      $query = "";  
      if($take == 1)
      {

        $query = self::select('positions.name as posisi','staff.nama as namaStaff','staff.ava as ava','stores.name as namaStore', \DB::raw('report_staffs.score AS average'))
                    ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.id')
                    ->join('stores', 'stores.code', '=', 'staff.code_store')
                    ->join('positions', 'positions.id', '=', 'staff.position_id')
                    ->where('report_staffs.report_id', '=', $report_id)
                    ->whereIn('staff.position_id',$array_posisi)
                    ->take($take)
                    ->orderBy('average','desc')
                    ->first();
      }else{ 
        $query = $query = self::select('positions.name as posisi','staff.nama as namaStaff','staff.ava as ava','stores.name as namaStore', \DB::raw('report_staffs.score AS average'))
                    ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.id')
                    ->join('stores', 'stores.code', '=', 'staff.code_store')
                    ->join('positions', 'positions.id', '=', 'staff.position_id')
                    ->where('report_staffs.report_id', '=', $report_id)
                    ->whereIn('staff.position_id',$array_posisi)
                    ->take($take)
                    ->orderBy('average','desc')
                    ->get(); 
      }    

      return $query;          
    }

    public static function topByPositionGabung2($report_id,$array_posisi,$take)
    {
      $query = "";  
      if($take == 1)
      {

        $query = self::select('positions.name as posisi','staff.nama as namaStaff','staff.ava as ava','stores.name as namaStore', \DB::raw('report_staffs.score_individu AS average'))
                    ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.id')
                    ->join('stores', 'stores.code', '=', 'staff.code_store')
                    ->join('positions', 'positions.id', '=', 'staff.position_id')
                    ->where('report_staffs.report_id', '=', $report_id)
                    ->whereIn('staff.position_id',$array_posisi)
                    ->take($take)
                    ->orderBy('average','desc')
                    ->first();
      }else{ 
        $query = $query = self::select('positions.name as posisi','staff.nama as namaStaff','staff.ava as ava','stores.name as namaStore', \DB::raw('report_staffs.score_individu AS average'))
                    ->leftJoin('report_staffs', 'report_staffs.staff_id', '=', 'staff.id')
                    ->join('stores', 'stores.code', '=', 'staff.code_store')
                    ->join('positions', 'positions.id', '=', 'staff.position_id')
                    ->where('report_staffs.report_id', '=', $report_id)
                    ->whereIn('staff.position_id',$array_posisi)
                    ->take($take)
                    ->orderBy('average','desc')
                    ->get(); 
      }    

      return $query;          
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store','code_store');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\Position','position_id');
    }

    public function report()
    {
        return $this->belongsTo('App\Models\Report','report_id');
    }

    public function reportstaff()
    {
         return $this->hasOne('App\Models\ReportStaff','staff_id');
    }
}
