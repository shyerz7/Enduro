<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Store extends Model
{
    protected $fillable = [
        'code',
        'name',
        'latitude',
        'longitude',
        'ava',
        'user_id',
        'kota_code',
        'address',
        'is_main',
        'type',
        'so1',
        'so2',
        'so3'
    ];

    protected $primaryKey = 'code'; // or null

    public $incrementing = false;

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function data()
    {
        return self::whereNull('is_main')->orderBy('name')->get();
    }

    /**
     * @param $report
     *
     * @return array|static[]
     */
    public static function ranks($report_id)
    {
        return self::whereNull('is_main')
                 ->select('stores.code as code','stores.name as name', DB::raw('IF(report_stores.score is null, 0,score) AS score'))
                 ->leftJoin('report_stores', function ($leftJoin) use ($report_id)
                   {
                       $leftJoin->on('report_stores.code_store', '=', 'stores.code')
                            ->where('report_stores.report_id', '=', $report_id);
                   })
                 ->latest('score')
                 ->get()
                 ->toArray();
    }

    /**
     * @param $report
     * @param $cabang
     *
     * @return array|static[]
     */
    public static function ranksInKota($report,$kota)
    {
        return self::whereNull('is_main')
                   ->select('stores.code as code','stores.name as name', DB::raw('report_stores.score AS average, kotas.name as nama_kota, kotas.code as code_kota'))
                   ->leftJoin('kotas', 'stores.kota_code', '=', 'kotas.code')
                   ->leftJoin('report_stores', function ($leftJoin) use ($report)
                   {
                       $leftJoin->on('report_stores.code_store', '=', 'stores.code')
                            ->where('report_stores.report_id', '=', $report);
                   })
                   ->where('kotas.code', '=', $kota->code)
                   // ->groupBy('stores.code')
                   ->latest('average')
                   ->get();
    }

     /**
     * @param $cabang
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function dataByKota($kota)
    {
        return self::select('stores.code as code', 'stores.name as nama_store', 'stores.address as alamat_store', 'kotas.code as code_kota')
                   ->join('kotas', 'kotas.code', '=', 'stores.kota_code')
                   ->where('kotas.code', '=', $kota->code)
                   ->orderBy('stores.name')
                   ->get();
    }

    /**
     * @param $report
     * @param $max
     *
     * @return array|static[]
     */
    public static function topStore($report, $max)
    {
        return self::select('stores.name as nama', \DB::raw('report_stores.score AS average'))
            ->leftJoin('report_stores', 'report_stores.code_store', '=', 'stores.code')
            ->where('report_stores.report_id', '=', $report)
            ->take($max)
            ->orderBy('average','desc')
            ->first();
    }

    /**
     * @param $report
     * @param $max
     *
     * @return array|static[]
     */
    public static function reportStoreByKkota($report, $kota_code)
    {
        return self::select('stores.name as nama', \DB::raw('report_stores.score AS average'))
            ->leftJoin('report_stores', 'report_stores.code_store', '=', 'stores.code')
            ->where('report_stores.report_id', '=', $report)
            ->where('stores.kota_code', '=', $kota_code)
            ->orderBy('average','desc')
            ->get();
    }



    /**
     * [user description]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function kota()
    {
        return $this->belongsTo('App\Models\Kota','kota_code');
    }

    public function reportstore()
    {
         return $this->hasOne('App\Models\ReportStore','code_store');
    }

    /**
     * @param $report
     *
     * @return array|static[]
     */
    public static function topPerformance($report)
    {
        return self::select('stores.name as namaStore',DB::raw('report_stores.score as average'))
            ->join('report_stores', 'report_stores.code_store', '=', 'stores.code')
            ->where('report_stores.report_id', '=', $report)
            ->latest('average')
            ->take(5)
            ->get();
    }

    /**
     * @param $report
     *
     * @return array|static[]
     */
    public static function lowPerformance($report)
    {
        return self::select('stores.name as namaStore',DB::raw('report_stores.score as average'))
            ->join('report_stores', 'report_stores.code_store', '=', 'stores.code')
            ->where('report_stores.report_id', '=', $report)
            ->orderBy('average','asc')
            ->take(5)
            ->get();
    }
}
