<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebSetting extends Model
{
	protected $table = 'web_settings';
	
    /**
     * @var array
     */
    protected $fillable = [
        'report_id',
        'maintenance_mode',
        'autosummary_positif',
        'autosummary_negatif',

    ];

    public function report()
    {
        return $this->belongsTo('App\Models\Report','report_id');
    }

}
