<?php

return [
    'dealer'   => [
        // semua dealer
        0 => [
            'sangat_baik'   => [75.99, 100.00],
            'cukup_baik'    => [59.99, 75.00],
            'kurang'        => [25.99, 59.00],
            'sangat_kurang' => [0.00, 25.00]
        ]
    ],
    'staff' => [
        // id posisi customer service
        1 => [
            'sangat_baik'   => [80.00, 100.00],
            'baik'          => [70.00, 79.99],
            'cukup'         => [60.00, 69.99],
            'kurang'        => [50.00, 59.99],
            'sangat_kurang' => [0.00, 49.99]
        ],
        // id posisi teller
        2 => [
            'sangat_baik'   => [80.00, 100.00],
            'baik'          => [70.00, 79.99],
            'cukup'         => [60.00, 69.99],
            'kurang'        => [50.00, 59.99],
            'sangat_kurang' => [0.00, 49.99]
        ],
        // id posisi satpam
        3 => [
            'sangat_baik'   => [80.00, 100.00],
            'baik'          => [70.00, 79.99],
            'cukup'         => [60.00, 69.99],
            'kurang'        => [50.00, 59.99],
            'sangat_kurang' => [0.00, 49.99]
        ],
        // id posisi mistery caller
        4 => [
            'sangat_baik'   => [80.00, 100.00],
            'baik'          => [70.00, 79.99],
            'cukup'         => [60.00, 69.99],
            'kurang'        => [50.00, 59.99],
            'sangat_kurang' => [0.00, 49.99]
        ],
    ],
    'fisik' => [
        // semua fisik
        0 => [
            'sangat_baik'        => [90.00, 100.00],
            'baik'               => [81.70, 89.99],
            'cukup_baik'         => [70.00, 81.69],
            'kurang_baik'        => [60.00, 69.99],
            'sangat_kurang_baik' => [0.00, 59.99]
        ],
        10 => [
            'sangat_baik'        => [95.00, 100.00],
            'baik'               => [89.00, 94.99],
            'cukup_baik'         => [79.00, 89.99],
            'kurang_baik'        => [59.00, 79.99],
            'sangat_kurang_baik' => [0.00, 59.99]
        ]
    ]
];