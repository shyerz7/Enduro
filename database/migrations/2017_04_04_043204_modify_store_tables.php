<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyStoreTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('stores');
        Schema::create('stores', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('code')->index();;
            $table->string('name');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('ava')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('kota_code');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('kota_code')->references('code')->on('kotas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
