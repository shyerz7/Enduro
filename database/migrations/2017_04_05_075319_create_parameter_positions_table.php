<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions_parameter_data', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('report_id');
            $table->unsignedInteger('position_parameter_category_id');
            $table->string('kode_atribut');
            $table->integer('sort')->nullable();
            $table->text('text');
            $table->text('positif_statement')->nullable();
            $table->text('negatif_statement')->nullable();
            $table->timestamps();
            $table->foreign('position_parameter_category_id')->references('id')->on('positions_parameter_category')->onDelete('cascade');
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positions_parameter_data');
    }
}
