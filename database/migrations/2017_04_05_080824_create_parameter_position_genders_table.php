<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterPositionGendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions_parameter_gender', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_parameter');
            $table->boolean('gender');
            $table->timestamps();
            $table->foreign('id_parameter')->references('id')->on('positions_parameter_data')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positions_parameter_gender');
    }
}
