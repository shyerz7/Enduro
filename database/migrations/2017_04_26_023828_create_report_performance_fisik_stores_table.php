<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportPerformanceFisikStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_performance_fisik_stores', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('report_id');
            $table->unsignedInteger('fisik_store_id');
            $table->unsignedInteger('category_id');
            $table->double('score')->default(0);
            $table->timestamps();
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
            $table->foreign('fisik_store_id')->references('id')->on('fisik_stores')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('fisiks_parameter_category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_performance_fisik_stores');
    }
}
