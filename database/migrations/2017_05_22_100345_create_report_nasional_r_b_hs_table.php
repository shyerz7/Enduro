<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportNasionalRBHsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_nasional_rbh', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('report_id');
            $table->boolean('is_nasional')->nullable();
            $table->string('kota_code')->nullable();
            $table->double('score')->default(0);
            $table->timestamps();
            $table->foreign('kota_code')->references('code')->on('kotas')->onDelete('cascade');
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_nasional_rbh');
    }
}
