<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(ReportStoreSeeder::class);
        // $this->call(ReportStaffSeeder::class);
        $this->call(ReportFisikStoreSeeder::class);
    }
}
