<?php

use Illuminate\Database\Seeder;
use App\Models\FisikStore;
use App\Models\Fisik;
use App\Models\Store;

class FisikStoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker 			= \Faker\Factory::create();
        $stores 		= Store::whereNull('is_main')->get();
        $fisiks 		= Fisik::get();
        $report_id 		= 1;

        foreach ($stores as $store) {

        	$i = 0;

        	foreach($fisiks as $fisik) {

        		if(($fisik->name == "ATM") && ($store->type == "kk"))
	        	{

	        		// do nothing
	        		// ngudud hela we
	        		
	        	}else{
	        		// credentials
	        		$data['report_id']      = $report_id;
	        		$data['fisik_id']       = $fisik->id;
	        		$data['code_store']     = $store->code;

			        FisikStore::create($data);

			        $i++;		
	        	}

        		

        	}


        }

    }
}
