<?php

use Illuminate\Database\Seeder;
use App\Models\FisikStore;
use App\Models\Fisik;
use App\Models\Store;
use App\Models\ReportFisikStore;

class ReportFisikStoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker 			= \Faker\Factory::create();
        $fisikstores 	= FisikStore::get();  
        $report_id 		= 1;

        foreach ($fisikstores as $fisikstore) {
        	$data['report_id']		=	$report_id;
        	$data['fisik_store_id'] =	$fisikstore->id;
        	$data['score']          =	rand(1,99);

        	ReportFisikStore::create($data);


        }
    }
}
