<?php

use Illuminate\Database\Seeder;
use App\Models\Report;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $report = new Report();
        $report->name="Wave I";
        $report->periode_start="2017-04-04";
        $report->periode_end="2017-04-29";
        $report->save();
    }
}
