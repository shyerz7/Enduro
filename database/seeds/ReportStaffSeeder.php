<?php

use Illuminate\Database\Seeder;
use App\Models\Staff;
use App\Models\Store;
use App\Models\Position;
use App\Models\ReportStaff;

class ReportStaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker 			= \Faker\Factory::create();
        $stores 		= Store::whereNull('is_main')->get();
        $positions 		= Position::get();
        $report_id = 1;

        foreach ($stores as $store) {

        	$i = 0;

        	foreach($positions as $position) {

        		// credentials
        		// credentials
		        $staff = Staff::where('report_id',$report_id)
		        				->where('position_id',$position->id)
		        				->where('code_store',$store->code)
		        				->first();

		       	if($staff)
		       	{
		       		$data['report_id']		=	$report_id;
		       		$data['staff_id']		=	$staff->id;
		       		$data['score']			=   rand(1,99);
		       		ReportStaff::create($data);
		       	} 	

        	}


        }

    }
}
