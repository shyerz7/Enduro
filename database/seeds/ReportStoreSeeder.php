<?php

use Illuminate\Database\Seeder;
use App\Models\Store;
use App\Models\ReportStore;

class ReportStoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker 			= \Faker\Factory::create();
        $stores 		= Store::whereNull('is_main')->get();
        $report_id = 1;

        foreach ($stores as $store) {
        	$data['report_id']		=	$report_id;
        	$data['code_store'] 	=	$store->code;
        	$data['score']          =	rand(1,99);

        	ReportStore::create($data);
        }
    }
}
