<?php

use Illuminate\Database\Seeder;
use App\Models\Staff;
use App\Models\Store;
use App\Models\Position;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker 			= \Faker\Factory::create();
        $stores 		= Store::whereNull('is_main')->get();
        $positions 		= Position::get();
        $report_id = 1;

        foreach ($stores as $store) {

        	$i = 0;

        	foreach($positions as $position) {

        		// credentials
        		$data['report_id']      = $report_id;
        		$data['position_id']    = $position->id;
        		$data['code_store']     = $store->code;
        		$data['kode_staff']     = substr( md5(rand()), 0, 4);
		        $data['nama']           = $faker->name;
		        if($i % 2 == 0)
		        	$data['gender']         = 1;
		        else
		        	$data['gender']         = 2;	

		        Staff::create($data);

		        $i++;

        	}


        }



    }
}
