<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat role admin sistem
		$adminSistem = new Role();
		$adminSistem->name = "admin";
		$adminSistem->display_name = "Admin Sistem";
		$adminSistem->save();

		// Membuat sample admin
		$admin = new User();
		$admin->name = 'Admin Sistem';
		$admin->email = 'admin@rad-research.com';
		$admin->password = bcrypt('123');
		$admin->save();
		$admin->attachRole($adminSistem);

		// Membuat role admin client
		$adminClient = new Role();
		$adminClient->name = "admin_client";
		$adminClient->display_name = "Admin Client";
		$adminClient->save();

		// Membuat sample admin client
		$admin_client = new User();
		$admin_client->name = 'Admin Client';
		$admin_client->email = 'admin@client.com';
		$admin_client->password = bcrypt('123');
		$admin_client->save();
		$admin_client->attachRole($adminClient);

		// Membuat role admin kota
		$adminKota = new Role();
		$adminKota->name = "admin_kota";
		$adminKota->display_name = "Admin Kota";
		$adminKota->save();

		// Membuat sample admin kota
		$adminKota = new User();
		$adminKota->name = 'Admin Kota';
		$adminKota->email = 'admin@kota1.com';
		$adminKota->password = bcrypt('123');
		$adminKota->save();
		$adminKota->attachRole($adminKota);

		// Membuat role admin store
		$store = new Role();
		$store->name = "store";
		$store->display_name = "Store";
		$store->save();

		// Membuat sample store
		$storeUser = new User();
		$storeUser->name = 'Store User';
		$storeUser->email = 'store1@ms.com';
		$storeUser->password = bcrypt('123');
		$storeUser->save();
		$storeUser->attachRole($store);



		
    }
}
