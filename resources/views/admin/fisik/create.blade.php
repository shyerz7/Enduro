@extends('admin.layouts.app')

@section('css')
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
            {!! Form::open(array('route' => array('fisiks.store'), 'files'=> false))  !!}
            
              <div class="box-body">
                  
                  <div class="form-group">
                      {!! Form::label('name', 'Nama', array('class'=>'')) !!}
                      {!! Form::text('name', old('name'), array('class'=>'form-control')) !!}
                  </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div>  

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>
  
@endsection