@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        
      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>
        
       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
      
       <div class="row">
       	
       	<div class="col-md-12">
       	   <div class="box box-primary">
               <div class="box-body">
                    <a href="{{ route('fisiks.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Fisik</a>
                    <br><br>
                   @if(count($fisiks))
                    <table class="table table-bordered" id="fisiks-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th >Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($fisiks as $fisik)
                                <tr>
                                    <td>{{ $fisik->name or '-' }}</td>
                                    <td >
                                        <a href='{{ route('admin.fisik.parameter_category.fisik', [$fisik->id]) }}' class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Input Kategori Parameter" data-container="body">
                                        <i class="fa fa-tags" aria-hidden="true"></i>
                                        </a> 
                                        <a href='{{ route('fisiks.edit', [$fisik->id]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" data-target="{{ route('fisiks.destroy', [$fisik->id]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                        <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
               </div>

            </div>	
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#fisiks-table').DataTable();
        } );
    </script>


@endsection