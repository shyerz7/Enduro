@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-12">
          <div class="box box-primary">
            
            {!!  Form::open(['route' => array('admin.fisik.parameter.store',$fisik->id,$category->id), 'class'=>''])  !!}
            <div class="box-body">

                <div class="form-group ">
                    <label class="control-label " for="nama">
                        Fisik
                    </label>
                    <input class="form-control" id="fisik" name="fisik" type="text" value="{{ $fisik->name or '-'}}" readonly/>
                </div>

                <div class="form-group ">
                    <label class="control-label " for="nama">
                        Kategori Parameter
                    </label>
                    <input class="form-control" id="kategori" name="kategori" type="text" value="{!! $category->category !!}" readonly/>
                </div> 

                <div class="form-group ">
                    <label class="control-label " for="nama">
                        Kategori Parameter <br>
                        <i>Pisahkan tanda koma ; antara kode atribut dan parameter teks ; positif statement dan negatif statement</i>    
                    </label>
                    <input class="form-control" id="teks" name="teks[]" type="text" required="required" />
                    <div class="clone"></div>
                    <br>
                    <a class="btn btn-primary" id="add_text" href="javascript:void(0);">
                        <i class="fa fa-plus"></i>
                        Parameter
                    </a>
                </div> 


            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('admin.fisik.parameter_category.fisik',$fisik->id) }}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div>  
        
      </div>

      <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Daftar Kategori {{ $category->category }} </h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    @if(count($parameters))
                        <table class="table table-bordered table-hover" id="parameter-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                            <thead>
                            <th class="text-center col-sm-1">Kode</th>
                            <th class="text-center col-sm-4">Pertanyaan</th>
                            <th class="text-center col-sm-4">Pernyataan Positif</th>
                            <th class="text-center col-sm-4">Pernyataan Negatif</th>
                            <th class="text-center col-sm-1">Aksi</th>
                            </thead>

                            <tbody>
                            <?php $i = 0; ?>

                            @foreach ($parameters as $key => $parameter)
                                <tr>
                                    <td class="text-center">{{ $parameter->kode_atribut }}</td>
                                    <td class="text-center">{{ $parameter->text }}</td>
                                    <td class="text-center">{{ $parameter->positif_statement }}</td>
                                    <td class="text-center">{{ $parameter->negatif_statement }}</td>
                                    <td class="text-center" style="width: 70px">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.fisik.parameter.edit', [$fisik->id, $category->id, $parameter->id]) }}" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Rubah" data-container="body">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" data-target="{{ route('admin.fisik.parameter.destroy', [$parameter->id]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="row">
                            <div class="col-sm-12">
                                <br />
                                <div class="alert alert-warning alert-dismissable">
                                    <i class="glyphicon glyphicon-warning-sign"></i>
                                    Belum ada data kategori untuk posisi ini.
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script src="{{ asset('admin/js/clone-text.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#parameter-table').DataTable();
        } );
    </script>
  
@endsection