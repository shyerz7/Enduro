@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class='row'>

        <div class="col-md-6">
            <div class="box box-primary">

           {!!  Form::model($parameter, array('route' => ['admin.fisik.parameter.update', $parameter->id], 'method' => 'PUT')) !!}
            <div class="box-body">

                <div class="form-group">
                    {{ Form::label('nama', 'Fisik', array('class' => 'control-label')) }}

                    {{ Form::text('nama', $fisik->name, array('class' => 'form-control', 'readonly')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('kategori', 'Kategori', array('class' => 'control-label')) }}

                    {{ Form::text('kategori', $category->category, array('class' => 'form-control', 'readonly')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('teks', 'Kode Parameter', array('class' => 'control-label')) }}

                    {{ Form::text('kode_atribut', $parameter->kode_atribut, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('teks', 'Parameter', array('class' => 'control-label')) }}

                    {{ Form::text('teks', $parameter->text, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('teks', 'Parameter', array('class' => 'control-label')) }}

                    {{ Form::text('positif_statement', $parameter->positif_statement, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('teks', 'Parameter', array('class' => 'control-label')) }}

                    {{ Form::text('negatif_statement', $parameter->negatif_statement, array('class' => 'form-control')) }}
                </div>


            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                {{ Form::submit('Simpan', array('class' => 'btn btn-primary btn-save')) }}
                <a href="{{ route('admin.fisik.parameter', [$fisik->id, $category->id]) }}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
        </div>
    </div><!-- /.row -->

      

    </section><!-- /.content -->

@endsection

@section('js')
  <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script src="{{ asset('admin/js/clone-text.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#parameter-table').DataTable();
        } );
    </script>
  
@endsection