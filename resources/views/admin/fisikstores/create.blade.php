@extends('admin.layouts.app')

@section('css')
  <link href="{!! asset('admin/css/select2.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('admin/css/select2.bootstrap.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
            {!! Form::open(array('route' => array('admin.fisikstores.simpan',$store->code), 'files'=> false))  !!}
            
              <div class="box-body">
                  
                  <div class="form-group">
                      {!! Form::label('name', 'Store', array('class'=>'')) !!}
                      {!! Form::text('name', $store->name, array('class'=>'form-control','readonly')) !!}
                  </div>

                  <div class="form-group ">
                      <label class="control-label requiredField" for="kanwil_id">
                          Fisik
                          <span class="asteriskField">
                        *
                       </span>
                      </label>
                      {!!  Form::select('fisik_id', $fisiks, old('fisik_id'), array('class'=>'form-control fisik_id'))  !!}
                  </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div> 	

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
   <script src="{!!  asset('admin/js/select2.min.js')  !!}"></script>
   <script src="{!!  asset('admin/js/select2_locale_id.js')  !!}"></script>
    <script>
        $(document).ready(function () {

            $(".fisik_id").select2({
                placeholder: 'Pilih Posisi'
            });

        });
    </script>
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>
  
@endsection