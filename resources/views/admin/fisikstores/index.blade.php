@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
       <div class="row">
       	
       	<div class="col-md-12">
            <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
                <li class="active"><a href="#datatable" data-toggle="tab"> <i class="fa fa-table" aria-hidden="true"></i> Datatable</a></li>
                {{-- <li><a href="#excel" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Staff Via Excel</a></li> --}}
                {{-- <li><a href="#ava" data-toggle="tab"><i class="fa fa-picture-o" aria-hidden="true"></i> Upload Ava Staff</a></li> --}}
             </ul>
             <div class="tab-content">
                <div class="tab-pane active" id="datatable">
                   <a href="{!! route('admin.fisikstores.create',$store->code) !!}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Fisik Store</a>
                   <br><br>  
                   @if(count($fisikstores))
                    <table class="table table-bordered" id="stores-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th style="width: 35px;">Code</th>
                            <th>Store</th>
                            <th>Fisik</th>
                            <th style="width: 80px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($fisikstores as $fisikstore)
                                <tr>
                                    <td style="width: 35px;">{{ $fisikstore->store->code  or '-' }}</td>
                                    <td style="width: 35px;">{{ $fisikstore->store->name  or '-' }}</td>
                                    <td style="width: 35px;">{{ $fisikstore->fisik->name  or '-' }}</td>
                                    <td style="width: 80px;">
                                        {{-- <a href='{{ route('admin.staffs.index', [$store->code]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Input Staff" data-container="body">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a> --}}
                                         <a href="#" data-target="{{ route('admin.fisikstores.destroy', [$fisikstore->id]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                            <i class="fa fa-trash-o"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
                </div>


             </div>
             <!-- /.tab-content -->
          </div>
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#stores-table').DataTable();
        } );
    </script>


@endsection