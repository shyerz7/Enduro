@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

      <div class="row">
       	
       	<div class="col-md-12">
       	   <div class="box box-primary">
               <div class="box-body">
                    <a href="{{ route('kotas.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Kota</a>
                    <br><br>
                   @if(count($kotas))
                    <table class="table table-bordered" id="kotas-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th style="width: 35px;">Code</th>
                            <th>Nama</th>
                            <th style="width: 80px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($kotas as $kota)
                                <tr>
                                    <td style="width: 35px;">{{ $kota->code  or '-' }}</td>
                                    <td style="width: 86px;">{{ $kota->name or '-' }}</td>
                                    <td style="width: 80px;">
                                        <a href='{{ route('kotas.edit', [$kota->code]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" data-target="{{ route('kotas.destroy', [$kota->code]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                            <i class="fa fa-trash-o"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
               </div>

            </div>	
       	</div>

       </div>
      
      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
               <div class="box-body">
                  <p class="lead">Fitur Autoupload Data Kota</p>
                  <a href="{{ route('admin.kotas.generateExcelTemplate') }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel</a>
                  {!! Form::open(['url'   =>  route('admin.kotas.importExcel'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                      <meta name="csrf-token" content="{{ csrf_token() }}">

                      <div class="form-group">
                          <label class="control-label " for="nama">
                              Upload
                          </label>
                          <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="required">
                      </div>

                      <div class="form-group">
                          <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                      </div>

                  {!! Form::close() !!}
               </div>

          </div>       
        </div>

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script src="{{ asset('admin/js/loader.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#kotas-table').DataTable();
        } );
    </script>


@endsection