<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">{{ \Session::get('report')->name }}</a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li>
                    <a href="#" >
                        @if(\Session::get('report')->maintenance_mode == 1)
                            Maintenance <span class="label label-success">Online</span>
                        @else
                             Maintenance <span class="label label-default">Offline</span>
                        @endif
                    </a>
                </li>
                <li>
                    <a href="#" >
                        @if(\Session::get('report')->autosummary_positif == 1)
                            Autosummary Positif <span class="label label-success">Online</span>
                        @else
                             Autosummary Positif <span class="label label-default">Offline</span>
                        @endif
                    </a>
                </li>
                <li>
                    <a href="#" >
                        @if(\Session::get('report')->autosummary_negatif == 1)
                            Autosummary Negatif <span class="label label-success">Online</span>
                        @else
                             Autosummary Negatif <span class="label label-default">Offline</span>
                        @endif
                    </a>
                </li>
                <li class="dropdown messages-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {{ \Session::get('report')->name }} <span class="label label-success">Aktif</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <!-- inner menu: contains the messages -->
                            <ul class="menu">

                                @foreach(\Session::get('reports') as $report)
                                    <li><!-- start message -->
                                        <a href="{!! route('admin.changeSession',$report->id) !!}">
                                            <!-- Message title and timestamp -->
                                            <h4>
                                                {{ $report->name }}
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                        </a>
                                    </li><!-- end message -->
                                @endforeach

                            </ul><!-- /.menu -->
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li><!-- /.messages-menu -->

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs"> {{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <p>
                                 {{ Auth::user()->name }}
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Home</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                            </div>
                        </li>
                    </ul>
                </li>
                

            </ul>
        </div>

    </nav>
</header>