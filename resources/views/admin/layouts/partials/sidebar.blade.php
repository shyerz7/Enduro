<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->
            {{-- <li class="{{ Request::segment(2) == 'home' ? 'active' : '' }}">
                        <a href="{{ route('admin.home') }}"><span>Homepage</span></a>
            </li> --}} 
            <li class="{{ Request::segment(2) == 'kotas' ? 'active' : '' }}">
                        <a href="{{ route('kotas.index') }}"><span>KC/ Kota</span></a>
            </li> 
            <li class="{{ Request::segment(2) == 'stores' ? 'active' : '' }}">
                        <a href="{{ route('stores.index') }}"><span>KCP / Store</span></a>
            </li> 
            <li class="{{ Request::segment(2) == 'positions' ? 'active' : '' }}">
                        <a href="{{ route('positions.index') }}"><span>Posisi</span></a>
            </li> 
            <li class="header">Fisiks</li>
            <li class="{{ Request::segment(2) == 'fisiks' ? 'active' : '' }}">
                        <a href="{{ route('fisiks.index') }}"><span>Fisik</span></a>
            </li> 
            <li class="{{ Request::segment(2) == 'fisikstores' ? 'active' : '' }}">
                        <a href="{{ route('admin.fisikstores.store') }}"><span>Fisik Store</span></a>
            </li> 
            <li class="{{ Request::segment(2) == 'staffs' ? 'active' : '' }}">
                        <a href="{{ route('admin.staffs.stores') }}"><span>Staff</span></a>
            </li>
            <li class="header">Report</li>
            <li class="{{ Request::segment(2) == 'reports' ? 'active' : '' }}">
                <a href="{{ route('reports.index') }}">Reports</a>
            </li>
            <li class="{{ Request::segment(2) == 'reportstore' ? 'active' : '' }}">
                <a href="{{ route('reportstore.index') }}">Report Store</a>
            </li> 
             <li class="{{ Request::segment(2) == 'reportnasionalrbh' ? 'active' : '' }}">
                <a href="{{ route('admin.reportnasionalrbh.edit') }}">Report Nasional & RBH</a>
            </li> 
            <li class="{{ Request::segment(2) == 'reportstaffs' ? 'active' : '' }}">
                <a href="{{ route('admin.reportstaffs.stores') }}">Report Staff</a>
            </li> 
            <li class="{{ Request::segment(2) == 'reportfisikstores' ? 'active' : '' }}">
                <a href="{{ route('admin.reportfisikstores.stores') }}">Report Fisik Store</a>
            </li> 
            <li class="{{ Request::segment(2) == 'reportparameter' ? 'active' : '' }}">
                <a href="{{ route('admin.reportparameter.index') }}">Report Parameter</a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>