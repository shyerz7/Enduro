@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#datatable" data-toggle="tab"> <i class="fa fa-table" aria-hidden="true"></i> Datatable</a></li>
                    <li><a href="#excel" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Via Excel</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="datatable">
                        {!!  Form::open(['route' => array('admin.posisi.parameter.store',$position->id,$category->id), 'class'=>''])  !!}
                            <div class="box-body">

                                <div class="form-group ">
                                    <label class="control-label " for="nama">
                                        Posisi
                                    </label>
                                    <input class="form-control" id="posisi" name="posisi" type="text" value="{{ $position->name or '-'}}" readonly/>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label " for="nama">
                                        Kategori Parameter
                                    </label>
                                    <input class="form-control" id="kategori" name="kategori" type="text" value="{!! $category->category !!}" readonly/>
                                </div> 

                                <div class="form-group ">
                                    <label class="control-label " for="nama">
                                        Gender
                                    </label>
                                    <div class=" ">
                                        <div class="checkbox">
                                            <label class="checkbox">
                                                {!! Form::checkbox('gender[]', '1')  !!}
                                                Pria
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label class="checkbox">
                                                {!! Form::checkbox('gender[]', '2')  !!}
                                                Wanita
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label " for="nama">
                                        Kategori Parameter <br>
                                        <i>Pisahkan tanda koma ; antara kode atribut dan parameter teks; positif statement dan negatif statement</i>    
                                    </label>
                                    <input class="form-control" id="teks" name="teks[]" type="text" required="required" />
                                    <div class="clone"></div>
                                    <br>
                                    <a class="btn btn-primary" id="add_text" href="javascript:void(0);">
                                        <i class="fa fa-plus"></i>
                                        Parameter
                                    </a>
                                </div> 


                            </div>
                              <!-- /.box-body -->

                              <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ route('admin.posisi.parameter_category.position',$position->id) }}" class="btn btn-default">Batal</a>
                              </div>

                            {!! Form::close() !!}
                    </div>

                    <div class="tab-pane fade" id="excel">
                        <a href="{{ route('admin.parameter.positions.data.generateExcelTemplate') }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel</a>
                        {!! Form::open(['url'   =>  route('admin.position.parameter.importExcel'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                              <meta name="csrf-token" content="{{ csrf_token() }}">

                              <div class="form-group">
                                  <label class="control-label " for="nama">
                                      Upload
                                  </label>
                                  <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                              </div>

                              <div class="form-group">
                                  <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                              </div>

                          {!! Form::close() !!}
                    </div>
                </div>    
            </div>
        </div>  
        
      </div><!-- end row -->

      <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Daftar Kategori {{ $position->name }} </h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    @if(count($parameters))
                        <table class="table table-bordered table-hover" id="parameter-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                            <thead>
                            <th class="text-center col-sm-1">ID</th>
                            <th class="text-center col-sm-1">Gender</th>
                            <th class="text-center col-sm-1">Kode</th>
                            <th class="text-center col-sm-8">Pertanyaan</th>
                            <th class="text-center col-sm-3">Aksi</th>
                            </thead>

                            <tbody>
                            <?php $i = 0; ?>

                            @foreach ($parameters as $key => $parameter)
                                <tr>
                                    <td class="text-center">{{ $parameter->id }}</td>
                                    <td class="text-center">{{ implode(', ', array_column($parameter->parameter_gender->toArray(), 'gender')) }}</td>
                                    <td class="text-center">{{ $parameter->kode_atribut }}</td>
                                    <td class="text-center">{{ $parameter->text }}</td>
                                    <td class="text-center" style="width: 70px">
                                        <div class="btn-group">
                                            <a href="{{ route('admin.posisi.parameter.edit', [$position->id, $category->id, $parameter->id]) }}" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Rubah" data-container="body">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="#" data-target="{{ route('admin.posisi.parameter.destroy', [$parameter->id]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="row">
                            <div class="col-sm-12">
                                <br />
                                <div class="alert alert-warning alert-dismissable">
                                    <i class="glyphicon glyphicon-warning-sign"></i>
                                    Belum ada data kategori untuk posisi ini.
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script src="{{ asset('admin/js/clone-text.js') }}"></script>
    <script src="{{ asset('admin/js/loader.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#parameter-table').DataTable({
                "order": [[ 0, "asc" ]]
            } );
        } );
    </script>
  
@endsection