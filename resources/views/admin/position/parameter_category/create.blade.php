@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-4">
          <div class="box box-primary">
            
            {!!  Form::open(['route' => array('admin.posisi.parameter_category.store',$id_posisi), 'class'=>''])  !!}
            <div class="box-body">

                <div class="form-group ">
                    <label class="control-label " for="nama">
                        Nama Posisi
                    </label>
                    <input class="form-control" id="nama" name="nama" type="text" value="{!! $position->name !!}" disabled>
                </div>

                <div class="form-group ">
                    <label class="control-label " for="nama">
                        Kategori Parameter
                    </label>
                    <input class="form-control" id="category" name="category" placeholder="Masukan kategori parameter" type="text" required="required" />
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('positions.index') }}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div>  
        
         <div class="col-md-8">
            <div class="box box-primary">
               
               <div class="box-body">
                   @if(count($categories))
                    <table class="table table-bordered" id="category-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($categories as $category)
                              <tr>
                                  <td>{{ $category->id }}</td>
                                  <td>{{ $category->category or '-' }}</td>
                                  <td>
                                    <a href="{{ route('admin.posisi.parameter',[$position->id,$category->id]) }}" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Rubah" data-container="body">   
                                      <i class="fa fa-signal"></i></a>
                                    <a href="{{ route('admin.posisi.parameter_category.position.edit',[$position->id,$category->id]) }}" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Rubah" data-container="body">   
                                      <i class="fa fa-edit"></i></a>
                                    <a href="#" data-target="{{ route('admin.posisi.parameter_category.position.destroy',$category->id) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)"><i class="fa fa-trash-o"></i></a></td>
                              </tr>
                          @endforeach
                        </tbody>
                    </table>
                    @else
                      @include('admin.layouts.partials.alertWarning')
                    @endif
               </div>

            </div>
        </div>

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#category-table').DataTable({
                "order": [[ 0, "asc" ]]
            } );
        } );
    </script>
  
@endsection