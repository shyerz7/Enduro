@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-4">
          <div class="box box-primary">
            
            {!!  Form::model($category, array('route' => ['admin.posisi.parameter_category.position.update', $position->id,$category->id], 'method' => 'PUT')) !!}
            <div class="box-body">

                <div class="form-group ">
                    <label class="control-label " for="nama">
                        Nama Posisi
                    </label>
                    <input class="form-control" id="nama" name="nama" type="text" value="{!! $position->name !!}" disabled>
                </div>

                <div class="form-group ">
                    <label class="control-label " for="nama">
                        Kategori Parameter
                    </label>
                    <input class="form-control" id="category" name="category"
                           value="{!! $category->category !!}" type="text"/>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('positions.index') }}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div>  

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>
  <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#category-table').DataTable();
        } );
    </script>
  
@endsection