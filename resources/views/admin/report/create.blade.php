@extends('admin.layouts.app')

@section('css')
  <link href="{!! asset('admin/css/daterangepicker-bs3.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
            {!! Form::open(array('route' => array('reports.store'), 'files'=> true))  !!}
              <div class="box-body">
                  
                  <div class="form-group">
                      {!! Form::label('name', 'Nama', array('class'=>'')) !!}
                      {!! Form::text('name', old('name'), array('class'=>'form-control')) !!}
                  </div>

                <div class="form-group">
                      {!! Form::label('periode', 'Periode', array('class'=>'')) !!}

                      <div class="input-group">
                         <span class="input-group-addon">
                             <i class="fa fa-calendar"></i>
                         </span>
                          {!! Form::text('periode', isset($report) ? date('m/d/Y', strtotime($report->periode_start)) . ' - ' . date('m/d/Y', strtotime($report->periode_end)) : old('periode'), array('id'=>'periode', 'placeholder' => '01/29/2013 - 01/31/2013', 'class' => 'form-control daterange','required'=>'required')) !!}
                          {!! Form::hidden('periode_start', old('periode_start'), array('class'=>'daterangestart')) !!}
                          {!! Form::hidden('periode_end', old('periode_end'), array('class'=>'daterangeend')) !!}
                      </div>

                      <div class="input-append">
                          <span class="add-on btn date_picker"></span>
                      </div>
                  </div>

                   <div class="form-group">
                      {!! Form::label('Kuisioner', 'Kuisioner', array('class'=>'')) !!}
                      {!! Form::file('kuisioner', array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('maintenane', 'Maintenance Mode', array('class'=>'')) !!}
                      <input name="maintenance_mode" type="radio" value="1" id="maintenance_mode">Online
                      <input name="maintenance_mode" type="radio" value="0" id="maintenance_mode" checked="checked">Offline
                  </div>

                  <div class="form-group">
                      {!! Form::label('maintenane', 'Autosummary Positif Mode', array('class'=>'')) !!}
                      <input name="autosummary_positif" type="radio" value="1" id="autosummary_positif">Online
                      <input name="autosummary_positif" type="radio" value="0" id="autosummary_positif" checked="checked">Offline
                  </div>

                  <div class="form-group">
                      {!! Form::label('maintenane', 'Autosummary Negatif', array('class'=>'')) !!}
                      <input name="autosummary_negatif" type="radio" value="1" id="autosummary_negatif">Online
                      <input name="autosummary_negatif" type="radio" value="0" id="autosummary_negatif" checked="checked">Offline
                  </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div> 	

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>
  <script src="{!!  asset('admin/js/moment.min.js')  !!}"></script>
  <script src="{!!  asset('admin/js/daterangepicker.js')  !!}"></script>
  <script src="{!!  asset('admin/js/date.js')  !!}"></script>
  
@endsection