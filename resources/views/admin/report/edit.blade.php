@extends('admin.layouts.app')

@section('css')
  <link href="{!! asset('admin/css/daterangepicker-bs3.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
           {!!  Form::model($report, array('method' => 'put', 'route' => array('reports.update', $report->id), 'files'=> true)) !!}
              <div class="box-body">
                  
                  <div class="form-group">
                      {!! Form::label('name', 'Nama', array('class'=>'')) !!}
                      {!! Form::text('name', old('name'), array('class'=>'form-control','required'=>'required')) !!}
                  </div>

                <div class="form-group">
                      {!! Form::label('periode', 'Periode', array('class'=>'')) !!}

                      <div class="input-group">
                         <span class="input-group-addon">
                             <i class="fa fa-calendar"></i>
                         </span>
                          {!! Form::text('periode', isset($report) ? date('m/d/Y', strtotime($report->periode_start)) . ' - ' . date('m/d/Y', strtotime($report->periode_end)) : old('periode'), array('id'=>'periode', 'placeholder' => '01/29/2013 - 01/31/2013', 'class' => 'form-control daterange','required'=>'required')) !!}
                          {!! Form::hidden('periode_start', old('periode_start'), array('class'=>'daterangestart')) !!}
                          {!! Form::hidden('periode_end', old('periode_end'), array('class'=>'daterangeend')) !!}
                      </div>

                      <div class="input-append">
                          <span class="add-on btn date_picker"></span>
                      </div>
                  </div>

                   <div class="form-group">
                      {!! Form::label('Kuisioner', 'Kuisioner', array('class'=>'')) !!}
                      {!! Form::file('kuisioner', array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('maintenane', 'Maintenance Mode', array('class'=>'')) !!} <br>
                      {!! Form::radio('maintenance_mode', '0',($report->maintenance_mode == 0 || is_null($report->maintenance_mode)) ? true : false); !!} Offline
                      {!! Form::radio('maintenance_mode', '1',$report->maintenance_mode == 1 ? true : false); !!} Online
                  </div>

                  <div class="form-group">
                      {!! Form::label('maintenane', 'Autosummary Positif Mode', array('class'=>'')) !!} <br>
                      {!! Form::radio('autosummary_positif', '0',($report->autosummary_positif == 0 || is_null($report->autosummary_positif)) ? true : false); !!} Offline
                      {!! Form::radio('autosummary_positif', '1',$report->autosummary_positif == 1 ? true : false); !!} Online
                  </div>

                  <div class="form-group"> 
                      {!! Form::label('maintenane', 'Autosummary Negatif Mode', array('class'=>'')) !!} <br>
                      {!! Form::radio('autosummary_negatif', '0',($report->autosummary_negatif == 0 || is_null($report->autosummary_negatif)) ? true : false); !!} Offline
                      {!! Form::radio('autosummary_negatif', '1',$report->autosummary_negatif == 1 ? true : false); !!} Online
                  </div>

              </div>


              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div> 	

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script src="{!!  asset('admin/js/moment.min.js')  !!}"></script>
  <script src="{!!  asset('admin/js/daterangepicker.js')  !!}"></script>
  <script src="{!!  asset('admin/js/date.js')  !!}"></script>
@endsection