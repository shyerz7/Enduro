@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
       <div class="row">
       	
       	<div class="col-md-12">
       	   <div class="box box-primary">
               <div class="box-body">
                    <a href="{{ route('reports.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Report</a>
                    <br><br>
                   @if(count($reports))
                    <table class="table table-bordered" id="reports-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th style="width: 35px;">ID</th>
                            <th>Nama</th>
                            <th>Periode</th>
                            <th>Kuisioner</th>
                            <th>Mode Maintenance</th>
                            <th>AutoSummary Positif</th>
                            <th>AutoSummary Negatif</th>
                            <th style="width: 80px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($reports as $report)
                                <tr>
                                    <td style="width: 35px;">{{ $report->id or '-' }}</td>
                                    <td style="width: 86px;">{{ $report->name or '-' }}</td>
                                    <td>{{ date("d/m/Y",strtotime($report->periode_start)) }} - {{ date("d/m/Y",strtotime($report->periode_end)) }}</td>
                                    <td>
                                      @if($report->kuisioner)
                                        <a href="{{ route('admin.downloadKuisioner', [$report->id]) }}" class="btn btn-success btn-xs">Sudah upload <i class="fa fa-download" aria-hidden="true"></i></a>
                                      @else
                                        <span class="label label-danger">Belum diupload</span>
                                      @endif
                                    </td>
                                    <td>
                                      @if($report->maintenance_mode ==1 )
                                        <span class="label label-success">Online</span>
                                      @else
                                        <span class="label label-default">Offline</span>
                                      @endif
                                    </td>
                                    <td>
                                      @if($report->autosummary_positif ==1 )
                                        <span class="label label-success">Online</span>
                                      @else
                                        <span class="label label-default">Offline</span>
                                      @endif
                                    </td>
                                    <td>
                                      @if($report->autosummary_negatif ==1 )
                                        <span class="label label-success">Online</span>
                                      @else
                                        <span class="label label-default">Offline</span>
                                      @endif
                                    </td>
                                    <td style="width: 80px;">
                                        <a href='{{ route('reports.edit', [$report->id]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" data-target="{{ route('reports.destroy', [$report->id]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                            <i class="fa fa-trash-o"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
               </div>

            </div>	
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#reports-table').DataTable();
        } );
    </script>
@endsection