@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/fileinput.min.css') }}" rel="stylesheet">


@endsection

@section('content')
	<section class="content">
		

        <div class="row">
	        <div class="col-md-12">

	          {{-- Untuk Pesan Error --}}
	          @if (count($errors) > 0)
	              <div class="alert alert-danger">
	                  <ul>
	                      @foreach ($errors->all() as $error)
	                          <li>{{ $error }}</li>
	                      @endforeach
	                  </ul>
	              </div>
	          @endif

	        </div>
	    </div>

	    <div class="row">
        
        	<div class="col-md-12">
          		<div class="box box-primary">
            
           {!!  Form::model($fisikstore, array('method' => 'put', 'route' => array('admin.reportfisikstores.update', $fisikstore->id), 'files'=> false)) !!}
            
	              <div class="box-body">
	                  
		                <div class="form-group">
		                      {!! Form::label('name', 'Store', array('class'=>'')) !!}
		                      {!! Form::text('name', $fisikstore->store->name, array('class'=>'form-control','readonly')) !!}
		                </div>

		                <div class="form-group ">
		                       {!! Form::label('Fisik', 'Fisik', array('class'=>'')) !!}
		                       {!! Form::text('Fisik', $fisikstore->fisik->name, array('class'=>'form-control','readonly')) !!}
		                </div>

		                <div class="form-group ">
			                    <label class="control-label " for="score">
			                        Score
			                    </label>
			                    <div class="input-group">
			                        <div class="input-group-addon">
			                            <i class="fa fa-book">
			                            </i>
			                        </div>
			                        <input class="form-control" id="score" name="score" type="text" value="{!! $fisikstore->score->score or '' !!}">
			                    </div>
			            </div>


						@if(count($fisikstore['fisik']['parameter_category']))
                            @foreach($fisikstore['fisik']['parameter_category'] as $key => $category)
                                <div class="form-group">
                                    <label for="{{ $category->category }}">
	                                    <h4> Kategori - {{ $category->category }} </h4>
	                                </label>

                                    <br>

                                    <?php

                                    	$value = App\Models\ReportPerformanceFisikStore::where('report_id', '=', \Session::get('report')->id)
                                    												   ->where('fisik_store_id', '=', $fisikstore->id)
                                    												   ->where('category_id', '=', $category->id)
                                    												   ->first();

                                    ?>
									
									<div class="form-group">
				                      <label for="gender">Input Nilai {{ $category->category }}</label>
				                      {!! Form::text('performance['.$category->id.']', $value ? $value->score : '', array('class'=>'form-control')) !!}
				                    </div>

                                    {{--query untuk mengeluarkan parameter data parameter sesuai categori--}}
                                    <?php $parameter = App\Models\ParameterFisik::where('fisik_parameter_category_id', $category->id)
                                            ->get(); ?>

                                    @if(count($parameter))
                                        <table class="table table-bordered table-hover">
                                            @foreach($parameter as $param)

                                                {{--query untuk mengeluarka parameter report dari parameter data--}}
                                                <?php $value = App\Models\ParameterFisikReport::where('fisik_store_id', '=', $fisikstore->id)
                                                        ->where('fisik_parameter_data_id', '=', $param->id)
                                                        ->where('report_id', '=', Session::get('report')->id)
                                                        ->first(); ?>

                                                <tr>
                                                    <td class="text-center" style="width: 10%">
                                                        {{ Form::select('parameter['.$param->id.']', [0 => 'N/A', 1 => 'Ya', 2 => 'Tidak'], ($value) ? $value->status : null, ['class' => 'form-control']) }}
                                                    </td>

                                                    <td style="width: 45%"> <strong>{{ $param->kode_atribut }}</strong> -  {{ $param->text }}</td>
                                                    <td class="text-center" style="width: 45%">
                                                        {{ Form::text('komentar['.$param->id.']',old('komentar', ($value) ? $value->comment : null), array('class' => 'form-control', 'placeholder' => 'Komentar...', 'maxlength' => 255)) }}
                                                    </td>
                                                </tr>

                                            @endforeach
                                        </table>
                                    @else
                                        <div class="alert alert-warning alert-dismissable" style="margin-top: 20px">
                                            <i class="glyphicon glyphicon-warning-sign"></i>
                                            Belum ada parameter untuk kategori ini.
                                        </div>
                                    @endif
                                </div>
                            @endforeach

                        @else
                            <div class="alert alert-warning alert-dismissable" style="margin-top: 30px">
                                <i class="glyphicon glyphicon-warning-sign"></i>
                                Belum ada kategori parameter untuk fisik ini.
                            </div>
                        @endif


	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Submit</button>
	                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
	              </div>

	            {!! Form::close() !!}

	            </div>
	          </div> 	

	      </div>
		
		
	</section>
@endsection

@section('js')
  {{-- <script src="{!!  asset('admin/js/jquery.form.min.js')  !!}"></script> --}}
  <script src="{!!  asset('admin/js/ckeditor/ckeditor.js')  !!}"></script>
  <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('admin/js/confirm.js') }}"></script>
@endsection