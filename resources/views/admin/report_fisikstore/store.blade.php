@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
       <div class="row">
       	
       	<div class="col-md-12">
            <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
                <li class="active"><a href="#datatable" data-toggle="tab"> <i class="fa fa-table" aria-hidden="true"></i> Datatable</a></li>
                {{-- <li><a href="#excel" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Staff Via Excel</a></li> --}}
                {{-- <li><a href="#ava" data-toggle="tab"><i class="fa fa-picture-o" aria-hidden="true"></i> Upload Ava Staff</a></li> --}}
             </ul>
             <div class="tab-content">
                <div class="tab-pane active" id="datatable">
                   @if(count($stores))
                    <table class="table table-bordered" id="stores-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th style="width: 35px;">Code</th>
                            <th>Kota</th>
                            <th>Nama</th>
                            <th style="width: 80px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($stores as $store)
                                <tr>
                                    <td style="width: 35px;">{{ $store->code  or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->kota->name or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->name or '-' }}</td>
                                    <td style="width: 80px;">
                                        <a href='{{ route('admin.reportfisikstores.index', [$store->code]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Lihat Report Fisik Store" data-container="body">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
                </div>


             </div>
             <!-- /.tab-content -->
          </div>
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#stores-table').DataTable();
        } );
    </script>


@endsection