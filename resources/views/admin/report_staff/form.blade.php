@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/fileinput.min.css') }}" rel="stylesheet">


@endsection

@section('content')
	<section class="content">
		

        <div class="row">
	        <div class="col-md-12">

	          {{-- Untuk Pesan Error --}}
	          @if (count($errors) > 0)
	              <div class="alert alert-danger">
	                  <ul>
	                      @foreach ($errors->all() as $error)
	                          <li>{{ $error }}</li>
	                      @endforeach
	                  </ul>
	              </div>
	          @endif

	        </div>
	    </div>

	    <div class="row">
        
        	<div class="col-md-12">
          		<div class="box box-primary">
            
           {!!  Form::model($staff, array('method' => 'put', 'route' => array('admin.reportstaffs.update', $staff->id), 'files'=> true)) !!}
            
	              <div class="box-body">
	                  
		                  <div class="form-group">
		                      {!! Form::label('name', 'Store', array('class'=>'')) !!}
		                      {!! Form::text('name', $staff->store->name, array('class'=>'form-control','readonly')) !!}
		                  </div>

		                  <div class="form-group ">
		                       {!! Form::label('posisi', 'Posisi', array('class'=>'')) !!}
		                       {!! Form::text('posisi', $staff->position->name, array('class'=>'form-control','readonly')) !!}
		                  </div>

		                  <div class="form-group">
		                      {!! Form::label('kode_staff', 'Kode Staff', array('class'=>'')) !!}
		                      {!! Form::text('kode_staff', old('kode_staff'), array('class'=>'form-control','readonly')) !!}
		                  </div>

		                  <div class="form-group">
		                      {!! Form::label('name', 'Nama', array('class'=>'')) !!}
		                      {!! Form::text('nama', old('nama'), array('class'=>'form-control','readonly')) !!}
		                  </div>

		                  <div class="form-group">
		                      <label for="gender">Gender</label>
		                      {!! Form::text('gender', $staff->gender == 1 ? 'pria':'wanita', array('class'=>'form-control','readonly')) !!}
		                  </div>

		                   <div class="form-group ">
			                    <label class="control-label " for="score">
			                        Score
			                    </label>
			                    <div class="input-group">
			                        <div class="input-group-addon">
			                            <i class="fa fa-book">
			                            </i>
			                        </div>
			                        <input class="form-control" id="score" name="score" type="text" value="{!! $staff->reportstaff->score or '' !!}">
			                    </div>
			                </div>

			                <div class="form-group">
		                      {{ Form::label('content', 'Konten', array('class'=>'control-label')) }}
                              {{ Form::textarea('summary', ($staff['reportstaff']) ? $staff['reportstaff']->summary : '', array('class'=>'ckeditor')) }}
		                    </div>

		                    {{--untuk mendapatkan posisi parameter category dari posisi yang dipilih staff--}}
                        	@if(count($staff['position']['parameter_category']))
                            	@foreach($staff['position']['parameter_category'] as $key => $category)
									<div class="form-group">
	                                    <label for="{{ $category->category }}">
	                                        <h2> Kategori - {{ $category->category }} </h2>
	                                    </label>
										
										<br>
										
										<?php

											$value = App\Models\ReportPerformanceStaff::where('staff_id', '=', $staff->id)
                                                            ->where('category_id', '=', $category->id)
                                                            ->where('report_id', '=', \Session::get('report')->id)
                                                            ->first(); 

										?>
										
										<!--
										<div class="form-group">
					                      {{-- <label for="gender">Input Nilai {{ $category->category }}</label> --}}
					                      {{-- {!! Form::text('performance['.$category->id.']', $value ? $value->score : '', array('class'=>'form-control')) !!} --}}
					                    </div>-->		



	                                    <?php $parameter = $category['parameter_data'];  ?>

	                                    @if(count($parameter))
											<table class="table table-bordered table-hover">
												@foreach($parameter as $param)
													<?php

													    $ada_gender   = false;
	                                                    $parameter_gender = 0;

	                                                    foreach ($param['parameter_gender']->toArray() as $value) {
												            if($value['gender'] == $staff->gender) {
												                $ada_gender = true;
												                break;
												            }
												        }

	                                                ?>

	                                                  @if($ada_gender)

														  <?php 
														   $value = App\Models\ParameterReportPosition::where('staff_id', '=', $staff->id)
                                                            ->where('position_parameter_data_id', '=', $param->id)
                                                            ->where('report_id', '=', Session::get('report')->id)
                                                            ->first(); ?>

                                                            <tr>
		                                                        <td class="text-center" style="width: 10%">
		                                                            {{ Form::select('parameter['.$param->id.']', [0 => 'N/A', 1 => 'Ya', 2 => 'Tidak'], ($value) ? $value->status : null, ['class' => 'form-control']) }}
		                                                        </td>

		                                                        <td style="width: 45%"> <strong>{!! $param->kode_atribut !!}</strong> - {{ $param->text }}</td>
		                                                        <td class="text-center" style="width: 45%">
		                                                            {{ Form::text('komentar['.$param->id.']',old('komentar', ($value) ? $value->comment : null), array('class' => 'form-control', 'placeholder' => 'Komentar...', 'maxlength' => 255)) }}
		                                                        </td>
		                                                    </tr>
	                                                @endif

												@endforeach
											</table>
	                                    @else
	                                        <div class="alert alert-warning alert-dismissable" style="margin-top: 20px">
	                                            <i class="glyphicon glyphicon-warning-sign"></i>
	                                            Belum ada parameter untuk kategori ini.
	                                        </div>
	                                    @endif

                                    </div>    
                        	    @endforeach
	                        @else
	                            <div class="alert alert-warning alert-dismissable" style="margin-top: 30px">
	                                <i class="glyphicon glyphicon-warning-sign"></i>
	                                Belum ada kategori parameter untuk posisi ini.
	                            </div>
	                        @endif

	                        <div class="form-group">
		                      <label for="gender">File Multimedia</label>
		                      {!! Form::file('file',['class'=>'form-control']); !!}
		                    </div>


	              </div>
	              <!-- /.box-body -->

	              <div class="box-footer">
	                <button type="submit" class="btn btn-primary">Submit</button>
	                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
	              </div>

	            {!! Form::close() !!}

	            </div>
	          </div> 	

	      </div>
		
		
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					@if(count($files))
						<table class="table table-bordered" id="stores-table">
	                        <meta name="csrf-token" content="{{ csrf_token() }}">
	                        <thead>
	                        <tr>
	                            <th>Nama File</th>
	                            <th>Categori</th>
	                            <th>Action</th>
	                        </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($files as $file)
	                                <tr>
	                                    <td>{{ $file->filename  or '-' }}</td>
	                                    <td>{{ $file->filetype  or '-' }}</td>
	                                    <td style="width: 80px;">
	                                        <a href="#" data-target="{{ route('admin.file.staff.destroy', [$file->id]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                            <i class="fa fa-trash-o"></i>
                                            </a>
	                                    </td>
	                                </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
					@else
						@include('admin.layouts.partials.alertWarning')
					@endif
				</div>
			</div>
		</div>

	</section>
@endsection

@section('js')
  {{-- <script src="{!!  asset('admin/js/jquery.form.min.js')  !!}"></script> --}}
  <script src="{!!  asset('admin/js/ckeditor/ckeditor.js')  !!}"></script>
  <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('admin/js/confirm.js') }}"></script>
@endsection