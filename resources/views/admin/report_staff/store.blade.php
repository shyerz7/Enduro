@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/fileinput.min.css') }}" rel="stylesheet">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          @if (session()->has('flash_notification.message'))
            <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! session()->get('flash_notification.message') !!}
            </div>
           @endif

        </div>
      </div>

       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
       <div class="row">

       	<div class="col-md-12">
            <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
                <li class="active"><a href="#datatable" data-toggle="tab"> <i class="fa fa-table" aria-hidden="true"></i> Datatable</a></li>
                <li><a href="#excel" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Staff Via Excel Ceklis dan Autosummary</a></li>
                <li><a href="#filezila" data-toggle="tab"><i class="fa fa-picture-o" aria-hidden="true"></i> Upload File Multimedia Via Filezilla</a></li>
                <li><a href="#score" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Autouload Score Staff</a></li>
             </ul>
             <div class="tab-content">

                <div class="tab-pane active" id="datatable">
                   @if(count($stores))
                    <table class="table table-bordered" id="stores-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th style="width: 35px;">Code</th>
                            <th>Kota</th>
                            <th>Nama</th>
                            <th style="width: 80px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($stores as $store)
                                <tr>
                                    <td style="width: 35px;">{{ $store->code  or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->kota->name or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->name or '-' }}</td>
                                    <td style="width: 80px;">
                                        <a href='{{ route('admin.reportstaffs.index', [$store->code]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Input Staff" data-container="body">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
                </div>

                <div class="tab-pane" id="excel">
                   <div class="row">
                        <div class="col-md-6">
                            <div class="callout callout-warning">
                                <h4>Mohon Diperhatikan!</h4>

                                <ul>
                                    <li>Isi nilai hanya yang ada kode atribut nya saja</li>
                                    <li>Isi kode atribut dengan angka 0=>N/A; 1=Ya; 2=Tidak</li>
                                    <li>Isi kode atribut ditambah akhiran '_c' itu untuk upload komentar</li>
                                    <li>Untuk pengisian kolom <strong>gender</strong> pria = 1 dan wanita =2</li>
                                </ul>
                            </div>

                            {!! Form::open(['url'   =>  route('admin.reportstrore.importExcelReport'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                
                                <div class="form-group ">
                                    <label class="control-label requiredField" for="kanwil_id">
                                        Posisi
                                        <span class="asteriskField">
                                      *
                                     </span>
                                    </label>
                                    {!!  Form::select('position_id', $positionsList, old('position_id'), array('class'=>'form-control posisi_id'))  !!}
                                </div>


                                <div class="form-group">
                                    <label class="control-label " for="nama">
                                        Upload
                                    </label>
                                    <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                                </div>

                                <div class="form-group">
                                    <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                                </div>

                            {!! Form::close() !!}

                        </div>

                       <div class="col-md-6">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Daftar Kode Posisi</h3>
                                </div>
                                <div class="box-body no-padding">
                                  @if(count($positions))
                                    <table class="table table-bordered" id="posisi-table">
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <thead>
                                        <tr>
                                            <th style="width: 35px;">Kode Posisi</th>
                                            <th>Nama Posisi</th>
                                            <th>Download Template</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($positions as $position)
                                                <tr>
                                                    <td style="width: 35px;"><span class="label label-success" style="font-size: 11pt;">{{ $position->id  or '-' }}</span></td>
                                                    <td style="width: 86px;">{{ $position->name or '-' }}</td>
                                                    <td style="width: 86px;">
                                                      <a href="{{ route('admin.reportstaff.generateExcelTemplatePosition',$position->id) }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                        @include('admin.layouts.partials.alertWarning')
                                    @endif
                                </div>
                            </div>
                       </div>

                   </div> 


                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="filezila">

                  <div class="row">

                    <div class="col-md-6">

                      <div class="alert alert-info alert-dismissible">
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        Untuk menggunakan fitur ini gunakan <br>
                        <strong><a href="https://filezilla-project.org/download.php" target="_blank">Filezilla CLient</a></strong>
                        <ul>
                          <li>untuk kode_staff gunakan kode staff yang sudah diupload</li>
                          <li>untuk filetype isi dengan <strong>mp3</strong> atau <strong>mp4</strong></li>
                          <li>untuk filename isi dengan format (kode_staff)-(generateCode).mp3/mp4 </li>
                        </ul>
                      </div>

                      <a href="{!! route('admin.reportstaff.generateExcelTemplateValue') !!}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel</a>
                    </div>

                    <div class="col-md-6">
                      {!! Form::open(['url'   =>  route('admin.reportstaff.importFileMultimedia'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                          <meta name="csrf-token" content="{{ csrf_token() }}">
                          
                          <div class="form-group">
                              <label class="control-label " for="nama">
                                  Upload
                              </label>
                              <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                          </div>

                          <div class="form-group">
                              <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                          </div>

                      {!! Form::close() !!}
                    </div>

                  </div>


                </div>
                
                <div class="tab-pane" id="score">
                  
                  <div class="row">
                    
                    <div class="col-md-4">
                      <div class="callout callout-warning">
                          <h4>Mohon Diperhatikan!</h4>

                          <ul>
                              <li>Isi kode staff dengan kode yang sudah di simpan di sistem</li>
                          </ul>
                      </div>
                    </div>

                    <div class="col-md-4">
                        <a href="{{ route('admin.reportstaff.generateExcelTemplateScore') }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel Score People</a>

                        {!! Form::open(['url'   =>  route('admin.reportstrore.importExcelReportscore'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                
                                <div class="form-group">
                                    <label class="control-label " for="nama">
                                        Upload Score People
                                    </label>
                                    <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                                </div>

                                <div class="form-group">
                                    <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                                </div>

                            {!! Form::close() !!}

                    </div>

                    <div class="col-md-4">
                        <a href="{{ route('admin.reportstaff.generateExcelTemplateScore') }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel Score People</a>

                        {!! Form::open(['url'   =>  route('admin.reportstrore.importExcelReportscoreIndividu'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                
                                <div class="form-group">
                                    <label class="control-label " for="nama">
                                        Upload Scope Individu
                                    </label>
                                    <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                                </div>

                                <div class="form-group">
                                    <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                                </div>

                            {!! Form::close() !!}

                    </div>

                  </div>

                  <div class="row">
                    
                    <div class="col-md-6">
                      <table class="table table-responsive table-bordered">
                          <thead>
                            <tr>
                              <td>Kode</td>
                              <td>Categori</td>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($performances as $performance)
                              <tr>
                                <td>
                                  <span class="label label-primary">
                                    {{ $performance->kode }}
                                  </span>
                                </td>
                                <td>{{ $performance->kategori }}</td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                    </div>
                    <div class="col-md-6">
                      <div class="col-md-4">
                          <a href="{{ route('admin.reportstaff.generateExcelTemplateScorePerformance') }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel Score People Performance</a>

                          {!! Form::open(['url'   =>  route('admin.reportstrore.importExcelReportscorePerformance'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                                  <meta name="csrf-token" content="{{ csrf_token() }}">
                                  
                                  <div class="form-group">
                                      <label class="control-label " for="nama">
                                          Upload Score People
                                      </label>
                                      <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                                  </div>

                                  <div class="form-group">
                                      <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                                  </div>

                              {!! Form::close() !!}

                      </div>
                    </div>

                  </div>

                </div>


             </div>
             <!-- /.tab-content -->
          </div>
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script src="{{ asset('admin/js/loader.js') }}"></script>
    <script src="{{ asset('admin/js/fileinput.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#stores-table').DataTable();
            $('#stores2-table').DataTable();
            $('#posisi-table').DataTable();

            //  $(".picture").fileinput({
            //     previewFileType: "image",
            //     allowedFileExtensions: ["jpg", "png"],
            //     browseClass: "btn btn-success",
            //     browseLabel: "Pick Image",
            //     browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
            //     removeClass: "btn btn-danger",
            //     removeLabel: "Delete",
            //     removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
            //     showUpload: true,
            //     maxFileCount: 100
            // });


        } );
    </script>


@endsection
