@extends('admin.layouts.app')

@section('css')
  <link href="{!! asset('admin/css/select2.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('admin/css/select2.bootstrap.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">

        <div class="col-md-6">
          <div class="box box-primary">

             {!!  Form::model($store, array('method' => 'put', 'route' => array('reportstore.update', $store->code), 'files'=> true)) !!}

              <div class="box-body">

                  <div class="form-group ">
                      <label class="control-label requiredField" for="kanwil_id">
                          Kota
                      </label>
                      <input type="text" name="kota" value="{!! $store->kota->name or '-' !!}" class="form-control">
                  </div>

                  <div class="form-group">
                      {!! Form::label('name', 'Nama Store', array('class'=>'')) !!}
                      {!! Form::text('name', old('name'), array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('name', 'Score', array('class'=>'')) !!}
                      <input type="text" name="score" value="{!! $store->reportstore->score or '' !!}" class="form-control">
                  </div>

                   	

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div>

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
   <script src="{!!  asset('admin/js/select2.min.js')  !!}"></script>
   <script src="{!!  asset('admin/js/select2_locale_id.js')  !!}"></script>
    <script>
        $(document).ready(function () {

            $(".kota_code").select2({
                placeholder: 'Pilih Kota'
            });

        });
    </script>
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>

@endsection
