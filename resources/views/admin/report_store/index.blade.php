@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
       <div class="row">
       	
       	<div class="col-md-12">
            <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
                <li class="active"><a href="#datatable" data-toggle="tab"> <i class="fa fa-table" aria-hidden="true"></i> Datatable</a></li>
                <li><a href="#excel" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Via Excel</a></li>
             </ul>
             <div class="tab-content">
                <div class="tab-pane active" id="datatable">
                   @if(count($stores))
                    <table class="table table-bordered" id="stores-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th style="width: 35px;">Code</th>
                            <th>Kota</th>
                            <th>Nama</th>
                            <th>Score</th>
                            <th style="width: 80px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($stores as $store)
                                <tr>
                                    <td style="width: 35px;">{{ $store->code  or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->kota->name or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->name or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->reportstore ? $store->reportstore->score : 'belum diupload'  }}</td>
                                    <td style="width: 80px;">
                                        <a href='{{ route('reportstore.edit', [$store->code]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="excel">
                  
                  <div class="row">
                      <div class="col-md-6">
                          <div class="callout callout-warning">
                              <h4>Mohon Diperhatikan!</h4>

                              <ul>
                                  <li>Untuk pengisian kolom <strong>kode_store</strong> ada di tabel bawah sebelah kiri</li>
                              </ul>
                          </div>
                          
                      </div>

                      <div class="col-md-6">
                          <a href="{{ route('admin.reportstaff.generateExcelTemplate') }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel</a>
                          <br>

                          {!! Form::open(['url'   =>  route('admin.reportstrore.importExcel'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                              <meta name="csrf-token" content="{{ csrf_token() }}">

                              <div class="form-group">
                                  <label class="control-label " for="nama">
                                      Upload
                                  </label>
                                  <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="">
                              </div>

                              <div class="form-group">
                                  <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                              </div>

                          {!! Form::close() !!}
                      </div>
                  
                  <div class="row">
                    <div class="col-md-12">
                      {{-- Kode Store --}}
                       <div class="col-md-6">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Daftar Kode Store</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body no-padding">
                                  @if(count($stores))
                                    <table class="table table-bordered" id="stores2-table">
                                        <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <thead>
                                        <tr>
                                            <th style="width: 35px;">Kode Store</th>
                                            <th>Kota</th>
                                            <th>Nama Store</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($stores as $store)
                                                <tr>
                                                    <td style="width: 35px;"><span class="label label-success" style="font-size: 11pt;">{{ $store->code  or '-' }}</span></td>
                                                    <td style="width: 86px;">{{ $store->kota->name or '-' }}</td>
                                                    <td style="width: 86px;">{{ $store->name or '-' }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                        @include('admin.layouts.partials.alertWarning')
                                    @endif
                                </div>
                                <!-- /.box-body -->
                            </div>
                       </div>
                    </div>
                  </div>

                 </div>

                </div>
                <!-- /.tab-pane -->
             </div>
             <!-- /.tab-content -->
          </div>
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script src="{{ asset('admin/js/loader.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#stores-table').DataTable();
            $('#stores2-table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );
    </script>


@endsection