@extends('admin.layouts.app')

@section('css')
@endsection

@section('content')


      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
            {!! Form::open(array('route' => array('admin.reportnasionalrbh.edit'), 'files'=> false))  !!}
            
              <div class="box-body">
                  
                  <div class="form-group">
                      {!! Form::label('nasional', 'Nilai Nasional', array('class'=>'')) !!}

                      <?php 

                        $nasionalscore = App\Models\ReportNasionalRBH::where('report_id',$report->id)
                                                                       ->whereNotNull('is_nasional')
                                                                       ->first();

                      ?>

                      {!! Form::text('nasional', $nasionalscore ? $nasionalscore->score : '' , array('class'=>'form-control','required')) !!}
                  </div>


                  <div class="form-group">
                      {!! Form::label('rbh1', 'Nilai RBH 1', array('class'=>'')) !!}

                      <?php 

                        $rbh1score = App\Models\ReportNasionalRBH::where('report_id',$report->id)
                                                                       ->where('kota_code','rbh1')
                                                                       ->first();

                      ?>

                      {!! Form::text('rbh1', $rbh1score ? $rbh1score->score : '' , array('class'=>'form-control','required')) !!}
                  </div> 

                  <div class="form-group">
                      {!! Form::label('rbh2', 'Nilai RBH 2', array('class'=>'')) !!}

                      <?php 

                        $rbh2score = App\Models\ReportNasionalRBH::where('report_id',$report->id)
                                                                       ->where('kota_code','rbh2')
                                                                       ->first();

                      ?>

                      {!! Form::text('rbh2', $rbh2score ? $rbh2score->score : '' , array('class'=>'form-control','required')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('rbh3', 'Nilai RBH 3', array('class'=>'')) !!}

                      <?php 

                        $rbh3score = App\Models\ReportNasionalRBH::where('report_id',$report->id)
                                                                       ->where('kota_code','rbh3')
                                                                       ->first();

                      ?>

                      {!! Form::text('rbh3', $rbh3score ? $rbh2score->score : '' , array('class'=>'form-control')) !!}
                  </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div>  

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>
  
@endsection