@extends('admin.layouts.app')

@section('css')
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
          {!!  Form::model($reportnasionalget, array('method' => 'put', 'route' => array('admin.reportparameter.storeNasional'), 'files'=> false)) !!}
              
              <div class="box-body">

                  <h2>Form Report Parameter Nasional</h2>
                  
                  <div class="form-group">
                      {!! Form::label('cs', 'CS', array('class'=>'')) !!}
                      {!! Form::text('cs', $reportnasionalpeople ? $reportnasionalpeople[0]['cs'] : ''  , array('class'=>'form-control')) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::label('teller', 'Teller', array('class'=>'')) !!}
                      {!! Form::text('teller', $reportnasionalpeople ? $reportnasionalpeople[1]['teller'] : '', array('class'=>'form-control')) !!}
                  </div> 
                  <div class="form-group">
                      {!! Form::label('satpam', 'Satpam', array('class'=>'')) !!}
                      {!! Form::text('satpam', $reportnasionalpeople ? $reportnasionalpeople[2]['satpam'] : '', array('class'=>'form-control')) !!}
                  </div>

                  <hr>
                  
                  <div class="form-group">
                      {!! Form::label('gedung', 'Gedung', array('class'=>'')) !!}
                      {!! Form::text('gedung', $reportnasionaltangible ? $reportnasionaltangible[0]['gedung'] : '', array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('toilet', 'Toilet', array('class'=>'')) !!}
                      {!! Form::text('toilet', $reportnasionaltangible ? $reportnasionaltangible[1]['toilet'] : '', array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('atm', 'ATM', array('class'=>'')) !!}
                      {!! Form::text('atm', $reportnasionaltangible ? $reportnasionaltangible[2]['atm'] : '', array('class'=>'form-control')) !!}
                  </div>

                  <hr>
                  
                  <div class="form-group">
                      {!! Form::label('mc', 'MC', array('class'=>'')) !!}
                      {!! Form::text('mc', $reportnasionalget ? $reportnasionalget->report_mc : '', array('class'=>'form-control')) !!}
                  </div>



              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div> 	
        
        <div class="col-md-6">
          <table class="table table-responsive table-striped">
            <thead>
              <th>Nama</th>
              <th>Aksi</th>
            </thead>
            <tbody>
              @foreach($kotas as $kota)
                <tr>
                  <td>{{ $kota->name }}</td>
                  <td>
                    
                    <a href="{{ route('admin.reportparameter.getrbh',$kota->code) }}" class="btn btn-sm btn-primary">Input Nilai Parametr</a>

                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>  

      </div>

    </section><!-- /.content -->

@endsection

@section('js')

  
@endsection