@extends('admin.layouts.app')

@section('css')
  <link href="{!! asset('admin/css/select2.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('admin/css/select2.bootstrap.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
       <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
           {!!  Form::model($staff, array('method' => 'put', 'route' => array('admin.staffs.update', $staff->id), 'files'=> true)) !!}
            
              <div class="box-body">
                  
                  <div class="form-group">
                      {!! Form::label('name', 'Store', array('class'=>'')) !!}
                      {!! Form::text('name', $store->name, array('class'=>'form-control','readonly')) !!}
                  </div>

                  <div class="form-group ">
                      <label class="control-label requiredField" for="kanwil_id">
                          Posisi
                          <span class="asteriskField">
                        *
                       </span>
                      </label>
                      {!!  Form::select('position_id', $positions, old('position_id'), array('class'=>'form-control posisi_id'))  !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('kode_staff', 'Kode Staff', array('class'=>'')) !!}
                      {!! Form::text('kode_staff', old('kode_staff'), array('class'=>'form-control','required')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('name', 'Nama', array('class'=>'')) !!}
                      {!! Form::text('nama', old('nama'), array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      <label for="gender">Gender</label>
                      {{-- <input name="gender" type="radio" value="0" id="gender">Pria --}}
                      {!! Form::radio('gender', 1, $staff->gender == 1 ? true : false); !!} Pria
                      {!! Form::radio('gender', 2, $staff->gender == 2 ? true : false); !!} Wanita
                  </div>


                  <div class="form-group">
                      {!! Form::label('Ava', 'Ava', array('class'=>'')) !!}
                      {!! Form::file('ava', array('class'=>'form-control')) !!}
                  </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div> 	

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
   <script src="{!!  asset('admin/js/select2.min.js')  !!}"></script>
   <script src="{!!  asset('admin/js/select2_locale_id.js')  !!}"></script>
    <script>
        $(document).ready(function () {

            $(".posisi_id").select2({
                placeholder: 'Pilih Posisi'
            });

        });
    </script>
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>
  
@endsection