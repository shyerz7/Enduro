@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          {!! Breadcrumbs::render('breadcrumb') !!}
        </div>
      </div>

       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
       <div class="row">
       	
       	<div class="col-md-12">
            <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
                <li class="active"><a href="#datatable" data-toggle="tab"> <i class="fa fa-table" aria-hidden="true"></i> Datatable</a></li>
                {{-- <li><a href="#excel" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Via Excel</a></li> --}}
             </ul>
             <div class="tab-content">
                <div class="tab-pane active" id="datatable">
                   <h1>{{ $store->name }}</h1> <a href="{{ route('admin.staffs.create',$store->code) }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Staff</a>
                  <br><br>
                   @if(count($staffs))
                    <table class="table table-bordered" id="stores-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th  style="width: 100px;">ava</th>
                            <th  style="width: 100px;">Kode Staff</th>
                            <th>Nama</th>
                            <th>Posisi</th>
                            <th>Gender</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($staffs as $staff)
                                <tr>
                                    <td class="text-center">
                                      @if($staff->ava)
                                        <img src="{{ asset('attachments/ava_staff/table').DIRECTORY_SEPARATOR.$staff->ava }}" class="user-image img-circle" alt="{{ $staff->nama }}">
                                      @else
                                        <p class="text-yellow">Photo belum diupload</p>
                                      @endif
                                    </td>
                                    <td>{{ $staff->kode_staff  or '-' }}</td>
                                    <td style="width: 86px;">{{ $staff->nama or '-' }}</td>
                                    <td style="width: 86px;">{{ $staff->position->name or '-' }}</td>
                                    <td style="width: 86px;">{{ $staff->gender ==1 ? 'Pria' : 'Wanita'}}</td>
                                    <td style="width: 80px;">
                                        <a href='{{ route('admin.staffs.edit', [$staff->id]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" data-target="{{ route('admin.staffs.delete', [$staff->id]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                            <i class="fa fa-trash-o"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
                </div>
                
                <!-- /.tab-pane -->
                <div class="tab-pane" id="excel">
                  
                </div>
                <!-- /.tab-pane -->
             </div>
             <!-- /.tab-content -->
          </div>
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#stores-table').DataTable();
        } );
    </script>


@endsection