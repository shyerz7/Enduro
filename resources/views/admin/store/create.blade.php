@extends('admin.layouts.app')

@section('css')
  <link href="{!! asset('admin/css/select2.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('admin/css/select2.bootstrap.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Error --}}
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        </div>
      </div>

      <div class="row">
        
        <div class="col-md-6">
          <div class="box box-primary">
            
            {!! Form::open(array('route' => array('stores.store'), 'files'=> true))  !!}
            
              <div class="box-body">
                  
                  <div class="form-group ">
                      <label class="control-label requiredField" for="kanwil_id">
                          Kota
                          <span class="asteriskField">
                        *
                       </span>
                      </label>
                      {!!  Form::select('kota_code', $kotas, null, array('class'=>'form-control kota_code'))  !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('name', 'Nama', array('class'=>'')) !!}
                      {!! Form::text('name', old('name'), array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('latitude', 'Latitude', array('class'=>'')) !!}
                      {!! Form::text('latitude', old('latitude'), array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('longitude', 'Longitude', array('class'=>'')) !!}
                      {!! Form::text('longitude', old('longitude'), array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('Ava', 'Ava', array('class'=>'')) !!}
                      {!! Form::file('ava', array('class'=>'form-control')) !!}
                  </div>

                  <div class="form-group">
                      {!! Form::label('Address', 'Address', array('class'=>'')) !!}
                      {!! Form::text('address', old('address'), array('class'=>'form-control')) !!}
                  </div>

                  <hr>

                  <div class="form-group">
                      {!! Form::label('email', 'Email', array('class'=>'')) !!}
                      {!! Form::email('email',old('email'), array('class'=>'form-control','required')) !!}
                  </div> 

                  <div class="form-group">
                      {!! Form::label('password', 'Password', array('class'=>'')) !!}
                      {!! Form::text('password',old('email'), array('class'=>'form-control','required')) !!}
                  </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{!! route('reports.index') !!}" class="btn btn-default">Batal</a>
              </div>

            {!! Form::close() !!}

            </div>
          </div> 	

      </div>

    </section><!-- /.content -->

@endsection

@section('js')
   <script src="{!!  asset('admin/js/select2.min.js')  !!}"></script>
   <script src="{!!  asset('admin/js/select2_locale_id.js')  !!}"></script>
    <script>
        $(document).ready(function () {

            $(".kota_code").select2({
                placeholder: 'Pilih Kota'
            });

        });
    </script>
  <script>
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    })
  </script>
  
@endsection