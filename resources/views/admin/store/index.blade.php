@extends('admin.layouts.app')

@section('css')
  <link href="{{  asset('admin/css/datatables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/sweetalert2.css') }}" rel="stylesheet">
  <link href="{{  asset('admin/css/fileinput.min.css') }}" rel="stylesheet">


@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      
       <div class="row">
        <div class="col-md-12">

          {{-- Untuk Pesan Sukses --}}
          @if(Session::has('alert-success'))
              <div class="alert alert-success">
                    {{ Session::get('alert-success') }}
                </div>
          @endif

        </div>
      </div>
       <div class="row">
       	
       	<div class="col-md-12">
          <div class="nav-tabs-custom">
             <ul class="nav nav-tabs">
                <li class="active"><a href="#datatable" data-toggle="tab"> <i class="fa fa-table" aria-hidden="true"></i> Datatable</a></li>
                <li><a href="#excel" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Via Excel</a></li>
                <li><a href="#ava" data-toggle="tab"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Upload Ava KCP Autoupload</a></li>
             </ul>
             <div class="tab-content">
                <div class="tab-pane active" id="datatable">
                   <a href="{{ route('stores.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Tambah Store</a>
                  <br><br>
                   @if(count($stores))
                    <table class="table table-bordered" id="stores-table">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <thead>
                        <tr>
                            <th style="width: 35px;">Code</th>
                            <th>Kota</th>
                            <th>Nama</th>
                            <th>Type</th>
                            <th>Email</th>
                            <th style="width: 80px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($stores as $store)
                                <tr>
                                    <td style="width: 35px;">{{ $store->code  or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->kota->name or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->type or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->name or '-' }}</td>
                                    <td style="width: 86px;">{{ $store->user->email or '-' }}</td>
                                    <td style="width: 80px;">
                                        <a href='{{ route('stores.edit', [$store->code]) }}' class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" data-target="{{ route('stores.destroy', [$store->code]) }}" class="btn btn-danger btn-xs confirmation" data-toggle="tooltip" data-original-title="Hapus" data-container="body" onclick="hapusData(this)">
                                            <i class="fa fa-trash-o"></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        @include('admin.layouts.partials.alertWarning')
                    @endif
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="excel">
                  <p class="lead">Fitur Autoupload Data Kota</p>
                  <a href="{{ route('admin.stores.generateExcelTemplate') }}" class="btn btn-success"><span class="glyphicon glyphicon-cloud-download"></span> Download Template Excel</a>
                  {!! Form::open(['url'   =>  route('admin.stores.importExcel'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                      <meta name="csrf-token" content="{{ csrf_token() }}">

                      <div class="form-group">
                          <label class="control-label " for="nama">
                              Upload
                          </label>
                          <input class="form-control filestyle" data-icon="false" id="excel" name="excel" type="file" required="required">
                      </div>

                      <div class="form-group">
                          <input name="save" type="submit" value="Save" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                      </div>

                  {!! Form::close() !!}
                </div>

                <div class="tab-pane" id="ava">
                  
                    <div class="row">
                    
                      <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissible">
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                          Untuk menggunakan fitur ini adalah data staff sudah masuk terlebih dahulu <br>
                          kemudian nama photo nya <strong>SAMAKAN DENGAN KODE STAFF YANG SUDAH DIUPLOAD</strong>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        {!! Form::open(['url'   =>  route('admin.stores.importAva'),'method' => 'post', 'files'=>'true', 'class'=>'']) !!}
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            
                            <div class="form-group">
                                <input name="save" type="submit" value="Simpan Ava" class="btn btn-primary" onclick="waitingDialog.show();setTimeout(function () {waitingDialog.hide();}, 3000);" >
                            </div>

                            <div class="form-group">
                              <label class="control-label">Select File</label>
                              <input id="ava" name="ava[]" type="file" class="file" multiple data-show-upload="false" data-show-caption="true">
                            </div>

                        {!! Form::close() !!}
                      </div>
                    </div>

                </div>

                <!-- /.tab-pane -->
             </div>
             <!-- /.tab-content -->
          </div>
       	</div>

       </div>

    </section><!-- /.content -->

@endsection

@section('js')
  <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>
    <!-- jquery confirm -->
    <script src="{{ asset('admin/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('admin/js/confirm.js') }}"></script>
    <script src="{{ asset('admin/js/loader.js') }}"></script>
    <script src="{{ asset('admin/js/fileinput.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#stores-table').DataTable();
        } );
    </script>


@endsection

@section('js')
<script src="{{ asset('admin/js/fileinput.min.js') }}"></script>

@endsection