<!DOCTYPE html>
<html>

<!-- Mirrored from webapplayers.com/inspinia_admin-v2.7/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 16 Mar 2017 05:19:39 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>RAD Web Report</title>

    <link href="{!! asset('frontend/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('frontend/font-awesome/css/font-awesome.css') !!}" rel="stylesheet">

    <link href="{!! asset('frontend/css/animate.css') !!}" rel="stylesheet">
    <link href="{!! asset('frontend/css/style.css') !!}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">
          <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="ibox-content text-center">
                  <br /> <br />
                  <a class="navbar-minimalize minimalize-styl-1 " href="#"><img src="{{ asset('frontend/img/logo_header.png') }}" alt=""> </a>
                  <br />
                  <h3 class="font-bold">Welcome to Plasa Indonesia Web Report</h3>
                  <br />
                    <form class="m-t" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          <label class="pull-left">E-Mail</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <label class="pull-left">Password</label>
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-lg btn-success block full-width m-b">Login</button>

                    </form>

                    <p class="m-t">
                        <small>RAD Research Webreport V.3.1 &copy; 2017</small>
                    </p>
                </div>
            </div>
          <div class="col-md-3"></div>
        </div>

    </div>

</body>

</html>
