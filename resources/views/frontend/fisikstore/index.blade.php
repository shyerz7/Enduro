@extends('frontend.layouts.app')

@section('css')
    <link href="http://vjs.zencdn.net/5.10.7/video-js.css" rel="stylesheet">
    <!-- If you'd like to support IE8 -->
    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

    <style>
    	.bg-red {
		    background-color: #f56954 !important;
		}
		.bg-orange {
		    background-color: #ff851b !important;
		}
		.bg-yellow {
		    background-color: #f39c12 !important;
		}
		.bg-green {
		    background-color: #00a65a !important;
		}
		.bg-blue {
		    background-color: #0073b7 !important;
		}
		/*Box Berwarna*/
		.small-box {
		    position: relative;
		    display: block;
		    -webkit-border-radius: 2px;
		    -moz-border-radius: 2px;
		    border-radius: 2px;
		    margin-bottom: 15px
		}

		.small-box > .inner {
		    padding: 10px
		}

		.small-box > .small-box-footer {
		    position: relative;
		    text-align: center;
		    padding: 3px 0;
		    color: #fff;
		    color: rgba(255, 255, 255, .8);
		    display: block;
		    z-index: 10;
		    background: rgba(0, 0, 0, .1);
		    text-decoration: none
		}

		.small-box > .small-box-footer:hover {
		    color: #fff;
		    background: rgba(0, 0, 0, .15)
		}

		.small-box h3 {
		    font-size: 38px;
		    font-weight: 700;
		    margin: 0 0 10px;
		    white-space: nowrap;
		    padding: 0
		}

		.small-box p {
		    font-size: 15px
		}

		.small-box p > small {
		    display: block;
		    color: #f9f9f9;
		    font-size: 13px;
		    margin-top: 5px
		}

		.small-box h3, .small-box p {
		    z-index: 5px
		}

		.small-box .icon {
		    position: absolute;
		    top: auto;
		    bottom: 25px;
		    right: 10px;
		    z-index: 0;
		    font-size: 60px;
		    color: rgba(0, 0, 0, .15)
		}

		.small-box:hover {
		    text-decoration: none;
		    color: #f9f9f9
		}
		@media screen and (max-width: 480px) {
		    .small-box {
		        text-align: center
		    }

		    .small-box .icon {
		        display: none
		    }

		    .small-box p {
		        font-size: 12px
		    }

		}

		.small-box:hover .icon {
		    animation-name: tansformAnimation;
		    animation-duration: .5s;
		    animation-iteration-count: 1;
		    animation-timing-function: ease;
		    animation-fill-mode: forwards;
		    -webkit-animation-name: tansformAnimation;
		    -webkit-animation-duration: .5s;
		    -webkit-animation-iteration-count: 1;
		    -webkit-animation-timing-function: ease;
		    -webkit-animation-fill-mode: forwards;
		    -moz-animation-name: tansformAnimation;
		    -moz-animation-duration: .5s;
		    -moz-animation-iteration-count: 1;
		    -moz-animation-timing-function: ease;
		    -moz-animation-fill-mode: forwards
		}
    </style>

@endsection

@section('content')

	<div class="row">

		@include('frontend.fisikstore.partials.left')

	<div class="col-md-9">
		<div class="ibox">
			<div class="ibox-content">
				<div class="tabs-container">
				   <ul class="nav nav-tabs">
				      <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="false"> Chart Performance</a></li>
				      <li class="" style="padding-left: 150px;">
				      	<table class="table table-score table-responsive" >
							                    <tbody>
							                        <tr class="text-center">
							                        <td class="bg-red" style="width: 20%">Sangat Kurang</td>
							                        <td class="bg-orange" style="width: 20%">Kurang</td>
							                        <td class="bg-yellow" style="width: 20%">Cukup</td>
							                        <td class="bg-green" style="width: 20%">Baik</td>
							                        <td class="bg-blue" style="width: 20%">Sangat Baik</td>
							                        </tr>
							                        <tr>
							                            <td>
							                                <span class="pull-left">0</span>
							                                <span class="pull-right">54.9</span>
							                            </td>
							                            <td>
							                                <span class="pull-left">55</span>
							                                <span class="pull-right">64.9</span>
							                            </td>
							                            <td>
							                                <span class="pull-left">65</span>
							                                <span class="pull-right">74.9</span>
							                            </td>
							                            <td>
							                                <span class="pull-left">75</span>
							                                <span class="pull-right">84.9</span>
							                            </td>
							                            <td>
							                                <span class="pull-left">80</span>
							                                <span class="pull-right">100</span>
							                            </td>
							                        </tr>
							                   </tbody>
							            	</table>
				      </li>
				   </ul>
				   <div class="tab-content">

				      <div id="tab-1" class="tab-pane active">
				         <div class="panel-body">

				            <legend>Laporan</legend>

				      		<div class="row">

				      			<div class="col-md-4">
				      				<div id="chart-performance" style="width:420px;height:350px;margin:0 auto"></div>
				      			</div>
				      			<div class="col-md-8">

				      				<div class="row">

				      					<div class="col-md-12">

				      						 <table class="table borderless">

												<thead>
								                    <td width="45%"></td>
								                    <td width="40%"></td>
								                    {{-- <td width="15%"></td> --}}
								                </thead>
												<tbody>
													@foreach($report_performances as $report)
														<tr>
															<td>
																<strong><h4 style="padding-top:10px;" class="pull-right">{{ $report->category->category }}</h4></strong>
															</td>
															<td>
																<?php
																	if(is_null($report->score)){
								                                        $score = 0;
								                                    }else{
								                                        $score = (float) $report->score;
								                                    }

								                                    if($score >= 85.00)
								                                    {
								                                        $warna = 'bg-blue';
								                                        $teks  = 'Sangat Baik';
								                                        $icon  = 'glyphicon glyphicon-thumbs-up';
								                                    }
								                                    elseif($score >= 75.00 && $score <= 84.99)
								                                    {
								                                        $warna = 'bg-green';
								                                        $teks  = 'Baik';
								                                        $icon  = 'glyphicon glyphicon-plus';
								                                    }
								                                    elseif($score >= 65.00 && $score <= 74.99)
								                                    {
								                                        $warna = 'bg-yellow';
								                                        $teks  = 'Cukup';
								                                        $icon  = 'glyphicon glyphicon-ok';
								                                    }
								                                    elseif($score >= 55.00 && $score <= 64.99)
								                                    {
								                                        $warna = 'bg-orange';
								                                        $teks  = 'Kurang';
								                                        $icon  = 'glyphicon glyphicon-ok';
								                                    }
								                                    elseif($score <= 54.99)
								                                    {
								                                        $warna = 'bg-red';
								                                        $teks  = 'Sangat Kurang';
								                                        $icon  = 'glyphicon glyphicon-thumbs-down';
								                                    }elseif(is_null($score))
								                                    {
								                                        $warna = 'bg-red';
								                                        $teks  = '-';
								                                        $icon  = 'glyphicon glyphicon-thumbs-down';
								                                    }
																?>
																<div class="small-box {{ $warna }} mrg-top-20 text-left" style="width:100%;" >
								                                      <div class="inner">
								                                        <h3 style="color:white;">
								                                            @if(!is_null($score))
								                                              {{ number_format($score, 2) }}
								                                            @else
								                                              -
								                                            @endif
								                                          </h3>
								                                          <p>
								                                              {{ $teks }}
								                                          </p>
								                                    </div>
								                                 </div>
															</td>
														</tr>
													@endforeach
												</tbody>


				      						 </table>

				      					</div>

				      				</div>

				      			</div>

				      		</div>


				         </div>
				      </div>

				   </div>
				</div>
			</div>
		</div>
	</div>

	</div>

@endsection

@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

	<script>
		$('#chart-performance').highcharts({
            chart: {
                    type: 'column'
                },
                colors: ['#3498DB'],
                title: {
                    text: 'Staff Performance'
                },
                xAxis: {
                    type: 'category',
                     title: {
                        text: ' '
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                        borderWidth: 0,
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                yAxis: {
                     // tickPositioner: function() {
                     //        return [0,10];
                     //    },
                     gridLineWidth: 0,

                    title: {
                        text: 'Persentase'
                    }

                },
                credits:{
                    enabled:false
                },

                series: [{
                    name: 'Score',
                    stacking: 'normal',
                    colorByPoint: true,
                    data: {!! json_encode($report_performance_data,JSON_NUMERIC_CHECK) !!}
                }],
        });

	</script>
@endsection
