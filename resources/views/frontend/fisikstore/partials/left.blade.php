{{-- Profile Page --}}
<div class="col-md-3">
   <div class="wrapper wrapper-content project-manager">
      <h4>Profile Fisik Store</h4>
		<img src="http://placehold.it/300x200" class="img-responsive img-thumbnail">
	     <br>
      <h4><strong>{{ $fisikstore->fisik->name or '-' }}</strong></h4>
      <h5>Project files</h5>
      <ul class="list-unstyled project-files">
         <li><a href="{{ route('fisikstore.download.pdf',$fisikstore->id) }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download Report PDF</a></li>
      </ul>
   </div>
</div><!-- end colmd-3 -->