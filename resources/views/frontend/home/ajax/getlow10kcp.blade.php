@if(count($kcp_low_perfomances))
	<ul class="list-group">
		<?php
			$id_kcp=1;
		?>
		@foreach($kcp_low_perfomances as $data_kcp_low)
			<li class="list-group-item">
		    <span class="badge">{{ number_format((float) $data_kcp_low->average,1) }}</span>
		    {{ $id_kcp.'. '.$data_kcp_low->namaStore }}
		  </li>
		  <?php $id_kcp++ ?>
		@endforeach

	</ul>
@else
	@include('frontend.home.partials.datakosong')
@endif