@if(count($kcp_perfomances))
	<ul class="list-group">
		<?php
			$id_kcp=1;
		?>
		@foreach($kcp_perfomances as $data_kcp_top)
			<li class="list-group-item">
		    <span class="badge">{{ number_format((float) $data_kcp_top->average,1) }}</span>
		    {{ $id_kcp.'. '.$data_kcp_top->namaStore }}
		  </li>
		  <?php $id_kcp++ ?>
		@endforeach

	</ul>
@else
	@include('frontend.home.partials.datakosong')
@endif