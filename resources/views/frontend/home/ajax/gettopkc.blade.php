@if(count($kc_perfomances))
	<ul class="list-group">
		<?php
			$id_kc=1;
		?>
		@foreach($kc_perfomances as $kc_perfomance)
			<li class="list-group-item">
		    <span class="badge">{{ number_format((float) $kc_perfomance->average,1) }}</span>
		    {{ $id_kc.'. '.$kc_perfomance->namaKota }}
		  </li>
		  <?php $id_kc++ ?>
		@endforeach

	</ul>
@else
	@include('frontend.home.partials.datakosong')
@endif