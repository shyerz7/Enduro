@if(count($top_staffs))
	<ul class="list-group">
		<?php
			$id_kcp=1;
		?>
		@foreach($top_staffs as $top_staff)
			<li class="list-group-item">
		    <span class="badge">{{ number_format((float) $top_staff->average,1) }}</span>
		    {{ $id_kcp.'. '.$top_staff->namaStaff }} <br>
		    <span class="label label-primary"><i>{{ $top_staff->namaStore }}</i></span>
		  </li>
		  <?php $id_kcp++ ?>
		@endforeach

	</ul>
@else
	@include('frontend.home.partials.datakosong')
@endif