@extends('frontend.layouts.app')

@section('css')

 <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet">

<style>
  .bg-red {
        background-color: #f56954 !important;
    }
    .bg-orange {
        background-color: #ff851b !important;
    }
    .bg-yellow {
        background-color: #f39c12 !important;
    }
    .bg-green {
        background-color: #00a65a !important;
    }
    .bg-blue {
        background-color: #0073b7 !important;
    }
    /*Box Berwarna*/
    .small-box {
        position: relative;
        display: block;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        margin-bottom: 15px
    }


    #cssload-loader {
      position: absolute;
      margin: auto;
      left: 0;
      right: 0;
      width: 44px;
    }
    #cssload-loader ul {
      margin: 0;
      list-style: none;
      width: 44px;
      height: 31px;
      position: relative;
      padding: 0;
      height: 5px;
    }
    #cssload-loader ul li {
      position: absolute;
      width: 2px;
      height: 0;
      background-color: rgb(0,0,0);
      bottom: 0;
    }


    #cssload-loader li:nth-child(1) {
      left: 0;
      animation: cssload-sequence1 1.15s ease infinite 0;
        -o-animation: cssload-sequence1 1.15s ease infinite 0;
        -ms-animation: cssload-sequence1 1.15s ease infinite 0;
        -webkit-animation: cssload-sequence1 1.15s ease infinite 0;
        -moz-animation: cssload-sequence1 1.15s ease infinite 0;
    }
    #cssload-loader li:nth-child(2) {
      left: 15px;
      animation: cssload-sequence2 1.15s ease infinite 0.12s;
        -o-animation: cssload-sequence2 1.15s ease infinite 0.12s;
        -ms-animation: cssload-sequence2 1.15s ease infinite 0.12s;
        -webkit-animation: cssload-sequence2 1.15s ease infinite 0.12s;
        -moz-animation: cssload-sequence2 1.15s ease infinite 0.12s;
    }
    #cssload-loader li:nth-child(3) {
      left: 29px;
      animation: cssload-sequence1 1.15s ease-in-out infinite 0.23s;
        -o-animation: cssload-sequence1 1.15s ease-in-out infinite 0.23s;
        -ms-animation: cssload-sequence1 1.15s ease-in-out infinite 0.23s;
        -webkit-animation: cssload-sequence1 1.15s ease-in-out infinite 0.23s;
        -moz-animation: cssload-sequence1 1.15s ease-in-out infinite 0.23s;
    }
    #cssload-loader li:nth-child(4) {
      left: 44px;
      animation: cssload-sequence2 1.15s ease-in infinite 0.35s;
        -o-animation: cssload-sequence2 1.15s ease-in infinite 0.35s;
        -ms-animation: cssload-sequence2 1.15s ease-in infinite 0.35s;
        -webkit-animation: cssload-sequence2 1.15s ease-in infinite 0.35s;
        -moz-animation: cssload-sequence2 1.15s ease-in infinite 0.35s;
    }
    #cssload-loader li:nth-child(5) {
      left: 58px;
      animation: cssload-sequence1 1.15s ease-in-out infinite 0.46s;
        -o-animation: cssload-sequence1 1.15s ease-in-out infinite 0.46s;
        -ms-animation: cssload-sequence1 1.15s ease-in-out infinite 0.46s;
        -webkit-animation: cssload-sequence1 1.15s ease-in-out infinite 0.46s;
        -moz-animation: cssload-sequence1 1.15s ease-in-out infinite 0.46s;
    }
    #cssload-loader li:nth-child(6) {
      left: 73px;
      animation: cssload-sequence2 1.15s ease infinite 0.58s;
        -o-animation: cssload-sequence2 1.15s ease infinite 0.58s;
        -ms-animation: cssload-sequence2 1.15s ease infinite 0.58s;
        -webkit-animation: cssload-sequence2 1.15s ease infinite 0.58s;
        -moz-animation: cssload-sequence2 1.15s ease infinite 0.58s;
    }


    @keyframes cssload-sequence1 {
      0% {
        height: 10px;
      }
      50% {
        height: 49px;
      }
      100% {
        height: 10px;
      }
    }

    @-o-keyframes cssload-sequence1 {
      0% {
        height: 10px;
      }
      50% {
        height: 49px;
      }
      100% {
        height: 10px;
      }
    }

    @-ms-keyframes cssload-sequence1 {
      0% {
        height: 10px;
      }
      50% {
        height: 49px;
      }
      100% {
        height: 10px;
      }
    }

    @-webkit-keyframes cssload-sequence1 {
      0% {
        height: 10px;
      }
      50% {
        height: 49px;
      }
      100% {
        height: 10px;
      }
    }

    @-moz-keyframes cssload-sequence1 {
      0% {
        height: 10px;
      }
      50% {
        height: 49px;
      }
      100% {
        height: 10px;
      }
    }

    @keyframes cssload-sequence2 {
      0% {
        height: 19px;
      }
      50% {
        height: 63px;
      }
      100% {
        height: 19px;
      }
    }

    @-o-keyframes cssload-sequence2 {
      0% {
        height: 19px;
      }
      50% {
        height: 63px;
      }
      100% {
        height: 19px;
      }
    }

    @-ms-keyframes cssload-sequence2 {
      0% {
        height: 19px;
      }
      50% {
        height: 63px;
      }
      100% {
        height: 19px;
      }
    }

    @-webkit-keyframes cssload-sequence2 {
      0% {
        height: 19px;
      }
      50% {
        height: 63px;
      }
      100% {
        height: 19px;
      }
    }

    @-moz-keyframes cssload-sequence2 {
      0% {
        height: 19px;
      }
      50% {
        height: 63px;
      }
      100% {
        height: 19px;
      }
    }

</style>

@endsection

@section('content')

<?php
  $reports = App\Models\Report::orderBy('id','asc')->get();
?>
            <div class="row">
            @foreach($reports as $report)
              <a href="{{ url('kunjungan', $report->id) }}">
              <div class="col-md-4">
                <div class="panel panel-success">
                  <!-- <div class="panel-heading" style="background-color: #00baff; border: none;">
                    <h3 class="panel-title"></h3>
                  </div> -->
                  <div class="panel-body">

                      <div class="row">
                        <div class="col-md-5">
                            <div class="image-cropper text-center" style="background-color: #00baff">
                              <h1 style="font-size: 50px; color: #ffffff;">{{ $report->id or '' }}</h1>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <h4>{{ $report->name or '' }}</h4>
                            <p style="font-size: 8pt;">{{ date('l, d F Y', strtotime($report->periode_start)) }} - {{ date('d F Y', strtotime($report->periode_end)) }}</p>
                            <p><span class="label label-warning">Lihat Detail >></span></p>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              </a>
            @endforeach
            </div>


@endsection

@section('js')
<script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>

      <script>

        $('#rbh1-perfomance').highcharts({
            chart: {
                    type: 'bar',
                    height: '150%'
                },
                colors: ['#3498DB'],
                title: {
                    text: 'RBH I Performance'
                },
                xAxis: {
                    type: 'category',
                     title: {
                        text: ' '
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                        borderWidth: 0,
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                yAxis: {
                     // tickPositioner: function() {
                     //        return [0,10];
                     //    },
                     gridLineWidth: 0,

                    title: {
                        text: 'Persentase'
                    }

                },
                credits:{
                    enabled:false
                },

                series: [{
                    name: 'Score',
                    stacking: 'normal',
                    colorByPoint: true,
                    data: {!! json_encode($rbh1_performance_data,JSON_NUMERIC_CHECK) !!}
                }],
        });

        $('#rbh2-performance').highcharts({
            chart: {
                    type: 'bar',
                    height: '150%'
                },
                colors: ['#3498DB'],
                title: {
                    text: 'RBH II Perfomance'
                },
                xAxis: {
                    type: 'category',
                     title: {
                        text: ' '
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                        borderWidth: 0,
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                yAxis: {
                     // tickPositioner: function() {
                     //        return [0,10];
                     //    },
                     gridLineWidth: 0,

                    title: {
                        text: 'Persentase'
                    }

                },
                credits:{
                    enabled:false
                },

                series: [{
                    name: 'Score',
                    stacking: 'normal',
                    colorByPoint: true,
                    data: {!! json_encode($rbh2_performance_data,JSON_NUMERIC_CHECK) !!}
                }],
        });

        $('#rbh3-performance').highcharts({
            chart: {
                    type: 'bar',
                    height: '150%'
                },
                colors: ['#3498DB'],
                title: {
                    text: 'RBH III Perfomance'
                },
                xAxis: {
                    type: 'category',
                     title: {
                        text: ' '
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                      stacking: 'normal',
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                yAxis: {
                     // tickPositioner: function() {
                     //        return [0,10];
                     //    },
                     gridLineWidth: 0,

                    title: {
                        text: 'Persentase'
                    }

                },
                credits:{
                    enabled:false
                },

                series: [{
                    name: 'Score',
                    stacking: 'normal',
                    colorByPoint: true,
                    data: {!! json_encode($rbh3_performance_data,JSON_NUMERIC_CHECK) !!}
                }],
        });

      </script>

      <script>

          $(".data-top-kc").click(function(e){

              e.preventDefault();

              $.ajax({
                url  : '{{ route('ajax.gettopkc')  }}',
                type : 'get',
                success:function(html)
                {
                    $('.data-top-kc-data').html(html);

                }

              });

          });

          $(".data-top-kcp").click(function(e){

              e.preventDefault();

              $.ajax({
                url  : '{{ route('ajax.gettop10kcp')  }}',
                type : 'get',
                success:function(html)
                {
                    $('.data-top-kcp-data').html(html);

                }

              });

          });

          $(".data-low-kcp").click(function(e){

              e.preventDefault();

              $.ajax({
                url  : '{{ route('ajax.getlow10kcp')  }}',
                type : 'get',
                success:function(html)
                {
                    $('.data-low-kcp-data').html(html);

                }

              });

          });

          $(".data-top-cs").click(function(e){

              e.preventDefault();

              $.ajax({
                url  : '{{ route('ajax.gettop10cs')  }}',
                type : 'get',
                success:function(html)
                {
                    $('.data-top-cs-data').html(html);

                }

              });

          });

          $(".data-top-teller").click(function(e){

              e.preventDefault();

              $.ajax({
                url  : '{{ route('ajax.gettop10Teller')  }}',
                type : 'get',
                success:function(html)
                {
                    $('.data-top-teller-data').html(html);

                }

              });

          });


          $(".data-top-satpam").click(function(e){

              e.preventDefault();

              $.ajax({
                url  : '{{ route('ajax.gettop10Satpam')  }}',
                type : 'get',
                success:function(html)
                {
                    $('.data-top-satpam-data').html(html);

                }

              });

          });

          $(".data-top-mc").click(function(e){

              e.preventDefault();

              $.ajax({
                url  : '{{ route('ajax.gettop10mc')  }}',
                type : 'get',
                success:function(html)
                {
                    $('.data-top-mc-data').html(html);

                }

              });

          });



      </script>

      <!-- DataTables -->
    <script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/datatables.bootstrap.js') }}"></script>


    <script>
        $(document).ready(function() {
            $('#stores-table').DataTable();
            $('#staffs-table').DataTable();
        } );
    </script>

@endsection
