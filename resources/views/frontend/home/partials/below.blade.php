 <div class="row">
    <div class="col-md-12">
       <div class="tabs-container">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#roottab-1"> KCP / KK Datatables</a></li>
              {{-- <li class=""><a data-toggle="tab" href="#roottab-2">10 Dealer Teratas Per Posisi</a></li> --}}
          </ul>
          <div class="tab-content">
              <div id="roottab-1" class="tab-pane active">
                  <div class="panel-body">
                        @if(count($stores))
                          <table class="table table-bordered" id="stores-table">
                              <meta name="csrf-token" content="{{ csrf_token() }}">
                              <thead>
                              <tr>
                                  <th style="width: 30px;align:center;">No</th>
                                  <th style="width: 200px;">RBH</th>
                                  <th>Cabang</th>
                                  <th>type</th>
                                  <th>score</th>
                                  <th style="width: 30px;align:center;">Action</th>
                              </tr>
                              </thead>
                              <tbody>
                                  <?php
                                      $no=1;
                                  ?>
                                  @foreach($stores as $store)
                                      <tr>
                                          <td>{{ $no }}</td>
                                          <td>{{ $store->nameKC or '-' }}</td>
                                          <td>{{ $store->nameKCP or '-' }}</td>
                                          <td>
                                            @if($store->type == 'KCP')
                                              <span class="label label-primary">KCP</span>
                                            @else
                                              <span class="label label-success">KK</span>
                                            @endif  
                                          </td>
                                          <td>{{ $store->score or '-' }}</td>
                                          <td>
                                              <a href="{{ url('store',$store->codeKCP) }}" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                              <i class="fa fa-eye" aria-hidden="true"></i>
                                              </a>
                                          </td>
                                      </tr>
                                      <?php $no++; ?>
                                  @endforeach
                              </tbody>
                          </table>
                          @else
                              @include('admin.layouts.partials.alertWarning')
                          @endif
                  </div>
              </div>
              <div id="roottab-2" class="tab-pane">
                  <div class="panel-body">
                      @if(count($staffs))
                          <table class="table table-bordered" id="staffs-table">
                              <meta name="csrf-token" content="{{ csrf_token() }}">
                              <thead>
                              <tr>
                                  <th style="width: 75px;">Code Cabang</th>
                                  <th>Cabang</th>
                                  <th>type</th>
                                  <th>Posisi</th>
                                  <th>Nama</th>
                                  <th>score</th>
                                  <th style="width: 30px;align:center;">Action</th>
                              </tr>
                              </thead>
                              <tbody>
                                  @foreach($staffs as $staff)
                                      <tr>
                                          <td>{{ $staff->codeKCP or '-' }}</td>
                                          <td>{{ $staff->nameKCP or '-' }}</td>
                                          <td>
                                            @if($staff->type == 'KCP')
                                              <span class="label label-primary">KCP</span>
                                            @else
                                              <span class="label label-success">KK</span>
                                            @endif  
                                          </td>
                                          <td>{{ $staff->posisi }}</td>
                                          <td>{{ $staff->posisi }}</td>
                                          <td>{{ $staff->score or '-' }}</td>
                                          <td>
                                              <a href="{{ url('staff',$staff->id) }}" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Edit" data-container="body">
                                              <i class="fa fa-eye" aria-hidden="true"></i>
                                              </a>
                                          </td>
                                      </tr>
                                  @endforeach
                              </tbody>
                          </table>
                          @else
                              @include('admin.layouts.partials.alertWarning')
                          @endif
                  </div>
              </div>
          </div>


      </div>
    </div>
 </div>