<!-- Trio Collapse -->
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="fa fa-building">
                            </span> Tangible <span class="label label-default">{{ number_format((float)$nilai_total_tangible, 1) }}</span></a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="#">Gedung <span class="label label-default">{{ number_format((float)$reportnasionaltangible[0]['gedung'], 1) }}</span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Toilet <span class="label label-default">{{number_format((float)$reportnasionaltangible[1]['toilet'], 1)}}</span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">ATM <span class="label label-default">{{number_format((float)$reportnasionaltangible[2]['atm'], 1)}}</span></a>
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="fa fa-users">
                            </span> People <span class="label label-default">{{ number_format((float)$nilai_total_people, 1) }}</span></a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="#">Customer Service <span class="label label-default">{{ number_format((float)$reportnasionalpeople[0]['cs'], 1) }}</span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Teller <span class="label label-default">{{ number_format((float)$reportnasionalpeople[1]['teller'], 1) }}</span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#">Satpam <span class="label label-default">{{ number_format((float)$reportnasionalpeople[2]['satpam'], 1) }}</span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="fa fa-phone">
                            </span> Caller <span class="label label-default"><?php $reportFormatted = number_format((float)$reportnasionalget['report_mc'], 1) ?>{{ $reportFormatted or '-' }}</span></a>
                        </h4>
                    </div>
                    <!--<div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="#"> </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>-->
                </div>
            </div>
<!-- Trio Collapse -->
