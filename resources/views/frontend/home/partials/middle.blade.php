{{-- Middle COntent                --}}
<div class="row">

  <div class="col-md-6">
     <div class="tabs-container">
        <ul class="nav nav-tabs">
           <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> RBH I Performance</a></li>
           <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">RBH II Performance</a></li>
           <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">RBH III Performance</a></li>
        </ul>
        <div class="tab-content">
           <div id="tab-1" class="tab-pane active">
              <div class="panel-body">
                 <div id="rbh1-perfomance" style="width:auto; min-height: 250px; margin: 0 auto"></div>
              </div>
           </div>
           <div id="tab-2" class="tab-pane">
              <div class="panel-body">
                 <div id="rbh2-performance" style="width: auto; min-height: 250px; margin: 0 auto"></div>
              </div>
           </div>
           <div id="tab-3" class="tab-pane">
              <div class="panel-body">
                 <div id="rbh3-performance" style="width: auto; min-height: 250px; margin: 0 auto"></div>
              </div>
           </div>
        </div>
     </div>
  </div>

  <div class="col-md-6">
     <div class="ibox ">
        <div class="ibox-content">
           <ul class="list-group link-index">

              {{-- <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-highest-kc" class="data-top-kc">
                    <i class="glyphicon glyphicon-plus"></i>
                    Ranking KC
                    </a>
                    <div id="collapse-highest-kc" class="collapse mrg-top-10 data-top-kc-data" style="padding-top: 5px;">
                        <div style="width: 50px;height: 50px;">
                          @include('frontend.home.partials.loader')
                        </div>
                    </div>
                 </div>
              </li> --}}

              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-highest" class="data-top-kcp">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Tertinggi KCP / KK
                    </a>
                    <div id="collapse-highest" class="collapse mrg-top-10 data-top-kcp-data" style="padding-top: 5px;">
                        <div style="width: 50px;height: 50px;">
                          @include('frontend.home.partials.loader')
                        </div>
                    </div>
                 </div>
              </li>

              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-lowest" class="data-low-kcp">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Terendah KCP / KK
                    </a>
                    <div id="collapse-lowest" class="collapse mrg-top-10 data-low-kcp-data">
                       @include('frontend.home.partials.loader')
                    </div>
                 </div>
              </li>

              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-cs-non-kk" class="data-top-cs">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Tertinggi Staff CS
                    </a>
                    <div id="collapse-cs-non-kk" class="collapse mrg-top-10 data-top-cs-data">
                       @include('frontend.home.partials.loader')
                    </div>
                 </div>
              </li>

              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-teller-non-kk" class="data-top-teller">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Tertinggi Staff Teller
                    </a>
                    <div id="collapse-teller-non-kk" class="collapse mrg-top-10 data-top-teller-data">
                       @include('frontend.home.partials.loader')
                    </div>
                 </div>
              </li>

              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-top-satpam" class="data-top-satpam">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Tertinggi Staff Satpam
                    </a>
                    <div id="collapse-top-satpam" class="collapse mrg-top-10 data-top-satpam-data">
                       @include('frontend.home.partials.loader')
                    </div>
                 </div>
              </li>

              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-top-mc" class="data-top-mc">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Tertinggi Staff MC
                    </a>
                    <div id="collapse-top-mc" class="collapse mrg-top-10 data-top-mc-data">
                       @include('frontend.home.partials.loader')
                    </div>
                 </div>
              </li>

           </ul>

        </div>
    </div>

    @include('frontend.home.partials.below')
  </div>
</div>
