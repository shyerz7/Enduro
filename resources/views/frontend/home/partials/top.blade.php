{{-- Top COntent    --}}
<div class="row">
  <div class="col-lg-12">
     <div class="ibox float-e-margins">
        <div class="ibox-content">
           <div class="row">

              <div class="col-lg-2 text-center">

                  @if($mainstore->ava)
                    <img src="{{ asset('attachments/ava_store/thumb').DIRECTORY_SEPARATOR.$mainstore->ava }}" class="img-thumbnail img-responsive" style="min-height: 140px;">

                  @else
                    <img src="http://lorempixel.com/200/200/city" class="img-thumbnail img-circle img-responsive">
                  @endif

              </div>


              <div class="col-md-3 text-center">

                 <h2 style="color:black;">Mystery Shopping Plasa Indonesia</h2>

                 <table class="table table-score table-responsive">

                      <tbody>
                        <tr class="text-center">
                            <td class="bg-red" style="width: 20%;color:white;">Sangat Kurang</td>
                            <td class="bg-orange" style="width: 20%;color:white;">Kurang</td>
                            <td class="bg-yellow" style="width: 20%;color:white;">Cukup</td>
                            <td class="bg-green" style="width: 20%;color:white;">Baik</td>
                            <td class="bg-blue" style="width: 20%;color:white;">Sangat Baik</td>
                        </tr>
                        <tr>
                            <td>
                                <span class="pull-left">0</span>
                                <span class="pull-right">54.9</span>
                            </td>
                            <td>
                                <span class="pull-left">55</span>
                                <span class="pull-right">59.9</span>
                            </td>
                            <td>
                                <span class="pull-left">60</span>
                                <span class="pull-right">69.9</span>
                            </td>
                            <td>
                                <span class="pull-left">70</span>
                                <span class="pull-right">79.9</span>
                            </td>
                            <td>
                                <span class="pull-left">80</span>
                                <span class="pull-right">100</span>
                            </td>
                        </tr>
                    </tbody>
                  </table>
              </div>

              <div class="col-md-7">

                 <div class="row">
                    <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             <h5><span class="fa fa-flag"></span> Nilai Nasional</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">

                            <?php

                                if(is_null($score)){
                                    $score = 0;
                                }else{
                                    $score = $score->score;
                                }

                                if($score >= 85.00)
                                {
                                    $warna = 'bg-blue';
                                    $teks  = 'Sangat Baik';
                                    $icon  = 'glyphicon glyphicon-thumbs-up';
                                }
                                elseif($score >= 75.00 && $score <= 84.99)
                                {
                                    $warna = 'bg-green';
                                    $teks  = 'Baik';
                                    $icon  = 'glyphicon glyphicon-plus';
                                }
                                elseif($score >= 65.00 && $score <= 74.99)
                                {
                                    $warna = 'bg-yellow';
                                    $teks  = 'Cukup';
                                    $icon  = 'glyphicon glyphicon-ok';
                                }
                                elseif($score >= 55.00 && $score <= 64.99)
                                {
                                    $warna = 'bg-orange';
                                    $teks  = 'Kurang';
                                    $icon  = 'glyphicon glyphicon-ok';
                                }
                                elseif($score <= 54.99)
                                {
                                    $warna = 'bg-red';
                                    $teks  = 'Sangat Kurang';
                                    $icon  = 'glyphicon glyphicon-thumbs-down';
                                }elseif(is_null($score))
                                {
                                    $warna = 'bg-red';
                                    $teks  = '-';
                                    $icon  = 'glyphicon glyphicon-thumbs-down';
                                }

                            ?>

                            <span class="label {{ $warna }} pull-right" style="color:white">
                             <h1 class="no-margins" style="font-size: 48px;">{{ $score or '-' }}</h1>
                             <small>Total Index</small>
                            </span>
                          </div>
                       </div>
                    </div>

                    <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             {{-- <span class="label label-info pull-right">Current Wave</span> --}}
                             <h5><span class="fa fa-building"></span> RBH I</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">

                            <?php

                                $rbh1_scores  =  $rbh1_scores ? $rbh1_scores->score : 0;


                                if($rbh1_scores >= 85.00)
                                {
                                    $warnarbh1 = 'bg-blue';
                                    $teksbh1  = 'Sangat Baik';
                                    $iconbh1  = 'glyphicon glyphicon-thumbs-up';
                                }
                                elseif($rbh1_scores >= 75.00 && $rbh1_scores <= 84.99)
                                {
                                    $warnarbh1 = 'bg-green';
                                    $teksbh1  = 'Baik';
                                    $iconbh1  = 'glyphicon glyphicon-plus';
                                }
                                elseif($rbh1_scores >= 65.00 && $rbh1_scores <= 74.99)
                                {
                                    $warnarbh1 = 'bg-yellow';
                                    $teksbh1  = 'Cukup';
                                    $iconbh1  = 'glyphicon glyphicon-ok';
                                }
                                elseif($rbh1_scores >= 55.00 && $rbh1_scores <= 64.99)
                                {
                                    $warnarbh1 = 'bg-orange';
                                    $teksbh1  = 'Kurang';
                                    $iconbh1  = 'glyphicon glyphicon-ok';
                                }
                                elseif($rbh1_scores <= 54.99)
                                {
                                    $warnarbh1 = 'bg-red';
                                    $teksbh1  = 'Sangat Kurang';
                                    $iconbh1  = 'glyphicon glyphicon-thumbs-down';
                                }elseif(is_null($rbh1_scores))
                                {
                                    $warnarbh1 = 'bg-red';
                                    $teksbh1  = '-';
                                    $iconbh1  = 'glyphicon glyphicon-thumbs-down';
                                }


                             ?>

                             <span class="label {{ $warnarbh1 }} pull-right" style="color:white">
                             <h1 class="no-margins" style="font-size: 48px;">{{ $rbh1_scores or '-' }}</h1>
                             <small>Total Index</small>
                            </span>


                          </div>
                       </div>
                    </div>

                    <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             {{-- <span class="label label-info pull-right">Current Wave</span> --}}
                             <h5><span class="fa fa-building"></span> RBH II</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">

                            <?php

                                $rbh2_scores  =  $rbh2_scores ? $rbh2_scores->score : 0;

                                if($rbh2_scores >= 85.00)
                                {
                                    $warnarbh2 = 'bg-blue';
                                    $teksbh2  = 'Sangat Baik';
                                    $iconbh2  = 'glyphicon glyphicon-thumbs-up';
                                }
                                elseif($rbh2_scores >= 75.00 && $rbh2_scores <= 84.99)
                                {
                                    $warnarbh2 = 'bg-green';
                                    $teksbh2  = 'Baik';
                                    $iconbh2  = 'glyphicon glyphicon-plus';
                                }
                                elseif($rbh2_scores >= 65.00 && $rbh2_scores <= 74.99)
                                {
                                    $warnarbh2 = 'bg-yellow';
                                    $teksbh2  = 'Cukup';
                                    $iconbh2  = 'glyphicon glyphicon-ok';
                                }
                                elseif($rbh2_scores >= 55.00 && $rbh2_scores <= 64.99)
                                {
                                    $warnarbh2 = 'bg-orange';
                                    $teksbh2  = 'Kurang';
                                    $iconbh2  = 'glyphicon glyphicon-ok';
                                }
                                elseif($rbh2_scores <= 54.99)
                                {
                                    $warnarbh2 = 'bg-red';
                                    $teksbh2  = 'Sangat Kurang';
                                    $iconbh2  = 'glyphicon glyphicon-thumbs-down';
                                }elseif(is_null($rbh2_scores))
                                {
                                    $warnarbh2 = 'bg-red';
                                    $teksbh2  = '-';
                                    $iconbh2  = 'glyphicon glyphicon-thumbs-down';
                                }

                             ?>

                             <span class="label {{ $warnarbh2 }} pull-right" style="color:white">
                             <h1 class="no-margins" style="font-size: 48px;">{{ $rbh2_scores or '-' }}</h1>
                             <small>Total Index</small>
                            </span>

                          </div>
                       </div>
                    </div>

                    <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             {{-- <span class="label label-info pull-right">Current Wave</span> --}}
                             <h5><span class="fa fa-building"></span> RBH III</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">

                             <?php

                                $rbh3_scores  =  $rbh3_scores ? $rbh3_scores->score : 0;

                                if($rbh3_scores >= 85.00)
                                {
                                    $warnarbh3 = 'bg-blue';
                                    $teksbh3  = 'Sangat Baik';
                                    $iconbh3  = 'glyphicon glyphicon-thumbs-up';
                                }
                                elseif($rbh3_scores >= 75.00 && $rbh3_scores <= 84.99)
                                {
                                    $warnarbh3 = 'bg-green';
                                    $teksbh3  = 'Baik';
                                    $iconbh3  = 'glyphicon glyphicon-plus';
                                }
                                elseif($rbh3_scores >= 65.00 && $rbh3_scores <= 74.99)
                                {
                                    $warnarbh3 = 'bg-yellow';
                                    $teksbh3  = 'Cukup';
                                    $iconbh3  = 'glyphicon glyphicon-ok';
                                }
                                elseif($rbh3_scores >= 55.00 && $rbh3_scores <= 64.99)
                                {
                                    $warnarbh3 = 'bg-orange';
                                    $teksbh3  = 'Kurang';
                                    $iconbh3  = 'glyphicon glyphicon-ok';
                                }
                                elseif($rbh3_scores <= 54.99)
                                {
                                    $warnarbh3 = 'bg-red';
                                    $teksbh3  = 'Sangat Kurang';
                                    $iconbh3  = 'glyphicon glyphicon-thumbs-down';
                                }elseif(is_null($rbh3_scores))
                                {
                                    $warnarbh3 = 'bg-red';
                                    $teksbh3  = '-';
                                    $iconbh3  = 'glyphicon glyphicon-thumbs-down';
                                }

                             ?>

                             <span class="label {{ $warnarbh3 }} pull-right" style="color:white">
                             <h1 class="no-margins" style="font-size: 48px;">{{ $rbh3_scores or '-' }}</h1>
                             <small>Total Index</small>
                            </span>

                          </div>
                       </div>
                    </div>

                    <!-- <div class="col-lg-6">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             <h5><span class="fa fa-building"></span> Cabang Terbaik</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">
                            @if(!is_null($top_store))
                             <h1 class="no-margins" style="font-size: 48px;">{{ $top_store->average }}</h1>
                             <small>{{ $top_store->nama }}</small>
                            @endif
                          </div>
                       </div>
                    </div> -->

                 </div>

                 {{-- <div class="row">
                    <ul id="range" class="" style="padding: 0;padding-left: 20px;">
                         <li><a href="#"> <i class="fa fa-circle text-navy"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-danger"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-primary"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-info"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-warning"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                     </ul>
                 </div> --}}

              </div>


           </div>
        </div>
     </div>
  </div>
</div>
