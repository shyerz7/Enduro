@extends('frontend.layouts.app')

@section('css')
<style>

    .bg-red {
        background-color: #f56954 !important;
    }
    .bg-orange {
        background-color: #ff851b !important;
    }
    .bg-yellow {
        background-color: #f39c12 !important;
    }
    .bg-green {
        background-color: #00a65a !important;
    }
    .bg-blue {
        background-color: #0073b7 !important;
    }
    /*Box Berwarna*/
    .small-box {
        position: relative;
        display: block;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        margin-bottom: 15px
    }


    </style>
@endsection

@section('content')

@include('frontend.kota.partials.top')
@include('frontend.kota.partials.collapse')
@include('frontend.kota.partials.middle')
@include('frontend.kota.partials.below')

@endsection

@section('js')
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>

  <script>
    $('#chart-performance').highcharts({
            chart: {
                    type: 'column'
                },
                colors: ['#3498DB'],
                title: {
                    text: 'Cabang Performance'
                },
                xAxis: {
                    type: 'category',
                     title: {
                        text: ' '
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    column: {
                    colorByPoint: true
                },
                    series: {
                        borderWidth: 0,
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                yAxis: {
                     // tickPositioner: function() {
                     //        return [0,10];
                     //    },
                     gridLineWidth: 0,

                    title: {
                        text: 'Persentase'
                    }

                },
                credits:{
                    enabled:false
                },

                series: [{
                    name: 'Score',
                    colorByPoint: true,
                    data: {!! json_encode($report_performance_data,JSON_NUMERIC_CHECK) !!}
                }],
        });
<?php
$score  =  $kc_scores ? $kc_scores->score : 0;
?>
        Highcharts.chart('comparisonChart', {
            chart: {
                type: 'column',
                height: 250,
            },
            title: {
                text: '{{ $kota->name or '' }}'
            },
            xAxis: {
                categories: ['INDUSTRI', 'BANKWIDE', '{{ $kota->name or '' }}']
            },
            legend: {
                enabled: false
            },

            plotOptions: {
                series: {
                    borderWidth: 0,
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true
                    }
                },
                column: {
                  colorByPoint: true
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Komparasi Skor',
                data: [88.3, 85.9, {{ $score  }}]

            }]
        });
  </script>
@endsection
