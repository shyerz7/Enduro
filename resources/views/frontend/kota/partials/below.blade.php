{{-- Below Content --}}
<div class="row" style="padding-top: 10px;">
  <div class="col-md-12">
     <div class="tabs-container">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#roottab-1"> Cabang</a></li>
           {{--  <li class=""><a data-toggle="tab" href="#roottab-2">10 Dealer Teratas Per Posisi</a></li>
            <li class=""><a data-toggle="tab" href="#roottab-2">Kota Teratas</a></li>
            <li class=""><a data-toggle="tab" href="#roottab-2">Dealer Berdasarkan Kota</a></li>
            <li class=""><a data-toggle="tab" href="#roottab-2">Dealer</a></li>
            <li class=""><a data-toggle="tab" href="#roottab-2">Staff</a></li> --}}
        </ul>
        <div class="tab-content">
            <div id="roottab-1" class="tab-pane active">
                <div class="panel-body">
                    <div class="row">
                      @foreach($stores as $store)
                        <div class="col-sm-2">
                           <div class="file-box">
                              <div class="file">
                                  <a href="{{ url('store',$store->code) }}">
                                      <span class="corner"></span>
                                      <?php
                                      $name_clean = str_replace('.', '', $store->name);
                                      ?>
                                      <div style="background-color: #0070ba;height: 110px;">
                                        @if(!is_null($store->ava))
                                          <img src="{{ asset('attachments/ava_store/thumb').DIRECTORY_SEPARATOR.$store->ava }}">

                                          <!-- <h3 class="text-center" style="color: #fff">{{ $store->name or '' }}</h3> -->
                                        @else
                                          <!-- <img src="{{ asset('attachments/ava_store/thumb').DIRECTORY_SEPARATOR.'bnp.png' }}" style="height: 100px" class="img-responsive center-block"> -->
                                          <h4 class="text-center" style="color: #fff; line-height: 110px;">{{ $store->name or '' }}</h4>
                                        @endif
                                      </div>
                                      <div class="file-name">
                                          <p style="font-size:10pt;">{{ $store->name or '' }}</p>
                                          <small> <span class="label label-primary">{{ $store->code }}</span> - {{ $store->type  }}</small>
                                      </div>
                                  </a>

                              </div>
                          </div>
                        </div>
                      @endforeach

                        </div>
                </div>
            </div>

        </div>


    </div>
  </div>
</div>
