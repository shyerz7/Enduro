<div class="row">

  <div class="col-md-12">
     <div class="tabs-container">
        <ul class="nav nav-tabs">
           <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> KCP & KK Performance</a></li>
        </ul>
        <div class="tab-content">
           <div id="tab-1" class="tab-pane active">
              <div class="panel-body">
                 <div id="chart-performance" style="width:auto; min-height: 250px; margin: 0 auto"></div>
              </div>
           </div>
        </div>
     </div>
  </div>

  {{-- <div class="col-md-4">
     <div class="ibox ">
        <div class="ibox-content">
           <ul class="list-group link-index">
              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-highest" data-ajax="http://192.168.1.77:8080/bjb_rad/ajax/highest">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Tertinggi
                    </a>
                    <div id="collapse-highest" class="collapse mrg-top-10">
                       <div class="loader">Loading...</div>
                    </div>
                 </div>
              </li>
              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-lowest" data-ajax="http://192.168.1.77:8080/bjb_rad/ajax/lowest">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Terrendah
                    </a>
                    <div id="collapse-lowest" class="collapse mrg-top-10">
                       <div class="loader">Loading...</div>
                    </div>
                 </div>
              </li>
              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-average" data-ajax="http://192.168.1.77:8080/bjb_rad/ajax/average">
                    <i class="glyphicon glyphicon-plus"></i>
                    Nilai Rata-rata
                    </a>
                    <div id="collapse-average" class="collapse mrg-top-10">
                       <div class="loader">Loading...</div>
                    </div>
                 </div>
              </li>
              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#position-1" data-ajax="http://192.168.1.77:8080/bjb_rad/ajax/position-index/1">
                    <i class="glyphicon glyphicon-plus"></i>
                    Number of Staff (< 3.00)                  </a>
                    <div id="position-1" class="collapse mrg-top-10">
                       <div class="loader">Loading...</div>
                    </div>
                 </div>
              </li>
              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#position-4" data-ajax="http://192.168.1.77:8080/bjb_rad/ajax/position-index/4">
                    <i class="glyphicon glyphicon-plus"></i>
                    Number of Staff (3.00 - 3.50)                      </a>
                    <div id="position-4" class="collapse mrg-top-10">
                       <div class="loader">Loading...</div>
                    </div>
                 </div>
              </li>
              <li class="list-group-item">
                 <div id="accordion" class="accordion">
                    <a data-toggle="collapse" data-parent="#accordion" href="#position-3" data-ajax="http://192.168.1.77:8080/bjb_rad/ajax/position-index/3">
                    <i class="glyphicon glyphicon-plus"></i>
                    Number of Staff ( > 3.50)                      </a>
                    <div id="position-3" class="collapse mrg-top-10">
                       <div class="loader">Loading...</div>
                    </div>
                 </div>
              </li>
           </ul>
              
        </div>
    </div>
  </div> --}}

</div>