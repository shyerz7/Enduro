{{-- Top COntent    --}}
<div class="row">
  <div class="col-lg-12">
     <div class="ibox float-e-margins">
        <div class="ibox-content">
           <div class="row">

              <div class="col-lg-2">
                 <img src="{{ asset('attachments/ava_store/thumb').DIRECTORY_SEPARATOR.'bnp.png' }}" class="img-thumbnail img-responsive">
              </div>

              <div class="col-md-4">
                <h2 style="color: black;">{{ $kota->name or '' }}</h2>
                <address>
                 <p>
                    <i class="fa fa-map-marker"></i>
                    {{ $kota->address or '' }}
                 </p>
                 </address>
                 <table class="table table-score table-responsive">

                      <tbody>
                        <tr class="text-center">
                            <td class="bg-red" style="width: 20%;color:white;">Sangat Kurang</td>
                            <td class="bg-orange" style="width: 20%;color:white;">Kurang</td>
                            <td class="bg-yellow" style="width: 20%;color:white;">Cukup</td>
                            <td class="bg-green" style="width: 20%;color:white;">Baik</td>
                            <td class="bg-blue" style="width: 20%;color:white;">Sangat Baik</td>
                        </tr>
                        <tr>
                            <td>
                                <span class="pull-left">0</span>
                                <span class="pull-right">54.9</span>
                            </td>
                            <td>
                                <span class="pull-left">55</span>
                                <span class="pull-right">59.9</span>
                            </td>
                            <td>
                                <span class="pull-left">60</span>
                                <span class="pull-right">69.9</span>
                            </td>
                            <td>
                                <span class="pull-left">70</span>
                                <span class="pull-right">79.9</span>
                            </td>
                            <td>
                                <span class="pull-left">80</span>
                                <span class="pull-right">100</span>
                            </td>
                        </tr>
                    </tbody>
                  </table>
              </div>

              <div class="col-md-6">

                 <div class="row">
                    <div class="col-lg-4">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">

                             <?php

                                $score  =  $kc_scores ? $kc_scores->score : 0;

                                if($score >= 85.00)
                                {
                                    $warna = 'bg-blue';
                                    $teks  = 'Sangat Baik';
                                    $icon  = 'glyphicon glyphicon-thumbs-up';
                                }
                                elseif($score >= 75.00 && $score <= 84.99)
                                {
                                    $warna = 'bg-green';
                                    $teks  = 'Baik';
                                    $icon  = 'glyphicon glyphicon-plus';
                                }
                                elseif($score >= 65.00 && $score <= 74.99)
                                {
                                    $warna = 'bg-yellow';
                                    $teks  = 'Cukup';
                                    $icon  = 'glyphicon glyphicon-ok';
                                }
                                elseif($score >= 55.00 && $score <= 64.99)
                                {
                                    $warna = 'bg-orange';
                                    $teks  = 'Kurang';
                                    $icon  = 'glyphicon glyphicon-ok';
                                }
                                elseif($score <= 54.99)
                                {
                                    $warna = 'bg-red';
                                    $teks  = 'Sangat Kurang';
                                    $icon  = 'glyphicon glyphicon-thumbs-down';
                                }elseif(is_null($score))
                                {
                                    $warna = 'bg-red';
                                    $teks  = '-';
                                    $icon  = 'glyphicon glyphicon-thumbs-down';
                                }

                             ?>

                             <span class="label label-primary pull-right">Semester 1-2017</span>

                          </div>
                          <div class="ibox-content ibox-content-highlight">
                             <span class="label {{ $warna }} pull-right" style="color:white">
                               <h1 class="no-margins" >{{ $score  }}</h1>
                             </span>

                          </div>
                       </div>
                    </div>
                    <div class="col-lg-4">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             {{-- <span class="label label-info pull-right">Current Wave</span> --}}
                             <h5>Cabang Tertinggi</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">
                             <h1 class="no-margins">{{ $storemax->score  }}</h1>
                             <small>{{ $storemax->nama   }}</small>
                          </div>
                       </div>
                    </div>
                    <div class="col-lg-4">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             {{-- <span class="label label-info pull-right">Current Wave</span> --}}
                             <h5>Cabang Terendah</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">
                             <h1 class="no-margins">{{ $storemin->score  }}</h1>
                             <small>{{ $storemin->nama   }}</small>
                          </div>
                       </div>
                    </div>

                 </div>
                 <div class="row">
                   <div class="col-md-12 text-center">
                     <div id="comparisonChart"></div>
                   </div>
                 </div>

                 {{-- <div class="row">
                    <ul id="range" class="" style="padding: 0;padding-left: 20px;">
                         <li><a href="#"> <i class="fa fa-circle text-navy"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-danger"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-primary"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-info"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-warning"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                     </ul>
                 </div> --}}

              </div>

           </div>
        </div>
     </div>
  </div>
</div>
