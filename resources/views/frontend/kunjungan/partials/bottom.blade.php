{{-- Daftar Staff dan Fisik --}}
<div class="row">

   <div class="col-md-8">
      <div class="ibox">
         <div class="ibox-content">
            <h2>Nilai Individu</h2>
             <table class="table table-striped table-hover">
               <tbody>

                  @foreach($staffs as $staff)
                     <tr>
                        <td class="client-avatar">

                           @if($staff->ava)
                            <img src="{{ asset('attachments/ava_staff/table').DIRECTORY_SEPARATOR.$staff->ava }}" class="user-image img-circle" alt="{{ $staff->nama }}">
                          @else
                            <img src="http://placehold.it/300x300" alt="">
                          @endif

                        </td>
                        <td><a data-toggle="tab" href="#contact-1" class="client-link" aria-expanded="true">{{ $staff->nama or '-' }}</a></td>
                        <td>{{ $staff->position->alias }}</td>
                        <td class="client-status">
                           <a href="{{ url('/staff',$staff->id) }}" class="btn btn-primary btn-xs">Lihat Detail</a>
                        </td>
                     </tr>
                  @endforeach


               </tbody>
            </table>
         </div>
      </div>
   </div>

   <div class="col-md-4">
   {{--    <div class="ibox">
         <div class="ibox-content">
            <h2>Fisiks</h2>
            <p>
               Semua fisik yang sudah show di webreport, data, mp3 dan video sudah melewati proses QC (Quality Control) terlebih dahulu
            </p>
            <ul class="folder-list m-b-md" style="padding: 0">
              @foreach($fisikstores as $fisikstore)
                  <li><a href="{{ url('/fisikstore',$fisikstore->id) }}"> <i class="fa fa-inbox "></i> {{ $fisikstore->fisik->name }} <span class="label label-warning pull-right">lihat detail</span> </a></li>
              @endforeach
            </ul>
         </div>
      </div>
   </div> --}}

</div><!-- row -->
