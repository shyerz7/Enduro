{{-- Middle COntent --}}
<div class="row">

  <div class="col-md-12">
    <div class="ibox float-e-margins">
       <div class="ibox-content">

            <h2>KUNJUNGAN {{ $so->id }}<small></h2>
              <br />
                        <div class="row">
                        @foreach($staffs as $staff)
                          <a href="{{ url('staff', $staff->id) }}">
                          <div class="col-md-4">
                            <div class="panel panel-success">
                              <div class="panel-heading" style="background-color: #00baff; border: none;">
                                <h3 class="panel-title"></h3>
                              </div>
                              <div class="panel-body" style="min-height: 160px">

                                  <div class="row">
                                    <div class="col-md-5">
                                        <div class="image-cropper text-center" style="background-color: #00baff">
                                          @if(!is_null($staff->ava))
                                              <img src="{{ asset('attachments/ava_staff/thumb').'/'.$staff->ava }}" alt="" class="img">
                                          @else
                                              <img src="http://lorempixel.com/300/200/business" class="img">
                                          @endif
                                        </div>
                                    </div>

                                    <div class="col-md-7">
                                        <h4>{{ $staff->nama or '' }}</h4>
                                        <p style="font-size: 8pt;">{{ $staff->position_name }}</p>
                                        <p><span class="label label-warning">Lihat Detail >></span></p>
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                          </a>
                        @endforeach
                        </div>
                        {{ $staffs->links() }}
        </div>
      </div>
  </div>

</div>
