{{-- Top COntent    --}}
<div class="row">
  <div class="col-lg-12">
     <div class="ibox float-e-margins">
        <div class="ibox-content">
           <div class="row">

              <div class="col-lg-2">
                @if(!is_null($store->ava))
                  <img src="{{ asset('attachments/ava_store/thumb/'.$store->ava) }}" class="img-thumbnail img-responsive">
                @else
                  <img src="{{ asset('attachments/ava_store/thumb/bnp.png') }}" class="img-thumbnail img-responsive">
                @endif

              </div>

              <div class="col-md-3">
                  <h2 style="color: black;">{{ $store->name or 'Store Name' }}</h2>
                  <p>
                     <i class="fa fa-map-marker"></i>
                     {!! $store->address or '-' !!}
                  </p>
                  <table class="table table-score table-responsive">
                       <tbody>
                          <tr class="text-center">
                             <td class="bg-red" style="width: 20%">Sangat Kurang</td>
                             <td class="bg-orange" style="width: 20%">Kurang</td>
                             <td class="bg-yellow" style="width: 20%">Cukup</td>
                             <td class="bg-green" style="width: 20%">Baik</td>
                             <td class="bg-blue" style="width: 20%">Sangat Baik</td>
                          </tr>
                          <tr>
                             <td>
                                <span class="pull-left">0</span>
                                <span class="pull-right">54.9</span>
                             </td>
                             <td>
                                <span class="pull-left">55</span>
                                <span class="pull-right">64.9</span>
                             </td>
                             <td>
                                <span class="pull-left">65</span>
                                <span class="pull-right">74.9</span>
                             </td>
                             <td>
                                <span class="pull-left">75</span>
                                <span class="pull-right">84.9</span>
                             </td>
                             <td>
                                <span class="pull-left">80</span>
                                <span class="pull-right">100</span>
                             </td>
                          </tr>
                       </tbody>
                  </table>
              </div>

              <div class="col-md-7">

                 <div class="row">
                    <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             <h5>Branch Score</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">
                             <h1 class="no-margins">{{ $store->reportstore->score or '-' }}</h1>
                             {{-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> --}}
                          </div>
                       </div>
                    </div>

                    <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             <h5>Staff Terbaik</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">
                             <h1 class="no-margins">{{ $staffmax ?  $staffmax->score : '-' }}</h1>
                             <small>{{ $staffmax ? $staffmax->nama : '-' }} - {{ $staffmax ? $staffmax->alias : '-' }}</small>
                          </div>
                       </div>
                    </div>

                    <div class="col-lg-3">
                       <div class="ibox float-e-margins">
                          <div class="ibox-title">
                             <h5>SSO</h5>
                          </div>
                          <div class="ibox-content ibox-content-highlight">
                            	{{ $so->so1 }} <br />
                            	{{ $so->so2 }} <br />
                            	{{ $so->so3 }} 
                          </div>
                       </div>
                    </div>
                    <div class="row">
                     <div class="col-md-12">
                       <div id="comparisonChart"></div>
                     </div>
                    </div>
                 </div>

                 {{-- <div class="row">
                    <ul id="range" class="" style="padding: 0;padding-left: 20px;">
                         <li><a href="#"> <i class="fa fa-circle text-navy"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-danger"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-primary"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-info"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                         <li><a href="#"> <i class="fa fa-circle text-warning"></i> 0-59.99 (Sangat Kurang Baik)</a></li>
                     </ul>
                 </div> --}}

              </div>

           </div>
        </div>
     </div>
  </div>
</div>
