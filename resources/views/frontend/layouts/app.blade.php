<!DOCTYPE html>
<html>
   <!-- Mirrored from webapplayers.com/inspinia_admin-v2.7/dashboard_2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 16 Mar 2017 05:17:27 GMT -->
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Plasa Indonesia Web Report</title>
      <link href="{!! asset('frontend/css/bootstrap.min.css') !!}" rel="stylesheet">
      <link href="{!! asset('frontend/font-awesome/css/font-awesome.css') !!}" rel="stylesheet">
      <link href="{!! asset('frontend/css/animate.css') !!}" rel="stylesheet">
      <link href="{!! asset('frontend/css/style.css') !!}" rel="stylesheet">

      @yield('css')

      <style>
         ul#range li {
             display:inline;
         }
         .footer-bnp {
             position:relative;
             background:#aeaeae;
             padding-top: 30px;
             padding-bottom: 30px;
             padding-left: 10px;
             padding-right: 10px;
         }
      </style>
   </head>
   <body>

      <div id="wrapper">

         <div id="">

            <!-- Header -->
            @include('frontend.partials.header')

            {{-- <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Helper css classes</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li>
                            <a>Forms</a>
                        </li>
                        <li class="active">
                            <strong>Helper css classes</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div> --}}

            <!-- Content -->
            <div class="wrapper wrapper-content">
              <div class="container">
                @yield('content')
              </div>
            </div><!-- end wrapper wrapper-content -->

         </div><!-- gray-bg -->

      </div> <!-- end div wrapper -->

      <!-- Footer -->
            {{-- @include('frontend.partials.footer') --}}
      <!-- Mainly scripts -->
      <script src="{!! asset('frontend/js/jquery-3.1.1.min.js') !!}"></script>
      <script src="{!! asset('frontend/js/bootstrap.min.js') !!}"></script>

      {{-- <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
      <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script> --}}


      @yield('js')
   </body>
</html>
