   <nav class="navbar navbar-static-top white-bg" role="navigation">
     <div class="container">
      <div class="row">
        {{-- Logo --}}
        <div class="col-xs-6 col-md-2">
           <div class="navbar-header" style="padding-top: 3px;">
            <a class="navbar-minimalize minimalize-styl-1 " href="{{ url ('home') }}"><img src="{{ asset('frontend/img/logo_header.png') }}" alt=""> </a>
          </div>
        </div>

        {{-- Left Navbar --}}
        <div class="col-xs-6 col-md-5">
          <ul class="nav navbar-top-links navbar-left">



          </ul>
        </div>

        {{-- Right Navbar --}}
        <div class="col-xs-12 col-md-5">
          <ul class="nav navbar-top-links navbar-right">

            <li>
              <ol class="breadcrumb">
                  <li><a href="{{ url('/home') }}">Home</a></li>
              </ol>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-user" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                   <li>
                      @role('admin')
                      <a href="{{ route('admin.home') }}">
                         <div>
                            <i class="fa fa-cog" aria-hidden="true"></i> Panel
                         </div>
                      </a>
                       @endrole
                      <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                         <div>
                            <i class="fa fa-sign-out" aria-hidden="true"></i> Signout
                         </div>
                      </a>
                      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>

                   </li>

                </ul>
             </li>

            </ul>
        </div>

      </div>
    </div>
   </nav>
