@extends('frontend.layouts.app')

@section('css')
    <link href="http://vjs.zencdn.net/5.10.7/video-js.css" rel="stylesheet">
    <!-- If you'd like to support IE8 -->
    <script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

    <style>
    	.bg-red {
		    background-color: #f56954 !important;
		}
		.bg-orange {
		    background-color: #ff851b !important;
		}
		.bg-yellow {
		    background-color: #f39c12 !important;
		}
		.bg-green {
		    background-color: #00a65a !important;
		}
		.bg-blue {
		    background-color: #0073b7 !important;
		}
		/*Box Berwarna*/
		.small-box {
		    position: relative;
		    display: block;
		    -webkit-border-radius: 2px;
		    -moz-border-radius: 2px;
		    border-radius: 2px;
		    margin-bottom: 15px
		}

		.small-box > .inner {
		    padding: 10px
		}

		.small-box > .small-box-footer {
		    position: relative;
		    text-align: center;
		    padding: 3px 0;
		    color: #fff;
		    color: rgba(255, 255, 255, .8);
		    display: block;
		    z-index: 10;
		    background: rgba(0, 0, 0, .1);
		    text-decoration: none
		}

		.small-box > .small-box-footer:hover {
		    color: #fff;
		    background: rgba(0, 0, 0, .15)
		}

		.small-box h3 {
		    font-size: 38px;
		    font-weight: 700;
		    margin: 0 0 10px;
		    white-space: nowrap;
		    padding: 0
		}

		.small-box p {
		    font-size: 15px
		}

		.small-box p > small {
		    display: block;
		    color: #f9f9f9;
		    font-size: 13px;
		    margin-top: 5px
		}

		.small-box h3, .small-box p {
		    z-index: 5px
		}

		.small-box .icon {
		    position: absolute;
		    top: auto;
		    bottom: 25px;
		    right: 10px;
		    z-index: 0;
		    font-size: 60px;
		    color: rgba(0, 0, 0, .15)
		}

		.small-box:hover {
		    text-decoration: none;
		    color: #f9f9f9
		}
		@media screen and (max-width: 480px) {
		    .small-box {
		        text-align: center
		    }

		    .small-box .icon {
		        display: none
		    }

		    .small-box p {
		        font-size: 12px
		    }

		}

		.small-box:hover .icon {
		    animation-name: tansformAnimation;
		    animation-duration: .5s;
		    animation-iteration-count: 1;
		    animation-timing-function: ease;
		    animation-fill-mode: forwards;
		    -webkit-animation-name: tansformAnimation;
		    -webkit-animation-duration: .5s;
		    -webkit-animation-iteration-count: 1;
		    -webkit-animation-timing-function: ease;
		    -webkit-animation-fill-mode: forwards;
		    -moz-animation-name: tansformAnimation;
		    -moz-animation-duration: .5s;
		    -moz-animation-iteration-count: 1;
		    -moz-animation-timing-function: ease;
		    -moz-animation-fill-mode: forwards
		}
    </style>

@endsection

@section('content')

<div class="row">

    @include('frontend.staff.partials.left')

	<div class="col-md-9">
		<div class="ibox">
			<div class="ibox-content">
				<div class="tabs-container">
				   <ul class="nav nav-tabs">
				      <!-- <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="false"> Summary</a></li> -->
				      <li class="active"><a data-toggle="tab" href="#tab-2" aria-expanded="true">Score</a></li>
				      <!-- <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="true">Chart Performance</a></li> -->
				   </ul>
				   <div class="tab-content">

				      <!-- <div id="tab-1" class="tab-pane active">
				         <div class="panel-body">

				            <legend>Laporan</legend>

							@if($staff_score->summary)
							    {!!  $staff_score->summary !!}
							@else
							    <div class="mrg-top-30">
							        <div class="alert alert-warning alert-dismissable">
							            <i class="glyphicon glyphicon-warning-sign"></i>
							            Belum ada konten laporan untuk staff ini.
							        </div>
							    </div>
							@endif


				         </div>
				      </div> -->

				      <div id="tab-2" class="tab-pane active">
				         <div class="panel-body">

				            <div class="row">

				            	<div class="col-md-4">
				            		<div class="ibox float-e-margins">
			                          <?php
                           $teks = '';
							             if($score >=  80.00)
							            {
							                $warna = 'bg-blue';
							                // $teks  = 'Sangat Baik';
							                $icon  = 'glyphicon glyphicon-thumbs-up';
							            }
							            elseif($score >= 70.00 && $score <= 79.99)
							            {
							                $warna = 'bg-green';
							                // $teks  = 'Baik';
							                $icon  = 'glyphicon glyphicon-plus';
							            }
							            elseif($score >=60.00 && $score <= 69.99)
							            {
							                $warna = 'bg-yellow';
							                // $teks  = 'Cukup';
							                $icon  = 'glyphicon glyphicon-ok';
							            }
							             elseif($score >=55.00 && $score <= 59.99)
							            {
							                $warna = 'bg-orange';
							                // $teks  = 'Kurang';
							                $icon  = 'glyphicon glyphicon-ok';
							            }
							            elseif($score <= 0.1)
							            {
							                $warna = 'bg-red';
							                $teks  = 'The Officer did not do the core standards';
							                $icon  = 'glyphicon glyphicon-thumbs-down';
							            }
                          elseif(is_null($score))
							            {
							                $warna = 'bg-red';
							                $teks  = '-';
							                $icon  = 'glyphicon glyphicon-thumbs-down';
							            }
							        ?>
			                          <div class="small-box mrg-top-20 text-left pull-right" style="width:100%" >
								            <div class="inner">
								                <h3 style="color:#000000;">
								                    @if(!is_null($score))
								                      {{ number_format($score, 1) }}
								                    @else
								                      -
								                    @endif
								                  </h3>
                                  @if($score <= 0.1)
								                  <div class="badge">
								                      {{ $teks }}
								                  </div>
                                  @else
                                  <p></p>
                                  @endif
								            </div>
								            <!-- <div class="icon">
								              <i class="glyphicon glyphicon-ok"></i>
								            </div> -->
								            <!-- <a class="small-box-footer"></a> -->
								        </div>

			                          <!-- <table class="table table-score table-responsive">
									        <tbody><tr class="text-center">
									            <td class="bg-red" style="width: 20%">Sangat Kurang</td>
									            <td class="bg-orange" style="width: 20%">Kurang</td>
									            <td class="bg-yellow" style="width: 20%">Cukup</td>
									            <td class="bg-green" style="width: 20%">Baik</td>
									            <td class="bg-blue" style="width: 20%">Sangat Baik</td>
									        </tr>
									        <tr>
									            <td>
									                <span class="pull-left">0</span>
									                <span class="pull-right">20</span>
									            </td>
									            <td>
									                <span class="pull-left">21</span>
									                <span class="pull-right">45</span>
									            </td>
									            <td>
									                <span class="pull-left">46</span>
									                <span class="pull-right">65</span>
									            </td>
									            <td>
									                <span class="pull-left">66</span>
									                <span class="pull-right">80</span>
									            </td>
									            <td>
									                <span class="pull-left">81</span>
									                <span class="pull-right">100</span>
									            </td>
									        </tr>
									    </tbody></table> -->
			                       </div>
				            	</div>

				            	<div class="col-md-8">

				            		@if($file_report)
				            		    @if($file_report->filename)

                              @if(strpos($file_report->filename, 'mp3') !== false)
                                <audio src="{{ asset('attachments/file/mp3/'.$file_report->filename) }}" controls>
                                @else
    								            <video id="my-video" class="video-js" controls preload="auto" width="362" height="180"
    								                   data-setup="{}">
    								                <source src="{{ asset('attachments/file/video/'.$file_report->filename) }}" type="video/mp4">
    								                <p class="vjs-no-js">
    								                    To view this video please enable JavaScript, and consider upgrading to a web browser that
    								                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    								                </p>
    								            </video>
                                @endif

							            @endif

							        @else
							            <div class="alert alert-warning alert-dismissable" style="margin-top: 20px;">
							                <i class="glyphicon glyphicon-warning-sign"></i>
							                Belum ada laporan video untuk staff ini.
							            </div>
							        @endif

				            	</div>

				            </div>

				         </div>
				      </div>

				      <!-- <div id="tab-3" class="tab-pane">

				      	<div class="panel-body">
				      		<div id="chart-performance" style="width:620px;height:350px;margin:0 auto"></div>
				      	</div>

				      </div> -->

				   </div>
				</div>
			</div>
		</div>
    <div class="row">
      <div class="col-md-12">
        <div class="ibox">
    			<!-- <div class="ibox-content">
            <iframe src="{{ route('staff.download.pdf',$staff->id) }}" style="border: none;" height="600px" width="100%"></iframe>
          </div> -->
        </div>
      </div>
    </div>
	</div>

</div><!-- end row -->

@endsection

@section('js')

	<script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

	<script>
		$('#chart-performance').highcharts({
            chart: {
                    type: 'column'
                },
                colors: ['#3498DB'],
                title: {
                    text: 'Staff Performance'
                },
                xAxis: {
                    type: 'category',
                     title: {
                        text: ' '
                    }
                },

                legend: {
                    enabled: false
                },

                plotOptions: {
                    series: {
                        borderWidth: 0,
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                yAxis: {
                     // tickPositioner: function() {
                     //        return [0,10];
                     //    },
                     gridLineWidth: 0,

                    title: {
                        text: 'Persentase'
                    }

                },
                credits:{
                    enabled:false
                },

                series: [{
                    name: 'Score',
                    stacking: 'normal',
                    colorByPoint: true,
                    data: {!! json_encode($report_performance_data,JSON_NUMERIC_CHECK) !!}
                }],
        });

	</script>

@endsection
