{{-- Profile Page --}}
<div class="col-md-3">
   <div class="wrapper wrapper-content project-manager">
      <h4>Profile Staff</h4>
      			@if(!is_null($staff->ava))
                <img src="{{ asset('attachments/ava_staff/thumb').'/'.$staff->ava }}" alt="" class="img img-responsive img-thumbnail">
            @else
                <img src="http://lorempixel.com/300/200/business" class="img-responsive img-thumbnail">
            @endif
	      <br>
      <h4><strong>{{ $staff->nama or '-' }}</strong></h4>
      <p><i class="fa fa-user"></i> {{ $staff->position->alias or '-' }} - {{ $staff->store->name or '-' }}</p>
      <p class="small font-bold">
         <span><i class="fa fa-circle text-warning"></i> Peringkat <strong>{{ ($staff_score->score > 0) ? $current_rank_in_position : 'N/A' }}</strong> Dari {{ $staff['position']->alias }} </span> <br>
         <!-- <span><i class="fa fa-circle text-warning"></i>
			Peringkat ke <strong>{{ ($staff_score->score > 0) ? $current_rank_in_kota_by_position : 'N/A' }}</strong> Dari {{ count($ranks_in_kota_by_position) }} {{ $staff['position']->alias }} Di {{ $staff->store->kota->name }}
         </span> <br> -->
      </p>
      <h5>Project files</h5>
      <ul class="list-unstyled project-files">
         <!-- <li><a href="{{ route('staff.download.pdf',$staff->id) }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download Report PDF</a></li> -->

         {{-- download file multimedia --}}
         @if(!is_null($file_report))
			@if($file_report->filetype == 'mp4' || $file_report->filetype == 'MP4')
				<li><a href="{{ route('staff.download.multimedia',$file_report->id) }}"><i class="fa fa-video-camera"></i> Download File MP4</a></li>
			@else
				<li><a href="{{ route('staff.download.multimedia',$file_report->id) }}"><i class="fa fa-music"></i> Download File MP3</a></li>
			@endif
		 @else
		 	<span class="label label-warning">Data belum diupload</span>
         @endif
      </ul>
   </div>
</div><!-- end colmd-3 -->
