<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta charset="utf-8" />
        <style>
            .text-center {
                text-align: center;
            }
            .text-right {
                text-align: right;
            }
            h3 {
                font-size: 24px;
            }
            h1, h2, h3 {
                margin-top: 10px;
                margin-bottom: 10px;
            }
            h1, h2, h3, h4, h5, h6 {
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-weight: 500;
                line-height: 1.1;
                color: inherit;
            }
            .table-bordered {
                border: 1px solid #000;
            }
            .table {
                margin: 0 auto;
                margin-top: 10px;
                width: 100%;
            }
            table {
                background-color: transparent;
                border-collapse: collapse;
                border-spacing: 0;
            }
            .table > caption + thead > tr:first-child > th,
            .table > colgroup + thead > tr:first-child > th,
            .table > thead:first-child > tr:first-child > th,
            .table > caption + thead > tr:first-child > td,
            .table > colgroup + thead > tr:first-child > td,
            .table > thead:first-child > tr:first-child > td {
                border-top: 0;
            }
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                vertical-align: middle;
            }
            .table-bordered>thead>tr>th,
            .table-bordered>thead>tr>td {
                border-bottom-width: 2px;
            }
            .table-bordered>thead>tr>th,
            .table-bordered>tbody>tr>th,
            .table-bordered>tfoot>tr>th,
            .table-bordered>thead>tr>td,
            .table-bordered>tbody>tr>td,
            .table-bordered>tfoot>tr>td {
                border: 1px solid #000;
            }
            .table>thead>tr>th {
                border-bottom: 2px solid #000;
            }
            .table>thead>tr>th,
            .table>tbody>tr>th,
            .table>tfoot>tr>th,
            .table>thead>tr>td,
            .table>tbody>tr>td,
            .table>tfoot>tr>td {
                padding: 8px;
                line-height: 1.428571429;
                border-top: 1px solid #000;
            }
            img {
                vertical-align: middle;
                border: 0;
            }
            .table.no-border>thead>tr>th,
            .table.no-border>tbody>tr>th,
            .table.no-border>tfoot>tr>th,
            .table.no-border>thead>tr>td,
            .table.no-border>tbody>tr>td,
            .table.no-border>tfoot>tr>td {
                border: none;
            }
            .report {
                border: 1px solid #333;
                padding: 5px 20px;
            }
        </style>
    </head>

    <body>
    
    <?php

    /**
     *
     */
    function konversiTanggal($format, $tanggal)
    {
        $en = [
                "Sun",
                "Mon",
                "Tue",
                "Wed",
                "Thu",
                "Fri",
                "Sat",
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
        ];

        $id = [
                "Minggu",
                "Senin",
                "Selasa",
                "Rabu",
                "Kamis",
                "Jumat",
                "Sabtu",
                "Januari",
                "Pebruari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "Nopember",
                "Desember"
        ];

        return str_replace($en, $id, date($format, strtotime($tanggal)));
    }
    ?>
        
        <div class="text-center">
            <img src="{{ public_path('frontend/img/logo_header.png') }}" alt="Erafone" style="max-height:200px" />


            <h2>Laporan {{ $report->name }}</h2>
            </h2>

            <h2 style="padding-bottom:5px;border-bottom: 1px solid #333">
                {{ konversiTanggal('j M Y', date('Y-m-d')) }}
            </h2>
        </div>

        <div class="text-center" style="margin-top: 10px">
            <h3>Data Staff</h3>
        </div>

        <table class="table table-bordered">
            <tr>
                <td rowspan="2">
                    <strong>Foto</strong>
                </td>
                <td rowspan="2">
                    
                    @if(!is_null($staff->ava))
                        <img src="{{ asset('attachments/ava_staff/thumb').'/'.$staff->ava }}" alt="" class="img img-responsive img-thumbnail">
                    @else
                        <img src="http://lorempixel.com/300/200/" class="img-responsive img-thumbnail">    
                    @endif

                    
                </td>

                <td><strong>Nama</strong></td>
                <td>{{ $staff->nama }}</td>
            </tr>

            <tr>
                <td><strong>Posisi</strong></td>
                <td>{{ $staff['position']->name }}</td>
            </tr>

            <?php
                $range = Config::get('range.dealer.'. 0);

                $score = (float) $staff_score->score;

                if($score>= 81.00)
                   {
                       $warna = '#0073b7';
                       $teks  = 'Sangat Baik';
                   }
                   elseif($score >= 66.0 && $score < 80.99)
                   {
                       $warna = '#00a65a';
                       $teks  = 'Baik';
                   }
                   elseif($score >= 46.00 && $score < 65.99)
                   {
                       $warna = '#f39c12';
                       $teks  = 'Cukup Baik';
                   }
                   elseif($score >= 21.00 && $score < 45.99)
                   {
                       $warna = '#ff851b';
                       $teks  = 'Kurang Baik';
                   }
                   elseif($score < 20.00)
                   {
                       $warna = '#f56954';
                       $teks  = 'Sangat Kurang';
                   }

            ?>

            <tr>
                <td><strong>Index</strong></td>
                <td>
                        <span style="color: {{ $warna }}">
                            {{ $staff_score->score > 0 ? number_format($staff_score->score, 2) : 'N/A' }}
                        </span>
                </td>

                <td><strong>Deskripsi Index</strong></td>
                <td>
                        <span style="color: {{ $warna }}">
                            {{ $teks }}
                        </span>
                </td>
            </tr>

            <tr>
                <td><strong>Store</strong></td>
                <td colspan="3">{{ $staff['store']->name }}</td>
            </tr>

            <tr>
                <td><strong>Peringkat</strong></td>
                <td colspan="3">
                    - Peringkat {{ $current_rank_in_position }} dari Staff di semua store <br>
                    - Peringkat ke <strong>{{ ($staff_score->score > 0) ? $current_rank_in_kota_by_position : 'N/A' }}</strong> Dari {{ count($ranks_in_kota_by_position) }} {{ $staff['position']->name }} Di {{ $staff->store->kota->name }}
                </td>
            </tr>

        </table>

        <br />

        <div class="text-center" style="margin-bottom: 20px">
            <h3>Parameter</h3>
        </div>
        
        @if(count($staff['position']['parameter_category']))
            @foreach($staff['position']['parameter_category'] as $key => $category)
                @if($key > 0)
                    <br />
                @endif

                <h4 class="text-center" style="margin-bottom: 20px">
                    {{ $category->category }}
                </h4>

                <?php $parameter = $category['parameter_data']; ?>

                @if(count($parameter))
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">Parameter</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Komentar</th>
                        </tr>
                        </thead>

                        @foreach($parameter as $param)
                            <?php $value = \App\Models\ParameterReportPosition::where('staff_id', '=', $staff->id)->where('position_parameter_data_id', '=', $param->id)->where('report_id', '=', $report->id)->first(); ?>

                            @if($value)
                                <tr>
                                    <td>{{ $param->text }}</td>
                                    <td class="text-center">
                                        @if($value->status == 3)
                                            N/A
                                        @elseif($value->status == 1)
                                            Ya
                                        @elseif($value->status == 2)
                                            Tidak
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $value->comment }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                @else
                    <p>Belum ada parameter untuk kategori ini.</p>
                @endif
            @endforeach
        @else
            <p>Belum ada kategori parameter untuk posisi ini.</p>
        @endif
        
        <div class="text-center" style="margin-bottom: 20px">
            <h3>Kesimpulan</h3>
        </div>

        <div class="report">
            {!! ( ! is_null($staff['reportstaff']) && $staff['reportstaff']->summary) ? $staff['reportstaff']->summary : 'Tidak ada kesimpulan laporan untuk staff ini.'  !!}
        </div>

    </body>
</html>