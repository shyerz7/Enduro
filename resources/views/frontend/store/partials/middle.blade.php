{{-- Middle COntent                --}}
<div class="row">

  <div class="col-md-8">
     <div class="tabs-container">
        <ul class="nav nav-tabs">
           <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> People Performance</a></li>
           <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Tangible Performance</a></li>
           <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">Mystery Calling Performance</a></li>
        </ul>
        <div class="tab-content">

           <div id="tab-1" class="tab-pane active">
              <div class="panel-body">
                 <div id="staff-performance" style="width:auto; min-height: 250px; margin: 0 auto"></div>
              </div>
           </div>

           <div id="tab-2" class="tab-pane">
              <div class="panel-body">

                 <div class="row">
                   <div class="col-md-7">
                     <center>
                        <div id="fisik-performance" style="width: auto; min-height: 250px; margin: 0 auto"></div>
                     </center>
                   </div>
                   <div class="col-md-5">

                     @if(!is_null($fisik_performance_data))
                       @foreach($fisik_performance_data as $data_fisik)

                          <?php
                                $score_fisik = number_format((float) $data_fisik['y'],1);

                                if($score_fisik >= 85.00)
                                {
                                    $warna = 'bg-blue';
                                    $teks  = 'Sangat Baik';
                                    $icon  = 'glyphicon glyphicon-thumbs-up';
                                }
                                elseif($score_fisik >= 75.00 && $score_fisik <= 84.99)
                                {
                                    $warna = 'bg-green';
                                    $teks  = 'Baik';
                                    $icon  = 'glyphicon glyphicon-plus';
                                }
                                elseif($score_fisik >= 65.00 && $score_fisik <= 74.99)
                                {
                                    $warna = 'bg-yellow';
                                    $teks  = 'Cukup';
                                    $icon  = 'glyphicon glyphicon-ok';
                                }
                                elseif($score_fisik >= 55.00 && $score_fisik <= 64.99)
                                {
                                    $warna = 'bg-orange';
                                    $teks  = 'Kurang';
                                    $icon  = 'glyphicon glyphicon-ok';
                                }
                                elseif($score_fisik <= 54.99)
                                {
                                    $warna = 'bg-red';
                                    $teks  = 'Sangat Kurang';
                                    $icon  = 'glyphicon glyphicon-thumbs-down';
                                }elseif(is_null($score_fisik))
                                {
                                    $warna = 'bg-red';
                                    $teks  = '-';
                                    $icon  = 'glyphicon glyphicon-thumbs-down';
                                }

                            ?>

                            <div class="small-box {{ $warna }} mrg-top-15 text-left pull-right" style="width:100%;">
                              <div class="inner" style="padding-left: 7px;">
                                  <h3 style="color:white;">
                                        {{ $score_fisik }}
                                      <p>
                                      {{ $data_fisik['name'] }} - {{ $teks }}
                                      <?php
                                        $id = $data_fisik['id'];
                                      ?>
                                      <span class="pull-right">
                                        <a href="{{ route('fisikstore.download.pdf', $id ) }} }}" class="btn btn-xs btn-primary">Download Laporan</a>
                                      </span>
                                  </p>
                                  </h3>
                              </div>
                            </div>

                       @endforeach
                     @endif



                   </div>
                 </div>

              </div>
           </div>

            <div id="tab-3" class="tab-pane">
                <div id="mc-performance" style="width:auto; min-height: 250px; margin: 0 auto"></div>
            </div>

        </div>
     </div>
  </div>

  <div class="col-md-4">
     <div class="ibox ">
        <div class="ibox-content">

           <strong>Overall Ranking</strong>
             <ul class="list-group">
                <li class="list-group-item">
                   Rank
                   <span class="pull-right">
                   {{ ($current_rank > 0) ? $current_rank : 'N/A' }} dari {{ count($semua_store) }} Cabang
                   </span>
                </li>
                <li class="list-group-item">
                   Rank In RBH
                   <span class="pull-right">
                   {{ ($current_rank_in_kota > 0) ? $current_rank_in_kota : 'N/A' }} dari {{ count($count_store_by_kota) }} Cabang di {{$kota->name}}
                   </span>
                </li>
                <li class="list-group-item">
                   Highest Staff Index
                   <span class="pull-right">
                    {!! $staffmax ? $staffmax->score : '-' !!}
                   </span>
                </li>
                <li class="list-group-item">
                   Lowest Staff Index
                   <span class="pull-right">
                   {!! $staffmin ? $staffmin->score  : '-' !!}
                   </span>
                </li>
             </ul>

        </div>
    </div>
  </div>

</div>
