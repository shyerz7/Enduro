<html>

<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plasa Indonesia Web Report</title>
    <meta name="Nova theme" content="width=device-width, initial-scale=1">
    <!-- <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png"/> -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('landing/css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/responsive.css')}}"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <style>
    .nav-tabs > li {
    float: left;
    margin-bottom: -1px;
    background-color: rgba(255,255,255,.6);
    border-radius: 3px;
    font-size: 12px;
    }
    </style>

</head>

<body>

<!-- Navigation
    ================================================== -->
<div class="hero-background">
    <div>
        <img class="strips" src="{{ asset('landing/images/strips.png') }}">
    </div>
    <div class="container">
        <div class="header-container header">
            <a class="navbar-brand logo" href="#"></a>
            <!-- @if (Auth::guest())
                <a href="{!! url('/login') !!}"><button class="hero-btn">LOGIN</button></a>
            @else
                  <a href="{{ url('/home') }}"><button class="hero-btn">DASHBOARD</button></a>
            @endif -->
            <!-- <div class="header-right">
                <a class="navbar-item" href="#features">Features</a>
            </div> -->
        </div>

        <!-- Hero-Section
          ================================================== -->

        <div class="hero row">
            <div class="hero-right col-sm-6 col-sm-6">
                <h1 class="header-headline bold"> Plaza Indonesia Web Report <br></h1>
                <h4 class="header-running-text light"> Mystery Shopping Online Report.</h4>
                @if (Auth::guest())
                    <a href="{!! url('/login') !!}"><button class="hero-btn">LOGIN</button></a>
                @else
                      <a href="{{ url('/home') }}">
                        <button class="hero-btn"> DASHBOARD</button>
                      </a>
                @endif
            </div><!--hero-left-->

            <div class="col-sm-6 col-sm-6">
              @if (Auth::guest())
              <p>&nbsp;</p>
              @else

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" data-target="#home1" href="#"><span class="fa fa-home"></span>&nbsp;</a></li>
                    <li><a data-toggle="tab" data-target="#tab1" href="#">Reliability</a></li>
                    <li><a data-toggle="tab" data-target="#tab2" href="#">Assurance</a></li>
                    <li><a data-toggle="tab" data-target="#tab3" href="#">Tangible</a></li>
                    <li><a data-toggle="tab" data-target="#tab4" href="#">Empathy</a></li>
                    <li><a data-toggle="tab" data-target="#tab5" href="#">Responsiveness</a></li>
                  </ul>

                  <div class="tab-content">
                    <div id="home1" class="tab-pane fade in active">
                      <div id="container" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                      <div id="container2" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                    <div id="tab1" class="tab-pane fade">
                      <div id="container3" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                      <div id="container8" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                    <div id="tab2" class="tab-pane fade">
                      <div id="container4" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                      <div id="container9" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                    <div id="tab3" class="tab-pane fade">
                      <div id="container5" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                      <div id="container10" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                    <div id="tab4" class="tab-pane fade">
                      <div id="container6" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                      <div id="container11" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                    <div id="tab5" class="tab-pane fade">
                      <div id="container7" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                      <div id="container12" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                  </div>

              @endif
            </div>

            <div><img class="mouse" src="{{ asset('landing/images/mouse.svg') }}"/></div>

        </div><!--hero-->

    </div> <!--hero-container-->

</div><!--hero-background-->


<!-- Features
  ================================================== -->

<div id="features" class="features-section">

    <div class="features-container row">

        <h2 class="features-headline light">FEATURES</h2>

        <div class="col-sm-4 feature">
            <div class="feature-icon feature-no-display feature-display-mid">
                <img class="feature-img" src="{{ asset('landing/images/customizable.svg') }}">
            </div>
            <h5 class="feature-head-text feature-no-display feature-display-mid"> WEB APPS </h5>
            <p class="feature-subtext light feature-no-display feature-display-mid"> Complex web application that can measure any algorithm needed for every business needs. </p>
        </div>

        <div class="col-sm-4 feature">
            <div class="feature-icon feature-no-display feature-display-last">
                <img class="bullet-img" src="{{ asset('landing/images/design.svg') }}">
            </div>
            <h5 class="feature-head-text feature-no-display feature-display-last"> SECURE </h5>
            <p class="feature-subtext light feature-no-display feature-display-last"> Privacy, multiple role user login. Secure yet easy to access platform. </p>
        </div>

        <div class="col-sm-4 feature">

            <div class="feature-icon feature-no-display">
                <img class="feature-img" src="{{ asset('landing/images/responsive.svg') }}">
            </div>
            <h5 class="feature-head-text feature-no-display"> FULLY RESPONSIVE </h5>
            <p class="feature-subtext light feature-no-display"> Multiple
                device view: smartphone, tablet, laptop and desktop.</p>
        </div>
    </div> <!--features-container-->
</div> <!--features-section-->

<!-- Email-Section
  ================================================== -->

<div class="blue-section">
    <h3 class="blue-section-header bold">
RAD Research Web Report V.3.1 ©2017</h3>

    <!--email-form-div-->

    <div id="newsletter-loading-div"></div>
    <!--email-form-->
</div>
<!--blue-section-->

<!-- Footer
  ================================================== -->

<div class="footer">

    <div class="container">
        <div class="row">

            <div class="col-sm-2"></div>

            <div class="col-sm-8 webscope">
                <span class="webscope-text"> Developed By: </span>
                <a href="http://rad-research.com"> RAD Research </a>
            </div>
            <!--webscope-->

            <div class="col-sm-2">

            </div>
            <!--social-links-parent-->

        </div>
        <!--row-->

    </div>
    <!--container-->
</div>
<!--footer-->

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
<script src="{{ asset('landing/js/script.js') }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.3/js/themes/dark-unica.js"></script> -->
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script>

// Highcharts.setOptions(Highcharts.theme);
//Plaza Indonesia
Highcharts.chart('container', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Performa Staff Plaza Indonesia',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y: 97.03,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:  95.04,
            drilldown: 'Customer Service Team'
        }, {
            name: 'Parking Team',
            y:  69.06,
            drilldown: 'Parking Team'
        }, {
            name: 'House Keeping Team',
            y:  98.89,
            drilldown: 'House Keeping Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'In Mall Security',
                    97.70
                ],
                [
                    'Lobby Entrance',
                    95.82
                ],
                [
                    'Lobby Basement',
                    94.91
                ],
                [
                    'Car Checking',
                    99.68
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Doorman',
                    88.21
                ],
                [
                    'Ask Me',
                    96.17
                ],
                [
                    'Car Call',
                    99.50
                ],
                [
                    'Concierge',
                    96.64
                ],
                [
                    'Phone Operator',
                    94.69
                ]
            ]
        }, {
            name: 'Parking Team',
            id: 'Parking Team',
            data: [
                [
                    'Cashier',
                    27.15
                ],
                [
                    'Valet Assistance',
                    80.03
                ],
                [
                    'Car Park Assistance',
                    100.00
                ]
            ]
        }, {
            name: 'House Keeping Team',
            id: 'House Keeping Team',
            data: [
                [
                    'Toilet Attendance',
                    98.89
                ]
            ]
        }
      ]
    }
});

//plaza tower
Highcharts.chart('container2', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Performa Staff Plaza Office Tower',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y:  90.4,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:   86.5,
            drilldown: 'Customer Service Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'Car Checking',
                    79.7
                ],
                [
                    'Lobby Entrance',
                    97.0
                ],
                [
                    'Lobby Office Tower',
                    94.5
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Concierge',
                    99.1
                ],
                [
                    'Reception',
                    73.9
                ]
            ]
        }
      ]
    }
});

//Reliability
Highcharts.chart('container3', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Plaza Indonesia Shopping Center',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y: 100.00,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:  73.33,
            drilldown: 'Customer Service Team'
        }, {
            name: 'Parking Team',
            y:  66.67,
            drilldown: 'Parking Team'
        }, {
            name: 'House Keeping Team',
            y:  100.00,
            drilldown: 'House Keeping Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'In Mall Security',
                    100.00
                ],
                [
                    'Lobby Entrance',
                    100.00
                ],
                [
                    'Lobby Basement',
                    100.00
                ],
                [
                    'Car Checking',
                    100.00
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Doorman',
                    90.00
                ],
                [
                    'Ask Me',
                    0
                ],
                [
                    'Car Call',
                    90.00
                ],
                [
                    'Concierge',
                    0
                ],
                [
                    'Phone Operator',
                    40.00
                ]
            ]
        }, {
            name: 'Parking Team',
            id: 'Parking Team',
            data: [
                [
                    'Cashier',
                    20.00
                ],
                [
                    'Valet Assistance',
                    80.00
                ],
                [
                    'Car Park Assistance',
                    100.00
                ]
            ]
        }, {
            name: 'House Keeping Team',
            id: 'House Keeping Team',
            data: [
                [
                    'Toilet Attendance',
                    100.00
                ]
            ]
        }
      ]
    }
});

//Assurance
Highcharts.chart('container4', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Plaza Indonesia Shopping Center',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y: 94.97,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:  93.88,
            drilldown: 'Customer Service Team'
        }, {
            name: 'Parking Team',
            y:  65.05,
            drilldown: 'Parking Team'
        }, {
            name: 'House Keeping Team',
            y:  93.64,
            drilldown: 'House Keeping Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'In Mall Security',
                    97.50
                ],
                [
                    'Lobby Entrance',
                    94.55
                ],
                [
                    'Lobby Basement',
                    89.09
                ],
                [
                    'Car Checking',
                    98.75
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Doorman',
                    82.56
                ],
                [
                    'Ask Me',
                    94.72
                ],
                [
                    'Car Call',
                    99.43
                ],
                [
                    'Concierge',
                    92.70
                ],
                [
                    'Phone Operator',
                    100.00
                ]
            ]
        }, {
            name: 'Parking Team',
            id: 'Parking Team',
            data: [
                [
                    'Cashier',
                    19.26
                ],
                [
                    'Valet Assistance',
                    75.88
                ],
                [
                    'Car Park Assistance',
                    100.00
                ]
            ]
        }, {
            name: 'House Keeping Team',
            id: 'House Keeping Team',
            data: [
                [
                    'Toilet Attendance',
                    93.64
                ]
            ]
        }
      ]
    }
});

//Tangible
Highcharts.chart('container5', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Plaza Indonesia Shopping Center',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y: 97.41,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:  97.50,
            drilldown: 'Customer Service Team'
        }, {
            name: 'Parking Team',
            y:  66.28,
            drilldown: 'Parking Team'
        }, {
            name: 'House Keeping Team',
            y:  99.53,
            drilldown: 'House Keeping Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'In Mall Security',
                    100.00
                ],
                [
                    'Lobby Entrance',
                    91.94
                ],
                [
                    'Lobby Basement',
                    97.71
                ],
                [
                    'Car Checking',
                    100.00
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Doorman',
                    90.00
                ],
                [
                    'Ask Me',
                    100.00
                ],
                [
                    'Car Call',
                    100.00
                ],
                [
                    'Concierge',
                    100.00
                ],
                [
                    'Phone Operator',
                    0
                ]
            ]
        }, {
            name: 'Parking Team',
            id: 'Parking Team',
            data: [
                [
                    'Cashier',
                    20.00
                ],
                [
                    'Valet Assistance',
                    78.84
                ],
                [
                    'Car Park Assistance',
                    100.00
                ]
            ]
        }, {
            name: 'House Keeping Team',
            id: 'House Keeping Team',
            data: [
                [
                    'Toilet Attendance',
                    99.53
                ]
            ]
        }
      ]
    }
});

//Empathy
Highcharts.chart('container6', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Plaza Indonesia Shopping Center',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y: 90.00,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:  92.00,
            drilldown: 'Customer Service Team'
        }, {
            name: 'Parking Team',
            y:  45.00,
            drilldown: 'Parking Team'
        }, {
            name: 'House Keeping Team',
            y:  0,
            drilldown: 'House Keeping Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'In Mall Security',
                    80.00
                ],
                [
                    'Lobby Entrance',
                    0
                ],
                [
                    'Lobby Basement',
                    0
                ],
                [
                    'Car Checking',
                    100.00
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Doorman',
                    90.00
                ],
                [
                    'Ask Me',
                    80.00
                ],
                [
                    'Car Call',
                    100.00
                ],
                [
                    'Concierge',
                    100.00
                ],
                [
                    'Phone Operator',
                    90.00
                ]
            ]
        }, {
            name: 'Parking Team',
            id: 'Parking Team',
            data: [
                [
                    'Cashier',
                    20.00
                ],
                [
                    'Valet Assistance',
                    70.00
                ],
                [
                    'Car Park Assistance',
                    0
                ]
            ]
        }, {
            name: 'House Keeping Team',
            id: 'House Keeping Team',
            data: [
                [
                    'Toilet Attendance',
                    0
                ]
            ]
        }
      ]
    }
});

//Responsiveness
Highcharts.chart('container7', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Plaza Indonesia Shopping Center',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y: 100.00,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:  80.00,
            drilldown: 'Customer Service Team'
        }, {
            name: 'Parking Team',
            y:  50.00,
            drilldown: 'Parking Team'
        }, {
            name: 'House Keeping Team',
            y:  0,
            drilldown: 'House Keeping Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'In Mall Security',
                    100.00
                ],
                [
                    'Lobby Entrance',
                    0
                ],
                [
                    'Lobby Basement',
                    0
                ],
                [
                    'Car Checking',
                    0
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Doorman',
                    0
                ],
                [
                    'Ask Me',
                    80.00
                ],
                [
                    'Car Call',
                    100.00
                ],
                [
                    'Concierge',
                    50.00
                ],
                [
                    'Phone Operator',
                    90.00
                ]
            ]
        }, {
            name: 'Parking Team',
            id: 'Parking Team',
            data: [
                [
                    'Cashier',
                    20.00
                ],
                [
                    'Valet Assistance',
                    80.00
                ],
                [
                    'Car Park Assistance',
                    0
                ]
            ]
        }, {
            name: 'House Keeping Team',
            id: 'House Keeping Team',
            data: [
                [
                    'Toilet Attendance',
                    0
                ]
            ]
        }
      ]
    }
});
//plaza tower Reliability
Highcharts.chart('container8', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Performa Staff Plaza Office Tower',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y:  93.33,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:   80.00,
            drilldown: 'Customer Service Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'Car Checking',
                    80.00
                ],
                [
                    'Lobby Entrance',
                    100.00
                ],
                [
                    'Lobby Office Tower',
                    100.00
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Concierge',
                    0
                ],
                [
                    'Reception',
                    80.00
                ]
            ]
        }
      ]
    }
});
//plaza tower Assurance
Highcharts.chart('container9', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Performa Staff Plaza Office Tower',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y:  86.48,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:   81.24,
            drilldown: 'Customer Service Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'Car Checking',
                    72.73
                ],
                [
                    'Lobby Entrance',
                    95.45
                ],
                [
                    'Lobby Office Tower',
                    91.25
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Concierge',
                    98.61
                ],
                [
                    'Reception',
                    63.86
                ]
            ]
        }
      ]
    }
});
//plaza tower Tangible
Highcharts.chart('container10', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Performa Staff Plaza Office Tower',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y:  91.37,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:   90.00,
            drilldown: 'Customer Service Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'Car Checking',
                    80.00
                ],
                [
                    'Lobby Entrance',
                    94.12
                ],
                [
                    'Lobby Office Tower',
                    100.00
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Concierge',
                    100.00
                ],
                [
                    'Reception',
                    80.00
                ]
            ]
        }
      ]
    }
});
//plaza tower Empathy
Highcharts.chart('container11', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Performa Staff Plaza Office Tower',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y:  80.00,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:   90.00,
            drilldown: 'Customer Service Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'Car Checking',
                    80.00
                ],
                [
                    'Lobby Entrance',
                    0
                ],
                [
                    'Lobby Office Tower',
                    80.00
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Concierge',
                    100.00
                ],
                [
                    'Reception',
                    80.00
                ]
            ]
        }
      ]
    }
});

//plaza tower Responsiveness
Highcharts.chart('container12', {
    chart: {
        type: 'column',
        backgroundColor: 'rgba(255,255,255,0.40)'
    },
    title: {
        text: 'Performa Staff Plaza Office Tower',
        style: {
          color: '#fff',
        },
    },
    subtitle: {
        text: 'Klik masing-masing chart untuk melihat detil score.',
        style: {
          color: '#fff',
        },
    },
    xAxis: {
        type: 'category',
        style: {
          color: '#fff',
        },
    },
    yAxis: {
        title: {
            text: 'Total',
            style: {
              color: '#fff',
            },
        },
        max: 100

    },
    credits:{
        enabled:false
    },
    legend: {
        enabled: false,
        style: {
          color: '#fff',
        },
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}',
            },
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [{
        name: 'Team Score',
        colorByPoint: true,
        data: [{
            name: 'Security Team',
            y:  100.00,
            drilldown: 'Security Team'
        }, {
            name: 'Customer Service Team',
            y:   70.00,
            drilldown: 'Customer Service Team'
        }
      ]
    }],
    drilldown: {
        series: [{
            name: 'Security Team',
            id: 'Security Team',
            data: [
                [
                    'Car Checking',
                    0
                ],
                [
                    'Lobby Entrance',
                    0
                ],
                [
                    'Lobby Office Tower',
                    100.00
                ]
            ]
        }, {
            name: 'Customer Service Team',
            id: 'Customer Service Team',
            data: [
                [
                    'Concierge',
                    60.00
                ],
                [
                    'Reception',
                    80.00
                ]
            ]
        }
      ]
    }
});
</script>
</body>

</html>
