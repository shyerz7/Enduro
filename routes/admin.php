<?php

Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {

	Route::get('home',[
	    'as'    =>  'admin.home',
	    'uses'  =>  'Admin\HomeController@index'
	]);

	/**
	 * route change session
	 */
	Route::get('changeSession/{id}',[
	    'as'    =>  'admin.changeSession',
	    'uses'  =>  'Admin\ReportController@changeSession'
	]);

	Route::resource('reports', 'Admin\ReportController', ['except' => ['show']]);
	Route::get('downloadKuisioner/{id}',[
	    'as'    =>  'admin.downloadKuisioner',
	    'uses'  =>  'Admin\ReportController@downloadKuisioner'
	]);

	Route::resource('kotas', 'Admin\KotaController', ['except' => ['show']]);
	Route::resource('stores', 'Admin\StoresController');
	Route::resource('positions', 'Admin\PositionController');
	Route::resource('fisiks', 'Admin\FisiksController');
	Route::resource('reportstore', 'Admin\ReportStoreController');

	/**
	 * route fisik store
	 */
	Route::get('/fisikstores/store','Admin\FisikStoreController@stores')->name('admin.fisikstores.store');
	Route::get('/fisikstores/{code_store}','Admin\FisikStoreController@index')->name('admin.fisikstores.index');
	Route::get('/fisikstores/create/{code_store}','Admin\FisikStoreController@create')->name('admin.fisikstores.create');
	Route::post('/fisikstores/post/{code_store}','Admin\FisikStoreController@store')->name('admin.fisikstores.simpan');
	Route::delete('/fisikstores/destroy/{id}','Admin\FisikStoreController@destroy')->name('admin.fisikstores.destroy');


	/**
	 * route crud staff
	 */
	Route::get('/staffs/stores/','Admin\StaffController@stores')->name('admin.staffs.stores');
	Route::get('/staffs/{code}','Admin\StaffController@index')->name('admin.staffs.index');
	Route::get('/staffs/create/{code}','Admin\StaffController@create')->name('admin.staffs.create');
	Route::post('/staffs/store/{code}','Admin\StaffController@store')->name('admin.staffs.store');
	Route::get('/staffs/edit/{id}','Admin\StaffController@edit')->name('admin.staffs.edit');
	Route::put('/staffs/update/{id}','Admin\StaffController@update')->name('admin.staffs.update');
	Route::delete('/staffs/delete/{id}','Admin\StaffController@destroy')->name('admin.staffs.delete');

	/**
 * route kaategori parameter posisi
	 */
	Route::get('posisi/parameter_category/{id_posisi}','Admin\ParameterCategoryPositionController@create')
		   ->name('admin.posisi.parameter_category.position');
	Route::post('posisi/parameter_category/store/{id_posisi}','Admin\ParameterCategoryPositionController@store')
		   ->name('admin.posisi.parameter_category.store');
	Route::get('posisi/parameter_category/{id_posisi}/{id}/edit','Admin\ParameterCategoryPositionController@edit')
		   ->name('admin.posisi.parameter_category.position.edit');
	Route::put('posisi/parameter_category/{id_posisi}/{id}/edit','Admin\ParameterCategoryPositionController@update')
		   ->name('admin.posisi.parameter_category.position.update');
	Route::delete('posisi/parameter_category/{id}','Admin\ParameterCategoryPositionController@destroy')
		   ->name('admin.posisi.parameter_category.position.destroy');

	/**
     * route parameter posisi
     */
    Route::get('posisi/parameter/{id_posisi}/{id_kategori}','Admin\ParameterPositionController@create')
		   ->name('admin.posisi.parameter');
	Route::post('posisi/parameter/{id_posisi}/{id_kategori}','Admin\ParameterPositionController@store')
		   ->name('admin.posisi.parameter.store');
	Route::get('posisi/parameter/{id_posisi}/{id_kategori}/{id}/edit','Admin\ParameterPositionController@edit')
		   ->name('admin.posisi.parameter.edit');
	Route::put('posisi/parameter/{id}/update','Admin\ParameterPositionController@update')
		   ->name('admin.posisi.parameter.update');
	Route::delete('posisi/parameter/{id}','Admin\ParameterPositionController@destroy')
		   ->name('admin.posisi.parameter.destroy');

	/**
	 * route kaategori parameter fisik
	 */
	Route::get('fisik/parameter_category/{id_fisik}','Admin\ParameterCategoryFisikController@create')
		   ->name('admin.fisik.parameter_category.fisik');
	Route::post('fisik/parameter_category/store/{id_posisi}','Admin\ParameterCategoryFisikController@store')
		   ->name('admin.fisik.parameter_category.store');
	Route::get('fisik/parameter_category/{id_posisi}/{id}/edit','Admin\ParameterCategoryFisikController@edit')
		   ->name('admin.fisik.parameter_category.fisik.edit');
	Route::put('fisik/parameter_category/{id_posisi}/{id}/edit','Admin\ParameterCategoryFisikController@update')
		   ->name('admin.fisik.parameter_category.fisik.update');
	Route::delete('fisik/parameter_category/{id}','Admin\ParameterCategoryFisikController@destroy')
		   ->name('admin.fisik.parameter_category.fisik.destroy');

	/**
     * route parameter fisik
     */
    Route::get('fisik/parameter/{id_fisik}/{id_kategori}','Admin\ParameterFisikController@create')
		   ->name('admin.fisik.parameter');
	Route::post('fisik/parameter/{id_fisik}/{id_kategori}','Admin\ParameterFisikController@store')
		   ->name('admin.fisik.parameter.store');
	Route::get('fisik/parameter/{id_fisik}/{id_kategori}/{id}/edit','Admin\ParameterFisikController@edit')
		   ->name('admin.fisik.parameter.edit');
	Route::put('fisik/parameter/{id}/update','Admin\ParameterFisikController@update')
		   ->name('admin.fisik.parameter.update');
	Route::delete('fisik/parameter/{id}','Admin\ParameterFisikController@destroy')
		   ->name('admin.fisik.parameter.destroy');

	/**
	 * route report staff
	 */
	Route::get('/reportstaffs/stores/','Admin\ReportStaffController@stores')->name('admin.reportstaffs.stores');
	Route::get('/reportstaffs/{code}','Admin\ReportStaffController@index')->name('admin.reportstaffs.index');
	Route::get('/reportstaffs/edit/{id}','Admin\ReportStaffController@edit')->name('admin.reportstaffs.edit');
	Route::put('/reportstaffs/update/{id}','Admin\ReportStaffController@update')->name('admin.reportstaffs.update');


	/**
	 * route report fisik store
	 */
	Route::get('/reportfisikstores/stores/','Admin\ReportFisikStoreController@stores')->name('admin.reportfisikstores.stores');
	Route::get('/reportfisikstores/{code}','Admin\ReportFisikStoreController@index')->name('admin.reportfisikstores.index');
	Route::get('/reportfisikstores/edit/{id}','Admin\ReportFisikStoreController@edit')->name('admin.reportfisikstores.edit');
	Route::put('/reportfisikstores/update/{id}','Admin\ReportFisikStoreController@update')->name('admin.reportfisikstores.update');

	/**
	 * hapus file attachment
	 */
	Route::delete('file/staff/destroy/{id}','Admin\ReportStaffController@destroyFile')
		   ->name('admin.file.staff.destroy');

	/**
	 * Route untuk setting web report
	 */
	// Route::put('update/websetting',[
	//     'as'    =>  'admin.update.websetting',
	//     'uses'  =>  'Admin\WebSettingController@update'
	// ]);
	
	/**
	 * Route parameter nasional
	 */
	Route::get('/reportparameter/','Admin\ReportParameterController@index')->name('admin.reportparameter.index');
	Route::put('/reportparameter/storeNasional','Admin\ReportParameterController@storeNasional')->name('admin.reportparameter.storeNasional');
	Route::get('/reportparameter/rbh/{id_rbh}','Admin\ReportParameterController@getRbh')->name('admin.reportparameter.getrbh');
	Route::put('/reportparameter/storeRbh/{code}','Admin\ReportParameterController@storeRbh')->name('admin.reportparameter.storeRbh');

	Route::get('/reportnasionalrbh/','Admin\ReportNasionalRBHController@edit')->name('admin.reportnasionalrbh.edit');
	Route::post('/reportnasionalrbh/','Admin\ReportNasionalRBHController@store')->name('admin.reportnasionalrbh.edit');
	

});
