<?php

Route::group(['prefix' => 'ajax', 'middleware' => ['role:admin|admin_client|admin_kota']], function() {

	Route::get('/getTopKC',[
	    'as'    =>  'ajax.gettopkc',
	    'uses'  =>  'AjaxController@gettopkc'
	]);

	Route::get('/getTop10KCP',[
	    'as'    =>  'ajax.gettop10kcp',
	    'uses'  =>  'AjaxController@gettop10kcp'
	]);

	Route::get('/getLow10KCP',[
	    'as'    =>  'ajax.getlow10kcp',
	    'uses'  =>  'AjaxController@getlow10kcp'
	]);

	Route::get('/getTop10CS',[
	    'as'    =>  'ajax.gettop10cs',
	    'uses'  =>  'AjaxController@gettop10cs'
	]);

	Route::get('/getTop10Teller',[
	    'as'    =>  'ajax.gettop10Teller',
	    'uses'  =>  'AjaxController@gettop10Teller'
	]);

	Route::get('/getTop10Satpam',[
	    'as'    =>  'ajax.gettop10Satpam',
	    'uses'  =>  'AjaxController@gettop10Satpam'
	]);

	Route::get('/getTop10MC',[
	    'as'    =>  'ajax.gettop10mc',
	    'uses'  =>  'AjaxController@gettop10mc'
	]);

});	