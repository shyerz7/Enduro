<?php

Route::group(['prefix' => 'autoupload', 'middleware' => ['role:admin']], function() {

	/**
	 * import data staff dari excel
	 */
	Route::post('/import/kotas/','Admin\KotaController@importExcel')->name('admin.kotas.importExcel');

	/**
	 * import data staff dari excel
	 */
	Route::post('/import/stores/','Admin\StoresController@importExcel')->name('admin.stores.importExcel');

	/**
	 * import data staff dari excel
	 */
	Route::post('/import/staffs/','Admin\StaffController@importExcel')->name('admin.staffs.importExcel');

	/**
	 * import ava untuk store
	 */
	Route::post('/import/stores/ava','Admin\StoresController@importAva')->name('admin.stores.importAva');

	/**
	 * import ava untuk staff
	 */
	Route::post('/import/staffs/ava','Admin\StaffController@importAva')->name('admin.staffs.importAva');

	/**
	 * import data report store dari excel
	 */
	Route::post('/import/reportstrore/','Admin\ReportStoreController@importExcel')->name('admin.reportstrore.importExcel');

	/**
	 * import data report store dari excel
	 */
	Route::post('/import/reportstaff/','Admin\ReportStaffController@importExcelReport')->name('admin.reportstrore.importExcelReport');
	Route::post('/import/reportstaffscore/','Admin\ReportStaffController@importExcelReportScore')->name('admin.reportstrore.importExcelReportscore');
	Route::post('/import/reportstaffscoreIndividu/','Admin\ReportStaffController@importExcelReportScoreIndividu')->name('admin.reportstrore.importExcelReportscoreIndividu');
	Route::post('/import/reportstaffscorePerformance/','Admin\ReportStaffController@importExcelReportScorePerformance')->name('admin.reportstrore.importExcelReportscorePerformance');

	/**
	 * import data report store dari excel
	 */
	Route::post('/import/reportstaff/fileMultimedia','Admin\ReportStaffController@importFileMultimediaViaExcel')->name('admin.reportstaff.importFileMultimedia');

	/**
	 * import data postions parameter data
	 */
	Route::post('/import/position/parameter/data/','Admin\ParameterPositionController@importExcel')->name('admin.position.parameter.importExcel');


});	