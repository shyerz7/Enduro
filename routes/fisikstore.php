<?php

Route::group(['prefix' => 'fisikstore', 'middleware' => ['role:admin|admin_client|admin_kota|store']], function() {

	Route::get('/{id}',[
	    'as'    =>  '/',
	    'uses'  =>  'FisikStoreController@index'
	]);

	Route::get('download/pdf/{id}',[
	    'as'    =>  'fisikstore.download.pdf',
	    'uses'  =>  'FisikStoreController@pdf'
	]);

});