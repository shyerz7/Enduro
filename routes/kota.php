<?php

Route::group(['prefix' => 'kota', 'middleware' => ['role:admin|admin_client|admin_kota']], function() {

	Route::get('/{id_kota}',[
	    'as'    =>  'kota',
	    'uses'  =>  'KotaController@index'
	]);

});	