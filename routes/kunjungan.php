<?php
Route::group(['prefix' => 'kunjungan', 'middleware' => ['role:admin|admin_client|admin_kota|store']], function() {

	Route::get('/{code}',[
	    'as'    =>  'kunjungan',
	    'uses'  =>  'KunjunganController@index'
	]);

});
