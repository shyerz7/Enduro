<?php

Route::group(['prefix' => 'staff', 'middleware' => ['role:admin|admin_client|admin_kota|store']], function() {

	Route::get('/{id}',[
	    'as'    =>  '/',
	    'uses'  =>  'StaffController@index'
	]);

	Route::get('download/multimedia/{id}',[
	    'as'    =>  'staff.download.multimedia',
	    'uses'  =>  'StaffController@downloadMultimedia'
	]);

	Route::get('download/pdf/{id}',[
	    'as'    =>  'staff.download.pdf',
	    'uses'  =>  'StaffController@pdf'
	]);

	

});