<?php

Route::group(['prefix' => 'store', 'middleware' => ['role:admin|admin_client|admin_kota|store']], function() {

	Route::get('/{code}',[
	    'as'    =>  'store',
	    'uses'  =>  'StoreController@index'
	]);

});	