<?php

Route::group(['prefix' => 'template', 'middleware' => ['role:admin']], function() {

	/**
	 * template excel untuk upload kota master
	 */
	Route::get('/kotas/generateExcelTemplate/','Admin\KotaController@generateExcelTemplate')->name('admin.kotas.generateExcelTemplate');

	/**
	 * template excel untuk upload kota master
	 */
	Route::get('/stores/generateExcelTemplate/','Admin\StoresController@generateExcelTemplate')->name('admin.stores.generateExcelTemplate');

	/**
	 * template excel untuk upload posisi parameter
	 */
	Route::get('/parameter/positions/data/generateExcelTemplate/','Admin\ParameterPositionController@generateExcelTemplate')->name('admin.parameter.positions.data.generateExcelTemplate');


	/**
	 * template excel untuk upload staff
	 */
	Route::get('/staffs/generateExcelTemplate/','Admin\StaffController@generateExcelTemplate')->name('admin.staffs.generateExcelTemplate');
	
	/**
	 * template excel untuk upload report store
	 */
	Route::get('/reportstaff/generateExcelTemplate/','Admin\ReportStoreController@generateExcelTemplate')->name('admin.reportstaff.generateExcelTemplate');

	

	/**
	 * template report staff group by posisi
	 */
	Route::get('/reportstaff/generateExcelTemplatePosition/{id}','Admin\ReportStaffController@generateExcelTemplatePosition')->name('admin.reportstaff.generateExcelTemplatePosition');
	Route::get('/reportstaff/generateExcelTemplateScore/','Admin\ReportStaffController@generateExcelTemplateScore')->name('admin.reportstaff.generateExcelTemplateScore');
	Route::get('/reportstaff/generateExcelTemplateScorePerformance/','Admin\ReportStaffController@generateExcelTemplateScorePerformance')->name('admin.reportstaff.generateExcelTemplateScorePerformance');
	
	/**
	 * template untukupload via filazilla
	 */
	Route::get('/reportstaff/generateExcelTemplateValue/','Admin\ReportStaffController@generateExcelTemplateValue')->name('admin.reportstaff.generateExcelTemplateValue');

});